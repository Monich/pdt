message(STATUS "Adding GTest library dynamically")

# Download and unpack googletest at configure time
configure_file("${CMAKE_CURRENT_LIST_DIR}/input/GTestDynamic.in.cmake" "googletest-download/CMakeLists.txt")
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Dynamic config step for googletest failed: ${result}")
endif()

set(PDT_GTEST_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/googletest-src")
