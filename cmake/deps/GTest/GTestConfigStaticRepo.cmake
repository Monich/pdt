message(STATUS "Adding GTest library statically")

find_path(PDT_GTEST_DIR
          NAMES "gtest/gtest.h"
          DOC "Directory containing GTest and GMock repository.")

if(PDT_GTEST_DIR)
  message(STATUS "GTest repo found at ${PDT_GTEST_DIR}")
else(PDT_GTEST_DIR)
  message(FATAL_ERROR "GTest repo not found. Please set PDT_AUTO_GTEST to true if you want to use automatically managed repository. You can also set variable PDT_GTEST_DIR to a directory containing GTest repository")
endif(PDT_GTEST_DIR)

# Download and unpack googletest at configure time
configure_file("${CMAKE_CURRENT_LIST_DIR}/input/GTestStatic.in.cmake" "googletest-download/CMakeLists.txt")
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Static configure step for googletest failed: ${result}")
endif()

set(PDT_GTEST_SOURCE_DIR "${PDT_GTEST_DIR}")
