if(PDT_AUTO_GTEST)
  include("${CMAKE_CURRENT_LIST_DIR}/GTestConfigDynamicRepo.cmake")
else(PDT_AUTO_GTEST)
  include("${CMAKE_CURRENT_LIST_DIR}/GTestConfigStaticRepo.cmake")
endif(PDT_AUTO_GTEST)

# Build the provided repository
message(STATUS "Configuring GTest repository")

execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${PDT_GTEST_SOURCE_DIR}
                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)
				 
# Prevent displaying gtest variables in the GUI
set(BUILD_GMOCK ON)
set(INSTALL_GTEST OFF)
mark_as_advanced(BUILD_GMOCK INSTALL_GTEST)

# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
  include_directories("${gtest_SOURCE_DIR}/include")
endif()

include_directories(${gmock_SOURCE_DIR}/include)

message(STATUS "Finished creating Google Test library")
