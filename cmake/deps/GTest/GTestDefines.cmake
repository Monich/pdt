option(PDT_BUILD_TESTS "Builds the PDT Core UT tests subproject." OFF)

if(PDT_BUILD_TESTS)
  option(PDT_AUTO_GTEST "Automatically manage GTest repository. Needs Internet connection at CMake reload." OFF)
  include("${CMAKE_CURRENT_LIST_DIR}/GTestConfig.cmake")
else(PDT_BUILD_TESTS)
  message(STATUS "Unit test disabled - to enable them set PDT_BUILD_TESTS to true")
endif(PDT_BUILD_TESTS)

