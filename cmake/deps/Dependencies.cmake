message(STATUS "Gathering PDT dependencies")

include("${CMAKE_CURRENT_LIST_DIR}/GTest/GTestDefines.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/Boost.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/ClangTidy.cmake")
