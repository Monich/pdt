find_program(CLANG_TIDY_EXE NAMES "clang-tidy" DOC "Path to clang-tidy executable")

if(NOT CLANG_TIDY_EXE)
    message(WARNING "clang-tidy not found.")
else()
    message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")

    option(CLANG_TIDY_FAIL_ON_WARN "Fail build if clang-tidy detects an error" OFF)

    set(CLANG_CHECKS "*,-fuchsia-default-arguments,-fuchsia-overloaded-operator,-llvm-header-guard")

    if(CLANG_TIDY_FAIL_ON_WARN)
        set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}" "-checks=${CLANG_CHECKS}" "-warnings-as-errors=${CLANG_CHECKS}" "-header-filter=.*")
        message(STATUS "Build will fail on clang-tidy error")
    else(CLANG_TIDY_FAIL_ON_WARN)
        set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}" "-checks=${CLANG_CHECKS}")
    endif(CLANG_TIDY_FAIL_ON_WARN)
endif()