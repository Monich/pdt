message(STATUS "Looking for Boost library")

find_package(Boost REQUIRED)

message(STATUS "Boost include files found at ${Boost_INCLUDE_DIR}")
include_directories("${Boost_INCLUDE_DIR}")
