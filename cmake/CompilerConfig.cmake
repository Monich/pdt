message(STATUS "Configuring compiler")

# Fix for 'Secure' Windows warnings in VSTS compiler
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
set(CMAKE_CXX_STANDARD 17)

if (UNIX)
    link_libraries(stdc++fs)
endif (UNIX)
