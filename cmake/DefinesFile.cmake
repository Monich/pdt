message(STATUS "Creating defines file")

set(PDT_TEST_FILES_PATH ${CMAKE_SOURCE_DIR}/testfiles)

configure_file(${CMAKE_CURRENT_LIST_DIR}/cmake_defines.cmake.in ${CMAKE_BINARY_DIR}/include/cmake_defines.hpp)

include_directories(${CMAKE_BINARY_DIR}/include)

file(COPY ${CMAKE_SOURCE_DIR}/html DESTINATION ${CMAKE_BINARY_DIR})
