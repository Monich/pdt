message(STATUS "Configuring CMake project")

include("${CMAKE_CURRENT_LIST_DIR}/CompilerConfig.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/DefinesFile.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/deps/Dependencies.cmake")
