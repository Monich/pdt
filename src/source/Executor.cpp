#include "Executor.hpp"

#include "AssignmentComparison.hpp"
#include "AssignmentFactory.hpp"
#include "CommonLogger.hpp"
#include "IAssignmentComparisonMaker.hpp"
#include "IFilesLookup.hpp"
#include "ICommandLineParametersAnalyzer.hpp"
#include "IHtmlOutputMaker.hpp"

#include <algorithm>
#include <iostream>

Executor::Executor(const IAssignmentComparisonMaker &p_assignmentComparisonMaker,
                   const IAssignmentFactory &p_assignmentFactory,
                   const ICommandLineParametersAnalyser& p_commandLineParametersAnalyzer,
                   const IHtmlOutputMaker &p_htmlOutputFormatter) :
    m_assignmentComparisonMaker(p_assignmentComparisonMaker),
    m_assignmentFactory(p_assignmentFactory),
    m_commandLineParametersAnalyzer(p_commandLineParametersAnalyzer),
    m_htmlOutputFormatter(p_htmlOutputFormatter)
{}

void Executor::execute(const CommandLineParameters&p_commandLineParameters)
{
    std::unique_ptr<CommandLineConfiguration> l_configuration;

    try
    {
        l_configuration = m_commandLineParametersAnalyzer.analyzeCommandLineParameters(p_commandLineParameters);
    }
    catch(const CommandLineParametersException& l_exception)
    {
        std::cout << "Critical exception encountered: " << l_exception.what();
        throw l_exception;
    }

    LogManager::getSingletonInstance()->initialize(
        l_configuration->areDebugLogsEnabled);
    LOG_INFO << "Launching PDT alpha";
    LOG_INFO << "Are debug logs enabled = " << l_configuration->areDebugLogsEnabled;

    const Assignments l_assignments = m_assignmentFactory.loadAssignments(l_configuration->isSimplifiedModeOn);
    if(l_assignments.empty())
    {
        LOG_ERROR << "No assignments found - PDT will terminate";
        LogManager::getSingletonInstance()->terminate();
        return;
    }

    AssignmentComparisonMap l_assignmentComparisonMap{};
    if(l_configuration->singleAssignmentName)
    {
        auto l_original = std::find_if(l_assignments.begin(), l_assignments.end(),
                [&l_configuration](const Assignment& p_assignment) {return p_assignment.getAssignmentName() == *(l_configuration->singleAssignmentName);});

        if(l_original == l_assignments.end())
        {
            LOG_ERROR << "There is no assignment named " << *l_configuration->singleAssignmentName;
            LogManager::getSingletonInstance()->terminate();
            return;
        }

        for (const auto &l_assignment : l_assignments)
        {
            if(l_assignment.getAssignmentName() != l_original->getAssignmentName())
            {
                auto l_comparisonResult = m_assignmentComparisonMaker.compareAssignments(*l_original, l_assignment);
                l_assignmentComparisonMap[std::make_pair(*l_original, l_assignment)] = l_comparisonResult;
            }
        }
    }
    else
    {
        for (u32 l_firstAssignmentIndex = 0; l_firstAssignmentIndex < l_assignments.size(); ++l_firstAssignmentIndex)
        {
            for (u32 l_secondAssignmentIndex = l_firstAssignmentIndex + 1; l_secondAssignmentIndex < l_assignments.size(); ++l_secondAssignmentIndex)
            {
                auto l_comparisonResult = m_assignmentComparisonMaker.compareAssignments(l_assignments[l_firstAssignmentIndex],
                                                                                         l_assignments[l_secondAssignmentIndex]);
                l_assignmentComparisonMap[{l_assignments[l_firstAssignmentIndex], l_assignments[l_secondAssignmentIndex]}] = l_comparisonResult;
            }
        }
    }

    m_htmlOutputFormatter.createHtmlOutput(l_assignments, l_assignmentComparisonMap);

    LogManager::getSingletonInstance()->terminate();
}
