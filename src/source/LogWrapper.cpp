#include "LogWrapper.hpp"

LogWrapper::LogWrapper(const std::shared_ptr<std::ostream> &p_stream)
{
    if(p_stream)
    {
        m_logger = std::make_unique<Logger>(p_stream);
    }
    else
    {
        m_logger = nullptr;
    }
}
