#include "ApplicationBase.hpp"

#include "AssignmentComparisonMaker.hpp"
#include "FilesLookup.hpp"
#include "CommandLineParametersAnalyser.hpp"
#include "DiffComparisonMaker.hpp"
#include "Executor.hpp"
#include "FileAccessor.hpp"
#include "FileLoader.hpp"
#include "LineSplitter.hpp"
#include "PathProvider.hpp"

#include <iostream>
#include <AssignmentFactory.hpp>
#include <HtmlOutputMaker.hpp>
#include <WhitespaceFilter.hpp>
#include <AssignmentTokenizerFilter.hpp>
#include <KeywordsRemoverFilter.hpp>
#include <WordsSplitterFilter.hpp>
#include <LineTokenizerFilter.hpp>
#include <FunctionExtractorFilter.hpp>
#include <FileTokenizerFilter.hpp>

namespace
{
    std::unique_ptr<CommandLineParameters> getParametersInput(int argc, const char **argv)
    {
        auto l_parameterVector = std::make_unique<CommandLineParameters>();
        l_parameterVector->reserve(static_cast<u64>(argc - 1));
        for (int l_index = 1; l_index < argc; ++l_index)
        {
            l_parameterVector->push_back({argv[l_index]}); //NOLINT
        }
        return l_parameterVector;
    }
} // namespace

void ApplicationBase::launch(int argc, const char **argv)
{
    PathProvider l_pathProvider{argv[0]};
    FilesLookup l_filesLookup(l_pathProvider);
    CommandLineParametersAnalyser l_commandLineParametersAnalyser;
    FileAccessor l_fileAccessor;
    DiffComparisonMaker l_diffComparisonMaker{};
    AssignmentComparisonMaker l_assignmentComparisonMaker{l_diffComparisonMaker};
    FileLoader l_fileJoiner(l_fileAccessor);
    LineSplitter l_lineSplitter{};
    WhitespaceFilter l_whitespaceFilter{};
    AssignmentTokenizerFilter l_assignmentTokenizerFilter{};
    LineTokenizerFilter l_lineTokenizerFilter{};
    KeywordsRemoverFilter l_keywordsRemoverFilter{};
    WordsSplitterFilter l_wordsSplitterFilter{};
    FunctionExtractorFilter l_functionExtractorFilter{};
    FileTokenizerFilter l_fileTokenizerFilter{};
    AssignmentFactory l_assignmentFactory{l_fileJoiner,
                                          l_filesLookup,
                                          l_lineSplitter,
                                          l_whitespaceFilter,
                                          l_assignmentTokenizerFilter,
                                          l_lineTokenizerFilter,
                                          l_keywordsRemoverFilter,
                                          l_wordsSplitterFilter,
                                          l_functionExtractorFilter,
                                          l_fileTokenizerFilter};
    HtmlOutputMaker l_htmlOutputFormatter{l_fileAccessor, l_pathProvider};

    std::unique_ptr<CommandLineParameters> l_parameters = getParametersInput(argc, argv);

    Executor l_executor(
            l_assignmentComparisonMaker,
            l_assignmentFactory,
            l_commandLineParametersAnalyser,
            l_htmlOutputFormatter);

    try
    {
      l_executor.execute(*l_parameters);
    }
    catch(const std::runtime_error&)
    {
        std::cout << "Runtime exception received, aborting...";
    }
}
