#include "LineTokenizerFilter.hpp"

AssignmentStream LineTokenizerFilter::filter(const AssignmentStream &p_data) const
{
    AssignmentStream l_result{};

    for (const auto &l_entry : p_data)
    {
        std::map<std::string, std::string> l_dictionary;
        l_result.push_back({
            l_entry.getOriginFile(),
            l_entry.getOriginLine(),
            l_entry.getLength(),
            tokenizeWithDictionary(l_entry.getLineData(), l_dictionary)
        });
    }

    return l_result;
}
