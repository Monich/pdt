#include "CommandLineParametersAnalyser.hpp"

std::unique_ptr<CommandLineConfiguration> CommandLineParametersAnalyser::analyzeCommandLineParameters(const CommandLineParameters& p_parameters) const
{
    std::unique_ptr<CommandLineConfiguration> l_result = std::make_unique<CommandLineConfiguration>();

    for (const auto& l_input : p_parameters)
    {
        if(l_input == "-d")
        {
            l_result->areDebugLogsEnabled = true;
        }
        else if(l_input == "-s")
        {
            l_result->isSimplifiedModeOn = true;
        }
        else if(not l_result->singleAssignmentName)
        {
            l_result->singleAssignmentName = l_input;
        }
        else
        {
            throw CommandLineParametersException("Parameter " + l_input + " is not understandable");
        }
    }

    return std::move(l_result);
}

CommandLineParametersException::CommandLineParametersException(const std::string& p_message) : std::runtime_error(p_message)
{}
