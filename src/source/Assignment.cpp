#include "Assignment.hpp"

#include <utility>

#include "Logger.hpp"

FileLineData::FileLineData(Path p_originFile, u32 p_originLine, std::string p_lineData) :
        m_originFile(std::move(p_originFile)),
        m_originLine(p_originLine),
        m_length(1),
        m_lineData(std::move(p_lineData))
{}

FileLineData::FileLineData(Path p_originFile, u32 p_originLine, u32 p_length, std::string p_lineData) :
        m_originFile(std::move(p_originFile)),
        m_originLine(p_originLine),
        m_length(p_length),
        m_lineData(std::move(p_lineData))
{}

bool FileLineData::operator==(const FileLineData &p_rhs) const
{
    return m_originFile == p_rhs.m_originFile &&
           m_originLine == p_rhs.m_originLine &&
           m_length == p_rhs.m_length &&
           m_lineData == p_rhs.m_lineData;
}

bool FileLineData::operator!=(const FileLineData &p_rhs) const
{
    return !(p_rhs == *this);
}

const std::string &FileLineData::getLineData() const
{
    return m_lineData;
}

u32 FileLineData::getOriginLine() const
{
    return m_originLine;
}

const Path &FileLineData::getOriginFile() const
{
    return m_originFile;
}

u32 FileLineData::getLength() const
{
    return m_length;
}

u32 FileLineData::getLastOriginLine() const
{
    return m_originLine + m_length - 1;
}

Assignment::Assignment(std::string p_assignmentName,
                       AssignmentStream p_stepOneData) :
        m_assignmentName(std::move(p_assignmentName)),
        m_stepOneStream(std::move(p_stepOneData))
{}

Assignment::Assignment(std::string p_assignmentName,
                       AssignmentStream p_stepOneData,
                       AssignmentStream p_stepTwoData,
                       AssignmentStream p_commentData,
                       AssignmentStream p_tokenizedData,
                       AssignmentStream p_tokenizedLineData,
                       AssignmentStream p_noKeywordsData,
                       AssignmentStream p_identifiersData,
                       AssignmentStream p_functionExtractionData,
                       AssignmentStream p_functionTokenizedData,
                       AssignmentStream p_tokenizedFileData) :
        m_assignmentName(std::move(p_assignmentName)),
        m_stepOneStream(std::move(p_stepOneData)),
        m_stepTwoStream(std::move(p_stepTwoData)),
        m_commentStream(std::move(p_commentData)),
        m_tokenizedStream(std::move(p_tokenizedData)),
        m_tokenizedLineStream(std::move(p_tokenizedLineData)),
        m_noKeywordsStream(std::move(p_noKeywordsData)),
        m_identifiersStream(std::move(p_identifiersData)),
        m_functionExtractionStream(std::move(p_functionExtractionData)),
        m_functionTokenizedStream(std::move(p_functionTokenizedData)),
        m_tokenizedFileStream(std::move(p_tokenizedFileData))
{}

const std::string &Assignment::getAssignmentName() const
{
    return m_assignmentName;
}

const AssignmentStream &Assignment::getStepOneStream() const
{
    return m_stepOneStream;
}

const AssignmentStream &Assignment::getStepTwoStream() const
{
    return m_stepTwoStream;
}

bool Assignment::operator<(const Assignment &p_other) const
{
    return m_assignmentName < p_other.m_assignmentName;
}

bool Assignment::operator==(const Assignment &p_other) const
{
    return m_assignmentName == p_other.m_assignmentName &&
           m_stepOneStream == p_other.m_stepOneStream &&
            m_stepTwoStream == p_other.m_stepTwoStream &&
           m_commentStream == p_other.m_commentStream &&
           m_tokenizedStream == p_other.m_tokenizedStream &&
           m_tokenizedLineStream == p_other.m_tokenizedLineStream &&
           m_noKeywordsStream == p_other.m_noKeywordsStream &&
           m_identifiersStream == p_other.m_identifiersStream and
           m_functionExtractionStream == p_other.m_functionExtractionStream and
           m_functionTokenizedStream == p_other.m_functionTokenizedStream and
            m_tokenizedFileStream == p_other.m_tokenizedFileStream;
}

const AssignmentStream &Assignment::getCommentStream() const
{
    return m_commentStream;
}

const AssignmentStream &Assignment::getTokenizedStream() const
{
    return m_tokenizedStream;
}

const AssignmentStream &Assignment::getTokenizedLineStream() const
{
    return m_tokenizedLineStream;
}

const AssignmentStream &Assignment::getNoKeywordsStream() const
{
    return m_noKeywordsStream;
}

const AssignmentStream &Assignment::getIdentifiersStream() const
{
    return m_identifiersStream;
}

const AssignmentStream &Assignment::getFunctionExtractionStream() const
{
    return m_functionExtractionStream;
}

const AssignmentStream &Assignment::getFunctionTokenizedStream() const
{
    return m_functionTokenizedStream;
}

const AssignmentStream &Assignment::getTokenizedFileStream() const
{
    return m_tokenizedFileStream;
}

void operator<<(Logger& p_logger, const AssignmentFiles& p_input)
{
    if(p_input.empty())
    {
        p_logger << "empty assignment files set";
    }
    else
    {
        p_logger << "{ ";

        auto l_iterator = p_input.begin();
        p_logger << l_iterator->string();
        l_iterator++;

        for(; l_iterator != p_input.cend(); l_iterator++)
        {
            p_logger << ", " << l_iterator->string();
        }

        p_logger << " }";
    }
}
