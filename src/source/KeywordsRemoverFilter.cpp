#include "KeywordsRemoverFilter.hpp"

#include <sstream>

std::string KeywordsRemoverFilter::filterEntry(const std::string &p_entry) const
{
    std::ostringstream l_resultString;
    bool l_stringStarted{false};
    std::vector l_tokens = tokenize(p_entry);

    for (const auto &l_token : l_tokens)
    {
        if (isIdentifier(l_token))
        {
            if (l_stringStarted)
            {
                l_resultString << ' ';
            }

            l_resultString << l_token;
            l_stringStarted = true;
        }
    }

    return l_resultString.str();
}
