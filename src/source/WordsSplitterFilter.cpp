#include "WordsSplitterFilter.hpp"

#include <sstream>

AssignmentStream WordsSplitterFilter::filter(const AssignmentStream &p_data) const
{
    AssignmentStream l_result{};

    for (const auto &l_entry : p_data)
    {
        std::istringstream l_stringStream{l_entry.getLineData()};

        std::string l_temp;
        while(std::getline(l_stringStream, l_temp, ' '))
        {
            l_result.push_back({
                l_entry.getOriginFile(),
                l_entry.getOriginLine(),
                l_temp
            });
        }
    }

    return l_result;
}
