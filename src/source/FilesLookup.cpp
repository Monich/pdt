#include "FilesLookup.hpp"

#include "Assignment.hpp"
#include "CommonLogger.hpp"
#include "IPathProvider.hpp"

#include <boost/algorithm/string.hpp>

namespace
{
    bool isAssignment(const Path& p_directory)
    {
        return fs::is_directory(p_directory) && p_directory.filename().string() != ".pdt";
    }

    std::string getExtension(const std::string& p_file)
    {
        std::vector<std::string> l_splitString;
        boost::split(l_splitString, p_file, boost::is_any_of("."));
        return l_splitString[l_splitString.size()-1];
    }

    bool isCppFile(const Path& p_fileToCheck)
    {
        const std::set<std::string> EXTENSIONS{"cpp", "c", "hpp", "h"};

        const std::string l_fileExtension =
            getExtension(p_fileToCheck.string());

        for(const auto& l_extension : EXTENSIONS)
        {
            if (l_fileExtension == l_extension)
            {
                return true;
            }
        }

        return false;
    }

    void scanFilesInDirectory(std::set<Path>* p_files, const Path& p_pathToScan)
    {
        for(auto& l_file : fs::directory_iterator(p_pathToScan))
        {
            if(fs::is_directory(l_file))
            {
                scanFilesInDirectory(p_files, l_file.path());
            }
            else if(isCppFile(l_file))
            {
                p_files->insert(l_file);
            }
            else
            {
                LOG_DEBUG << "Ignoring file " << l_file << " - Not a .cpp file";
            }
        }
    }
} // namespace

FilesLookup::FilesLookup(const IPathProvider& p_currentDirectoryProvider) :
    m_currentDirectoryProvider(p_currentDirectoryProvider)
{}

std::set<Path> FilesLookup::scanFilesInPath(const Path & p_pathToBeScanned) const
{
    LOG_INFO << "Scanning files in directory " << p_pathToBeScanned;

    std::set<Path> l_files;
    scanFilesInDirectory(&l_files, p_pathToBeScanned);

    return l_files;
}

std::set<Path> FilesLookup::scanDirectoriesForAssignment() const
{
    LOG_INFO << "Creating assignments list";

    std::set<Path> l_assignments{};

    for(auto& l_directory : fs::directory_iterator(m_currentDirectoryProvider.getCurrentDirectory()))
    {
        const Path& l_dirPath = l_directory.path();
        if(isAssignment(l_dirPath))
        {
            LOG_INFO << "Assignment found: " << l_directory;
            l_assignments.insert(l_dirPath);
        }
        else
        {
            LOG_DEBUG << "Assignment denied - not a directory or PDT directory: " << l_directory;
        }
    }

    LOG_INFO << "Found " << l_assignments.size() << " assignments";

    return l_assignments;
}
