#include "CurrentTimestamp.hpp"

#include "Defines.hpp"
#include "Logger.hpp"

#include <ctime>

void operator<<(Logger& p_logger, const CurrentTimestamp& p_time) // NOLINT
{
    constexpr u32 l_bufferSize = 128;
    time_t l_rawTime;
    struct tm* l_timeInfo;
    char l_buffer[l_bufferSize];

    time(&l_rawTime);
    l_timeInfo = localtime(&l_rawTime);
    strftime(l_buffer, l_bufferSize, "<%FT%X>", l_timeInfo); // NOLINT
    p_logger << l_buffer; // NOLINT
}
