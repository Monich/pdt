#include "MarkGenerator.hpp"

#include "Assignment.hpp"
#include "AssignmentComparison.hpp"

u32 getDataLength(const Assignment& p_assignment, const LinesMarker& p_lineMarker)
{
    u32 l_length{0};
    for (const auto &l_originalData : p_assignment.getStepTwoStream())
    {
        if(l_originalData.getOriginFile() == p_lineMarker.fileName
           and l_originalData.getOriginLine() >= p_lineMarker.firstLine
           and l_originalData.getOriginLine() <= p_lineMarker.lastLine)
        {
            l_length += l_originalData.getLineData().size();
        }
    }

    return l_length;
}

MarkGenerator::MarkGenerator(u32 mRate, const Assignment &mFirstAssignment, const Assignment &mSecondAssignment)
        : m_rate(mRate), m_firstAssignment(mFirstAssignment), m_secondAssignment(mSecondAssignment) {}

void MarkGenerator::generateMark(DuplicateLinesMarker& p_linesMarker) const
{
    u32 l_length = getDataLength(m_firstAssignment, p_linesMarker.firstAssignmentMarker) +
                   getDataLength(m_secondAssignment, p_linesMarker.secondAssignmentMarker);

    p_linesMarker.mark = m_rate * l_length / 2;
}
