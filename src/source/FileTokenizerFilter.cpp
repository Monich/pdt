#include "FileTokenizerFilter.hpp"

AssignmentStream FileTokenizerFilter::filter(const AssignmentStream & p_data) const
{
    AssignmentStream l_result;
    AssignmentStream l_tempData;
    Path l_currentFile = p_data[0].getOriginFile();
    for (const auto &l_entry : p_data)
    {
        if(l_entry.getOriginFile() != l_currentFile)
        {
            l_currentFile = l_entry.getOriginFile();
            AssignmentStream l_fileResult = AssignmentTokenizerFilter::filter(l_tempData);
            l_result.insert(l_result.end(), l_fileResult.begin(), l_fileResult.end());
            l_tempData.clear();
        }

        l_tempData.push_back(l_entry);
    }

    AssignmentStream l_fileResult = AssignmentTokenizerFilter::filter(l_tempData);
    l_result.insert(l_result.end(), l_fileResult.begin(), l_fileResult.end());

    return l_result;
}