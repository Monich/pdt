#include "FileAccessor.hpp"

#include "CommonLogger.hpp"

#include <fstream>

namespace
{

template<typename T>
std::unique_ptr<T> createFileStream(const Path &p_path)
{
    auto l_stream = std::make_unique<T>(p_path);

    if(not *l_stream)
    {
        LOG_ERROR << "Couldn't access file " << p_path;
        return nullptr;
    }

    return std::move(l_stream);
}

} // namespace

std::unique_ptr<std::istream> FileAccessor::getInputStream(const Path &p_path) const
{
    return createFileStream<std::ifstream>(p_path);
}

std::unique_ptr<std::ostream> FileAccessor::getOutputStream(const Path &p_path) const
{
    return createFileStream<std::ofstream>(p_path);
}
