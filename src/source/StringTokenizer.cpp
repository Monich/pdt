#include "StringTokenizer.hpp"

#include "Defines.hpp"

#include <algorithm>
#include <sstream>
#include <vector>

namespace
{
const std::string NUMBERS{"1234567890"};
const std::string DELIMITERS{" .,()[]{}|&?:-+=;<>'\""};
const std::vector<std::string> KEYWORDS{
    "asm", "auto", "bool", "break",
    "case", "catch", "char", "class",
    "const", "const_cast", "continue", "default",
    "delete", "do", "double", "dynamic_cast",
    "else", "enum", "explicit", "export",
    "extern", "false", "float", "for",
    "friend", "goto", "if", "inline",
    "int", "long", "mutable", "namespace",
    "new", "operator", "private", "protected",
    "public", "register", "reinterpret_cast", "return",
    "short", "signed", "sizeof", "static",
    "static_cast", "struct", "switch", "template",
    "this", "throw", "true", "try",
    "typedef", "typeid", "typename", "union",
    "unsigned", "using", "virtual", "void",
    "volatile", "wchar_t", "while", "std",
    "and", "and_eq", "bitand", "bitor",
    "compl", "not", "not_eq", "or",
    "or_eq", "or", "xor", "xor_eq",
    "nullptr", "null"
};

std::string createNewDictionaryEntry(u64 p_seed)
{
    std::string l_result{"AAA"};
    constexpr u64 l_asciiRange = 82;

    u64 l_seed = p_seed;

    u8 l_firstLetterIncrement = l_seed / (l_asciiRange * l_asciiRange);
    l_seed -= l_firstLetterIncrement;

    u8 l_secondLetterIncrement = l_seed / l_asciiRange;
    l_seed -= l_secondLetterIncrement;

    u8 l_thirdLetterIncrement = l_seed;

    l_result[0] += l_firstLetterIncrement;
    l_result[1] += l_secondLetterIncrement;
    l_result[2] += l_thirdLetterIncrement;

    return l_result;
}
}

std::string StringTokenizer::tokenizeWithDictionary(const std::string &p_str, std::map<std::string, std::string> &p_dictionary) const
{
    std::vector<std::string> l_tokens = tokenize(p_str);

    std::ostringstream l_dataCreator{};
    for (const auto &l_token : l_tokens)
    {
        l_dataCreator << toDictionaryString(l_token, p_dictionary);
    }

    return l_dataCreator.str();
}

std::vector<std::string> StringTokenizer::tokenize(const std::string &p_str) const
{
    std::vector<std::string> l_result;
    int l_tokenStart = 0;
    int l_delimPos = p_str.find_first_of(DELIMITERS);

    while (l_delimPos != std::string::npos)
    {
        std::string l_tok = p_str.substr(l_tokenStart, l_delimPos - l_tokenStart);
        if (not l_tok.empty())
        {
            l_result.push_back(l_tok);
        }
        l_result.push_back(std::string{p_str[l_delimPos]});
        l_delimPos++;
        l_tokenStart = l_delimPos;
        l_delimPos = p_str.find_first_of(DELIMITERS, l_delimPos);
    }

    std::string l_tok = p_str.substr(l_tokenStart, l_delimPos - l_tokenStart);
    l_result.push_back(l_tok);
    return l_result;
}

std::string
StringTokenizer::toDictionaryString(const std::string &p_input, std::map<std::string, std::string> &p_dictionary) const
{
    if(not isIdentifier(p_input))
        return p_input;

    auto l_iterator = p_dictionary.find(p_input);
    if (l_iterator != p_dictionary.end())
        return l_iterator->second;

    std::string l_newEntry = createNewDictionaryEntry(p_dictionary.size());
    p_dictionary[p_input] = l_newEntry;
    return l_newEntry;
}

bool StringTokenizer::isIdentifier(const std::string &p_input) const
{
    return p_input.find_first_not_of(DELIMITERS) != std::string::npos and
           p_input.find_first_not_of(NUMBERS) != std::string::npos and
           std::find(KEYWORDS.begin(), KEYWORDS.end(), p_input) == KEYWORDS.end();
}

void StringTokenizer::removeWhitespacesOnLineStartAndEnd(std::string &p_line) const
{
    const auto l_strBegin = p_line.find_first_not_of(" \t");
    if (l_strBegin == std::string::npos)
        return;

    const auto l_strEnd = p_line.find_last_not_of(" \t");
    const auto l_strRange = l_strEnd - l_strBegin + 1;

    p_line = p_line.substr(l_strBegin, l_strRange);
}

std::string StringTokenizer::removeWhitespacesOnLineStartAndEnd(const std::string &p_line) const
{
    std::string l_result{p_line};
    removeWhitespacesOnLineStartAndEnd(l_result);
    return l_result;
}
