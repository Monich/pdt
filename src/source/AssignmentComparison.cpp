#include "AssignmentComparison.hpp"

#include <utility>

AssignmentComparison::AssignmentComparison(DiffComparison p_firstStepComparison) :
        m_firstStepComparison(std::move(p_firstStepComparison))
{}

AssignmentComparison::AssignmentComparison(DiffComparison p_firstStepComparison,
                                           DiffComparison p_secondStepComparison,
                                           DiffComparison p_commentComparison,
                                           DiffComparison p_tokenizedComparison,
                                           DiffComparison p_tokenizedLineComparison,
                                           DiffComparison p_noKeywordsComparison,
                                           DiffComparison p_identifiersComparison,
                                           DiffComparison p_functionExtractionComparison,
                                           DiffComparison p_functionTokenizedComparison,
                                           DiffComparison p_tokenizedFileComparison,
                                           u32 firstAssignmentLineCount,
                                           u32 secondAssignmentLineCount) :
        m_firstStepComparison(std::move(p_firstStepComparison)),
        m_secondStepComparison(std::move(p_secondStepComparison)),
        m_commentComparison(std::move(p_commentComparison)),
        m_tokenizedComparison(std::move(p_tokenizedComparison)),
        m_tokenizedLineComparison(std::move(p_tokenizedLineComparison)),
        m_noKeywordsComparison(std::move(p_noKeywordsComparison)),
        m_identifiersComparison(std::move(p_identifiersComparison)),
        m_functionExtractionComparison(std::move(p_functionExtractionComparison)),
        m_functionTokenizedComparison(std::move(p_functionTokenizedComparison)),
        m_tokenizedFileComparison(std::move(p_tokenizedFileComparison))
{
    calculateRates(firstAssignmentLineCount, secondAssignmentLineCount);
    calculateWeight();
}

const DiffComparison &AssignmentComparison::getFirstStepComparison() const
{
    return m_firstStepComparison;
}

const DiffComparison &AssignmentComparison::getSecondStepComparison() const
{
    return m_secondStepComparison;
}

const DiffComparison &AssignmentComparison::getCommentComparison() const
{
    return m_commentComparison;
}

const DiffComparison &AssignmentComparison::getTokenizedComparison() const
{
    return m_tokenizedComparison;
}

const DiffComparison &AssignmentComparison::getTokenizedLineComparison() const
{
    return m_tokenizedLineComparison;
}

const DiffComparison &AssignmentComparison::getNoKeywordsComparison() const
{
    return m_noKeywordsComparison;
}

const DiffComparison &AssignmentComparison::getIdentifiersComparison() const
{
    return m_identifiersComparison;
}

const DiffComparison &AssignmentComparison::getFunctionExtractionComparison() const
{
    return m_functionExtractionComparison;
}

const DiffComparison &AssignmentComparison::getFunctionTokenizedComparison() const
{
    return m_functionTokenizedComparison;
}

const DiffComparison &AssignmentComparison::getTokenizedFileComparison() const
{
    return m_tokenizedFileComparison;
}

void fillSet(std::set<std::pair<Path, u32>>& p_set, const LinesMarker& p_linesMarker)
{
    for (unsigned l_i = p_linesMarker.firstLine; l_i < p_linesMarker.lastLine + 1; ++l_i)
    {
        p_set.insert(std::make_pair(p_linesMarker.fileName, l_i));
    }
}

void fillSet(std::set<std::pair<Path, u32>>& p_firstSet, std::set<std::pair<Path, u32>>& p_secondSet, const DiffComparison& p_diffComparison)
{
    for (const auto &l_duplicate : p_diffComparison.duplicates)
    {
        if(l_duplicate.mark > 50)
        {
            fillSet(p_firstSet, l_duplicate.firstAssignmentMarker);
            fillSet(p_secondSet, l_duplicate.secondAssignmentMarker);
        }
    }
}

void AssignmentComparison::calculateRates(u32 firstAssignmentLineCount, u32 secondAssignmentLineCount)
{
    std::set<std::pair<Path, u32>> l_firstSet;
    std::set<std::pair<Path, u32>> l_secondSet;

    fillSet(l_firstSet, l_secondSet, m_firstStepComparison);
    fillSet(l_firstSet, l_secondSet, m_secondStepComparison);
    fillSet(l_firstSet, l_secondSet, m_commentComparison);
    fillSet(l_firstSet, l_secondSet, m_tokenizedComparison);
    fillSet(l_firstSet, l_secondSet, m_tokenizedLineComparison);
    fillSet(l_firstSet, l_secondSet, m_functionExtractionComparison);
    fillSet(l_firstSet, l_secondSet, m_functionTokenizedComparison);
    fillSet(l_firstSet, l_secondSet, m_tokenizedFileComparison);

    m_firstAssignmentPercent = static_cast<double>(l_firstSet.size()) * 100 / firstAssignmentLineCount;
    m_secondAssignmentPercent = static_cast<double>(l_secondSet.size()) * 100 / secondAssignmentLineCount;
}

void AssignmentComparison::calculateWeight()
{
    m_weight = m_firstStepComparison.getWeight()
        + m_secondStepComparison.getWeight()
        + m_commentComparison.getWeight()
        + m_tokenizedComparison.getWeight()
        + m_tokenizedLineComparison.getWeight()
        + m_functionExtractionComparison.getWeight()
        + m_functionTokenizedComparison.getWeight()
        + m_tokenizedFileComparison.getWeight();
}

u64 AssignmentComparison::getWeight() const
{
    return m_weight;
}

double AssignmentComparison::getFirstAssignmentPercent() const
{
    return m_firstAssignmentPercent;
}

double AssignmentComparison::getSecondAssignmentPercent() const
{
    return m_secondAssignmentPercent;
}

u64 DiffComparison::getWeight()
{
    u64 l_result{0};

    for (const auto &l_item : duplicates)
    {
        if(l_item.mark > 50)
        {
            l_result += l_item.mark;
        }
    }

    return l_result;
}

bool LinesMarker::operator==(const LinesMarker &p_other) const
{
    return p_other.fileName == fileName
        and p_other.firstLine == firstLine
        and p_other.lastLine == lastLine;
}
