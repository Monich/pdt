#include "AssignmentTokenizerFilter.hpp"

AssignmentStream AssignmentTokenizerFilter::filter(const AssignmentStream &p_data) const
{
    AssignmentStream l_result{};
    std::map<std::string, std::string> l_dictionary;

    for (const auto &l_entry : p_data)
    {
        l_result.push_back({
            l_entry.getOriginFile(),
            l_entry.getOriginLine(),
            tokenizeWithDictionary(l_entry.getLineData(), l_dictionary)
        });
    }

    return l_result;
}
