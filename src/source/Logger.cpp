#include <iostream>
#include "Logger.hpp"

Logger::Logger(std::weak_ptr<std::ostream> p_flushStream) : m_flushStream(std::move(p_flushStream))
{}

Logger::~Logger()
{
    if(auto l_flushStream = m_flushStream.lock())
    {
        *this << std::endl;
        const std::string& l_buffer = str();
        std::cout << l_buffer;
        *l_flushStream << l_buffer;
    }
}
