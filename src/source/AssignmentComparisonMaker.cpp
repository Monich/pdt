#include <MarkGenerator.hpp>
#include "AssignmentComparisonMaker.hpp"

#include "Assignment.hpp"
#include "AssignmentComparison.hpp"
#include "CommonLogger.hpp"
#include "IDiffComparisonMaker.hpp"

AssignmentComparisonMaker::AssignmentComparisonMaker(IDiffComparisonMaker &p_diffComparisonMaker) :
    m_diffComparisonMaker(p_diffComparisonMaker)
{}

AssignmentComparison AssignmentComparisonMaker::compareAssignments(const Assignment &p_firstAssignment,
                                                                   const Assignment &p_secondAssignment) const
{
    LOG_INFO << "Comparing assignments: " << p_firstAssignment.getAssignmentName() << " and " << p_secondAssignment.getAssignmentName();

    LOG_DEBUG << "Conducting first step comparison";
    auto l_firstStepComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getStepOneStream(),
                                                                   p_secondAssignment.getStepOneStream(),
                                                                   MarkGenerator(3, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting second step comparison";
    auto l_secondStepComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getStepTwoStream(),
                                                                    p_secondAssignment.getStepTwoStream(),
                                                                    MarkGenerator(2, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting comment comparison";
    auto l_commentComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getCommentStream(),
                                                                 p_secondAssignment.getCommentStream(),
                                                                 MarkGenerator(5, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting tokenized data comparison";
    auto l_tokenizedDataComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getTokenizedStream(),
                                                                       p_secondAssignment.getTokenizedStream(),
                                                                       MarkGenerator(4, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting tokenized lines data comparison";
    auto l_tokenizedLineDataComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getTokenizedLineStream(),
                                                                           p_secondAssignment.getTokenizedLineStream(),
                                                                           MarkGenerator(1, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting data with no keywords comparison";
    auto l_noKeywordsComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getNoKeywordsStream(),
                                                                    p_secondAssignment.getNoKeywordsStream(),
                                                                    MarkGenerator(0, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting identifiers comparison";
    auto l_identifiersComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getIdentifiersStream(),
                                                                     p_secondAssignment.getIdentifiersStream(),
                                                                     MarkGenerator(0, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting functions comparison";
    auto l_functionsComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getFunctionExtractionStream(),
                                                                   p_secondAssignment.getFunctionExtractionStream(),
                                                                   MarkGenerator(4, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting functions tokenized comparison";
    auto l_functionsTokenizedComparison = m_diffComparisonMaker.compareData(
            p_firstAssignment.getFunctionTokenizedStream(),
            p_secondAssignment.getFunctionTokenizedStream(), MarkGenerator(3, p_firstAssignment, p_secondAssignment));

    LOG_DEBUG << "Conducting tokenized files comparison";
    auto l_tokenizedFileComparison = m_diffComparisonMaker.compareData(p_firstAssignment.getTokenizedFileStream(),
                                                                       p_secondAssignment.getTokenizedFileStream(),
                                                                       MarkGenerator(2, p_firstAssignment, p_secondAssignment));

    return {
            l_firstStepComparison,
            l_secondStepComparison,
            l_commentComparison,
            l_tokenizedDataComparison,
            l_tokenizedLineDataComparison,
            l_noKeywordsComparison,
            l_identifiersComparison,
            l_functionsComparison,
            l_functionsTokenizedComparison,
            l_tokenizedFileComparison,
            p_firstAssignment.getStepOneStream().size(),
            p_secondAssignment.getStepOneStream().size(),
    };
}
