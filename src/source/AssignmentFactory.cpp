#include "AssignmentFactory.hpp"

#include "CommonLogger.hpp"
#include "IFileLoader.hpp"
#include "IFilesLookup.hpp"
#include "IFilter.hpp"
#include "ILineSplitter.hpp"

AssignmentFactory::AssignmentFactory(const IFileLoader & p_fileLoader,
                                     const IFilesLookup &p_filesLookup,
                                     const ILineSplitter& p_lineSplitter,
                                     const IFilter& p_whitespaceFilter,
                                     const IFilter& p_assignmentTokenizerFilter,
                                     const IFilter& p_lineTokenizerFilter,
                                     const IFilter& p_keywordsRemoverFilter,
                                     const IFilter& p_wordsSplitterFilter,
                                     const IFilter& p_functionExtractionFilter,
                                     const IFilter& p_fileTokenizerFilter) :
        m_fileLoader(p_fileLoader),
        m_filesLookup(p_filesLookup),
        m_lineSplitter(p_lineSplitter),
        m_whitespaceFilter(p_whitespaceFilter),
        m_assignmentTokenizerFilter(p_assignmentTokenizerFilter),
        m_lineTokenizerFilter(p_lineTokenizerFilter),
        m_keywordsRemoverFilter(p_keywordsRemoverFilter),
        m_wordsSplitterFilter(p_wordsSplitterFilter),
        m_functionExtractionFilter(p_functionExtractionFilter),
        m_fileTokenizerFilter(p_fileTokenizerFilter)
{}

Assignments AssignmentFactory::loadAssignments(bool p_simplify) const
{
    LOG_INFO << "Creating assignments";
    Assignments l_result{};

    std::set<Path> l_pathsToAssignments{m_filesLookup.scanDirectoriesForAssignment()};

    for (const Path &l_assignmentPath : l_pathsToAssignments)
    {
        std::set<Path> l_assignmentFiles{m_filesLookup.scanFilesInPath(l_assignmentPath)};

        if(l_assignmentFiles.empty())
        {
            LOG_WARN << "No files specified for assignment " << l_assignmentPath << ", skipping it";
            continue;
        }

        auto l_assignmentName = l_assignmentPath.filename().string();

        LOG_INFO << "Filtering assignment " << l_assignmentName;

        LOG_DEBUG << "Loading assignment data for " << l_assignmentName;
        AssignmentStream l_stepOneData{m_fileLoader.loadAssignmentData(l_assignmentFiles)};

        LOG_DEBUG << "Line removal (stage 2) ongoing for assignment " << l_assignmentName;
        auto l_codeAndCommentsData = m_lineSplitter.splitCodeAndComments(l_stepOneData);
        const AssignmentStream& l_stepTwoData = l_codeAndCommentsData.first;

        LOG_DEBUG << "Tokenizing assignment " << l_assignmentName << " by line";
        AssignmentStream l_tokenizedLineDataWithSpaces{m_lineTokenizerFilter.filter(l_stepTwoData)};

        LOG_DEBUG << "Removing whitespaces for tokenized lines in assignment " << l_assignmentName;
        AssignmentStream l_tokenizedLineAssignmentData{m_whitespaceFilter.filter(l_tokenizedLineDataWithSpaces)};

        AssignmentStream l_tokenizedAssignmentData;
        AssignmentStream l_functionExtractionData;
        AssignmentStream l_functionTokenizedData;
        AssignmentStream l_fileTokenizedData;
        if(not p_simplify)
        {
            LOG_DEBUG << "Tokenizing assignment " << l_assignmentName;
            AssignmentStream l_tokenizedAssignmentDataWithSpaces{m_assignmentTokenizerFilter.filter(l_stepTwoData)};

            LOG_DEBUG << "Removing whitespaces for tokenized assignment " << l_assignmentName;
            l_tokenizedAssignmentData = m_whitespaceFilter.filter(l_tokenizedAssignmentDataWithSpaces);

            LOG_DEBUG << "Extracting function data from " << l_assignmentName;
            l_functionExtractionData = m_functionExtractionFilter.filter(l_stepTwoData);

            LOG_DEBUG << "Tokenizing extracted function data from " << l_assignmentName;
            AssignmentStream l_functionExtractionTokenizedDataWithSpaces{m_lineTokenizerFilter.filter(l_functionExtractionData)};

            LOG_DEBUG << "Removing spaces from tokenized extracted function data for " << l_assignmentName;
            l_functionTokenizedData = m_whitespaceFilter.filter(l_functionExtractionTokenizedDataWithSpaces);

            LOG_DEBUG << "Tokenizing file data for " << l_assignmentName;
            AssignmentStream l_fileTokenizedDataWithSpaces{m_fileTokenizerFilter.filter(l_stepTwoData)};

            LOG_DEBUG << "Removing spaces from tokenized file data for " << l_assignmentName;
            l_fileTokenizedData = m_whitespaceFilter.filter(l_fileTokenizedDataWithSpaces);
        }

        LOG_INFO << "Finished filtering assignment " << l_assignmentName;

        l_result.push_back(Assignment(
            std::move(l_assignmentName),
            std::move(l_stepOneData),
            std::move(l_codeAndCommentsData.first),
            std::move(l_codeAndCommentsData.second),
            std::move(l_tokenizedAssignmentData),
            std::move(l_tokenizedLineAssignmentData),
            {},
            {},
            std::move(l_functionExtractionData),
            std::move(l_functionTokenizedData),
            std::move(l_fileTokenizedData)
        ));
    }

    return l_result;
}
