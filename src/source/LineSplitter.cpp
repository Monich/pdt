#include "LineSplitter.hpp"

#include "Assignment.hpp"
#include "CommonLogger.hpp"

#include <algorithm>
#include <cctype>
#include <fstream>

namespace
{

    void removeIncludes(std::string &p_line)
    {
        if(p_line.rfind("#include", 0) == 0)
        {
            p_line.clear();
        }
    }

    bool splitComments(std::string &p_line, bool p_isInsideComment, std::string& p_comment)
    {
        bool l_isInsideString = false;
        bool l_commentSignEncountered = false;
        bool l_isInsideComment = p_isInsideComment;
        u32 l_commentStartIdx = 0;

        for (u32 l_index = 0; l_index < p_line.size(); ++l_index)
        {
            if(l_isInsideString)
            {
                l_isInsideString = p_line[l_index] != '"';
            }
            else
            {
                if(l_isInsideComment)
                {
                    if(l_commentSignEncountered)
                    {
                        if(p_line[l_index] == '/')
                        {
                            if(l_isInsideComment != p_isInsideComment)
                            { // This means that this comment started in current method - add space
                                p_line[l_commentStartIdx] = ' ';
                                p_comment = p_line.substr(l_commentStartIdx + 2, l_index - l_commentStartIdx - 3);
                                p_line.erase(l_commentStartIdx + 1, l_index - l_commentStartIdx);
                            }
                            else
                            {
                                p_comment = p_line.substr(0, l_index - l_commentStartIdx - 1);
                                p_line.erase(l_commentStartIdx, l_index - l_commentStartIdx + 1);
                            }
                            l_isInsideComment = false;
                            l_commentSignEncountered = false;
                        }
                    }
                    else
                    {
                        l_commentSignEncountered = p_line[l_index] == '*';
                    }
                }
                else if(l_commentSignEncountered)
                {
                    if(p_line[l_index] == '/')
                    {
                        p_comment = p_line.substr(l_index + 1, std::string::npos);
                        p_line.erase(l_index - 1, p_line.size() - l_index + 1);
                        return false;
                    }
                    else if(p_line[l_index] == '*')
                    {
                        l_isInsideComment = true;
                        l_commentSignEncountered = false;
                        l_commentStartIdx = l_index - 1;
                    }
                }
                else
                {
                    l_commentSignEncountered = p_line[l_index] == '/';
                    l_isInsideString = p_line[l_index] == '"';
                }
            }
        }

        if(l_isInsideComment)
        {
            if(p_isInsideComment)
            {
                p_comment = p_line.substr(l_commentStartIdx);
            }
            else
            {
                p_comment = p_line.substr(l_commentStartIdx + 2);
            }

            p_line.erase(l_commentStartIdx, p_line.size() - l_commentSignEncountered);
        }

        return l_isInsideComment;
    }

} // namespace

std::pair<AssignmentStream, AssignmentStream> LineSplitter::splitCodeAndComments(const AssignmentStream &p_stepOneData) const
{
    AssignmentStream l_codeData{};
    AssignmentStream l_commentsData{};
    bool l_isInsideMultiLineComment = false;

    for (const auto &data : p_stepOneData)
    {
        std::string l_temp{data.getLineData()};
        std::string l_comment{};
        removeIncludes(l_temp);
        l_isInsideMultiLineComment = splitComments(l_temp, l_isInsideMultiLineComment, l_comment);
        std::transform(l_temp.begin(), l_temp.end(), l_temp.begin(), [](char c){return std::tolower(c);});

        removeWhitespacesOnLineStartAndEnd(l_temp);
        removeWhitespacesOnLineStartAndEnd(l_comment);

        if(not l_temp.empty())
        {
            l_codeData.push_back({
                    data.getOriginFile(),
                    data.getOriginLine(),
                    l_temp
            });
        }
        if(not l_comment.empty())
        {
            l_commentsData.push_back({
                    data.getOriginFile(),
                    data.getOriginLine(),
                    l_comment
            });
        }
    }

    return std::make_pair( l_codeData, l_commentsData );
}
