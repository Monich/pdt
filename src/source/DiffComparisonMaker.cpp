#include "DiffComparisonMaker.hpp"

#include "AssignmentComparison.hpp"

#include <algorithm>
#include <map>
#include <IMarkGenerator.hpp>

namespace
{

struct SingleDuplicateLineMarker
{
    const FileLineData& firstAssignmentMarker;
    const FileLineData& secondAssignmentMarker;

    SingleDuplicateLineMarker(const FileLineData &p_firstAssignmentData, const FileLineData &p_secondAssignmentData) :
            firstAssignmentMarker(p_firstAssignmentData),
            secondAssignmentMarker(p_secondAssignmentData)
    {}

    [[nodiscard]] SingleDuplicateLineMarker reversed() const
    {
        return {
            secondAssignmentMarker,
            firstAssignmentMarker
        };
    }
};

using SingleDuplicateLineMarkers = std::vector<SingleDuplicateLineMarker>;

void loadLinesIntoSet(const AssignmentStream &p_data, std::set<std::string> &p_set)
{
    for (const auto &l_item : p_data)
    {
        p_set.insert(l_item.getLineData());
    }
}

void computeSingleLineDuplicates(SingleDuplicateLineMarkers &p_duplicates,
                                 const AssignmentStream &p_firstData,
                                 const AssignmentStream &p_secondData)
{
    for (const auto &l_item1 : p_firstData)
    {
        for (const auto &l_item2 : p_secondData)
        {
            if(l_item1.getLineData() == l_item2.getLineData())
            {
                p_duplicates.push_back(SingleDuplicateLineMarker(l_item1, l_item2));
            }
        }
    }
}

std::pair<u32, u32> getMinAndMaxLineOfFirstAssignment(const SingleDuplicateLineMarkers &p_singleLineSequenceCollection)
{
    u32 l_minLine{UINT32_MAX};
    u32 l_maxLine{0};

    for (const auto &item : p_singleLineSequenceCollection) {
        l_minLine = std::min(l_minLine, item.firstAssignmentMarker.getOriginLine());
        l_maxLine = std::max(l_maxLine, item.firstAssignmentMarker.getLastOriginLine());
    }

    return std::make_pair(l_minLine, l_maxLine);
}

std::pair<u32, u32> getMinAndMaxLineOfSecondAssignment(const SingleDuplicateLineMarkers &p_singleLineSequenceCollection)
{
    u32 l_minLine{UINT32_MAX};
    u32 l_maxLine{0};

    for (const auto &item : p_singleLineSequenceCollection) {
        l_minLine = std::min(l_minLine, item.secondAssignmentMarker.getOriginLine());
        l_maxLine = std::max(l_maxLine, item.secondAssignmentMarker.getLastOriginLine());
    }

    return std::make_pair(l_minLine, l_maxLine);
}

void addDuplicateLinesMarker(DuplicateLinesMarkers &p_duplicates,
                             const SingleDuplicateLineMarkers &p_singleLineSequenceCollection)
{
    auto l_firstAssignmentStats = getMinAndMaxLineOfFirstAssignment(p_singleLineSequenceCollection);
    auto l_secondAssignmentStats = getMinAndMaxLineOfSecondAssignment(p_singleLineSequenceCollection);

    p_duplicates.push_back({
        {
            p_singleLineSequenceCollection.front().firstAssignmentMarker.getOriginFile(),
            l_firstAssignmentStats.first,
            l_firstAssignmentStats.second
        },
        {
               p_singleLineSequenceCollection.front().secondAssignmentMarker.getOriginFile(),
               l_secondAssignmentStats.first,
               l_secondAssignmentStats.second
        },
    });
}

void sortSingleLineDuplicates(DuplicateLinesMarkers &p_resultDuplicates,
                              const SingleDuplicateLineMarkers &p_singleLineDuplicates,
                              const AssignmentStream &p_currentFileData,
                              const AssignmentStream &p_secondFileData,
                              bool p_isFirstSplitDone);

void checkAndAddDuplicateLinesMarker(DuplicateLinesMarkers &p_resultDuplicates,
                                     const SingleDuplicateLineMarkers &p_singleLineDuplicates,
                                     const AssignmentStream &p_lastFileData,
                                     const AssignmentStream &p_otherFileData,
                                     bool p_isFirstSplitDone)
{
    if(p_isFirstSplitDone)
    {
        addDuplicateLinesMarker(p_resultDuplicates, p_singleLineDuplicates);
    }
    else
    {
        sortSingleLineDuplicates(p_resultDuplicates,
                                 p_singleLineDuplicates,
                                 p_otherFileData,
                                 p_lastFileData,
                                 true);
    }
}

bool isPrimaryFileChanged(const SingleDuplicateLineMarkers &p_singleLineSequenceCollection,
                          const FileLineData &p_newLine)
{
    return not p_singleLineSequenceCollection.empty()
           and p_singleLineSequenceCollection.front().secondAssignmentMarker.getOriginFile() != p_newLine.getOriginFile();
}

// Forward declared
void sortSingleLineDuplicates(DuplicateLinesMarkers &p_resultDuplicates,
                              const SingleDuplicateLineMarkers &p_singleLineDuplicates,
                              const AssignmentStream &p_currentFileData,
                              const AssignmentStream &p_secondFileData,
                              bool p_isFirstSplitDone)
{
    // Znajdowanie stałych fragmentów zduplikowanych
    SingleDuplicateLineMarkers l_singleLineSequenceCollection;
    bool l_isSingleLineSequenceOngoing{ false };

    for (const FileLineData& l_currentFileLine : p_currentFileData)
    {
        if(isPrimaryFileChanged(l_singleLineSequenceCollection, l_currentFileLine))
        {
            l_isSingleLineSequenceOngoing = false;
        }

        if (not (l_isSingleLineSequenceOngoing or l_singleLineSequenceCollection.empty()))
        {
            checkAndAddDuplicateLinesMarker(p_resultDuplicates,
                l_singleLineSequenceCollection,
                p_currentFileData,
                p_secondFileData,
                p_isFirstSplitDone);
            l_singleLineSequenceCollection.clear();
        }

        l_isSingleLineSequenceOngoing = false;
        for (const SingleDuplicateLineMarker& l_duplicateEntry : p_singleLineDuplicates)
        {
            if(l_duplicateEntry.firstAssignmentMarker == l_currentFileLine)
            {
                l_isSingleLineSequenceOngoing = true;
                l_singleLineSequenceCollection.push_back(l_duplicateEntry.reversed());
            }
        }
    }

    if(not l_singleLineSequenceCollection.empty())
        checkAndAddDuplicateLinesMarker(p_resultDuplicates,
                                        l_singleLineSequenceCollection,
                                        p_currentFileData,
                                        p_secondFileData,
                                        p_isFirstSplitDone);
}

DuplicateLinesMarkers computeDuplicateLines(const AssignmentStream &p_firstData, const AssignmentStream &p_secondData, u32 p_linesHit)
{
    DuplicateLinesMarkers l_result;
    SingleDuplicateLineMarkers l_singleLineDuplicates;
    l_singleLineDuplicates.reserve(p_linesHit);

    computeSingleLineDuplicates(l_singleLineDuplicates, p_firstData, p_secondData);
    sortSingleLineDuplicates(l_result, l_singleLineDuplicates, p_firstData, p_secondData, false);

    return l_result;
}

} // namespace

DiffComparison
DiffComparisonMaker::compareData(const AssignmentStream &p_firstData, const AssignmentStream &p_secondData,
                                 const IMarkGenerator &p_markGenerator) const
{
    std::set<std::string> l_stringSet{};
    loadLinesIntoSet(p_firstData, l_stringSet);
    loadLinesIntoSet(p_secondData, l_stringSet);

    u32 l_linesHit = p_firstData.size() + p_secondData.size() - l_stringSet.size();
    DuplicateLinesMarkers l_duplicates;
    if(l_linesHit)
    {
        l_duplicates = computeDuplicateLines(p_firstData, p_secondData, l_linesHit);

        for (auto &l_duplicate : l_duplicates)
        {
            p_markGenerator.generateMark(l_duplicate);
        }
    }

    return { l_linesHit,
             std::max(p_firstData.size(), p_secondData.size()),
             l_duplicates
    };
}
