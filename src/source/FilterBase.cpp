#include "FilterBase.hpp"

AssignmentStream FilterBase::filter(const AssignmentStream &p_data) const
{
    AssignmentStream l_result{};

    for (const FileLineData &l_lineData : p_data)
    {
        l_result.push_back({
                                   l_lineData.getOriginFile(),
                                   l_lineData.getOriginLine(),
                                   l_lineData.getLength(),
                                   filterEntry(l_lineData.getLineData())
        });
    }

    return l_result;
}
