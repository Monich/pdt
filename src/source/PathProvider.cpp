#include "PathProvider.hpp"

#include "Assignment.hpp"

#include <sstream>

PathProvider::PathProvider(const Path &p_applicationPath) :
    m_applicationUtilitiesDirectory(p_applicationPath.parent_path().append("html"))
{}

Path PathProvider::getCurrentDirectory() const
{
    return fs::current_path();
}

Path PathProvider::getIndexHeader() const
{
    return getCurrentDirectory().append("index.html");
}

Path PathProvider::getIndexSiteHeaderPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("index_pieces").append("header.html");
    return l_tempPath;
}

Path PathProvider::getIndexSiteEntryPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("index_pieces").append("entry.html");
    return l_tempPath;
}

Path PathProvider::getIndexSiteFooterPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("index_pieces").append("footer.html");
    return l_tempPath;
}

Path PathProvider::getComparisonSiteHeaderPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("assignment_pieces").append("header.html");
    return l_tempPath;
}

Path PathProvider::getComparisonSiteEntryPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("assignment_pieces").append("entry.html");
    return l_tempPath;
}

Path PathProvider::getComparisonSiteFooterPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("assignment_pieces").append("footer.html");
    return l_tempPath;
}

Path PathProvider::getDetailsSiteFooterPath() const
{
    Path l_tempPath = m_applicationUtilitiesDirectory;
    l_tempPath.append("details.html");
    return l_tempPath;
}

Path PathProvider::getComparisonOutputPage(const Assignment &p_first, const Assignment &p_second) const
{
    std::stringstream l_filename;
    l_filename << p_second.getAssignmentName();
    l_filename << ".html";
    return getCurrentDirectory().append(".pdt").append(p_first.getAssignmentName()).append(l_filename.str());
}
