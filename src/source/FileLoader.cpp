#include "FileLoader.hpp"

#include "Assignment.hpp"
#include "CommonLogger.hpp"
#include "IFileAccessor.hpp"

FileLoader::FileLoader(const IFileAccessor &p_fileAccessor):
    m_fileAccessor(p_fileAccessor)
{}

AssignmentStream FileLoader::loadAssignmentData(const std::set<Path> &p_filesToLoad) const
{
    AssignmentStream l_result{};

    for (const Path& l_fileToJoin : p_filesToLoad)
    {
        LOG_DEBUG << "Loading file " << l_fileToJoin;
        auto l_inputFile = m_fileAccessor.getInputStream(l_fileToJoin);

        if(l_inputFile)
        {
            std::string l_tempString;
            u32 l_lineCount{0};
            while(std::getline(*l_inputFile, l_tempString))
            {
                l_lineCount++;
                l_result.push_back({
                        l_fileToJoin,
                        l_lineCount,
                        l_tempString
                });
            }
            LOG_DEBUG << "Loaded " << l_lineCount << " from " << l_fileToJoin;
        }
        else
        {
            LOG_ERROR << "File " << l_fileToJoin << " couldn't be opened";
            LOG_WARN << "File " << l_fileToJoin << " will be skipped and not analyzed";
        }
    }

    return l_result;
}
