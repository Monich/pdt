#include "WhitespaceFilter.hpp"

#include <algorithm>

std::string WhitespaceFilter::filterEntry(const std::string &p_entry) const
{
    std::string l_str{p_entry};
    l_str.erase(std::remove(l_str.begin(), l_str.end(), ' '), l_str.end());
    l_str.erase(std::remove(l_str.begin(), l_str.end(), '\t'), l_str.end());
    return l_str;
}
