#include "LogManager.hpp"

#include "LogWrapper.hpp"

LogManager::~LogManager()
{ terminate();
}

bool LogManager::initialize(bool p_shouldLogDebugLogs)
{
    m_fileLog.reset();

    m_shouldLogDebugLogs = p_shouldLogDebugLogs;

    m_fileLog = std::make_shared<std::ofstream>("pdt.log");

    return isInitialized();
}

void LogManager::terminate()
{
    if (m_fileLog)
    {
        m_fileLog->close();
    }
    m_fileLog.reset();
}

bool LogManager::isInitialized() const
{
    return m_fileLog && m_fileLog->is_open();
}

std::unique_ptr<LogWrapper> LogManager::getLogWithSeverity(ELogSeverity p_severity) const
{
    std::shared_ptr<std::ostream> l_stream;
    if (!isInitialized() || (p_severity == ELogSeverity_Debug && !m_shouldLogDebugLogs))
    {
        l_stream = nullptr;
    }
    else
    {
        l_stream = m_fileLog;
    }

    return std::make_unique<LogWrapper>(l_stream);
}

LogManager * LogManager::getSingletonInstance()
{
    static LogManager l_manager;
    return &l_manager;
}
