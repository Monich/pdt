#include "FunctionExtractorFilter.hpp"

#include <sstream>
#include <stack>

struct FunctionExtractorHelper
{
    std::ostringstream stream;
    u32 lineStarted;
};

AssignmentStream FunctionExtractorFilter::filter(const AssignmentStream &p_data) const
{
    AssignmentStream l_result{};
    std::stack<FunctionExtractorHelper> l_stringStreamStack{};

    Path l_currentPath{};

    for (const auto &l_entry : p_data)
    {
        if(l_currentPath != l_entry.getOriginFile())
        {
            while(not l_stringStreamStack.empty())
            {
                l_stringStreamStack.pop();
            }
            l_stringStreamStack.push({std::ostringstream{}, 0});
            l_currentPath = l_entry.getOriginFile();
        }
        else
        {
            l_stringStreamStack.top().stream << ' ';
        }

        std::vector<std::string> l_tokens = tokenize(l_entry.getLineData());

        for (const auto &l_token : l_tokens)
        {
            if(l_token.size() == 1 and l_token[0] == '{')
            {
                l_stringStreamStack.top().stream << '{';
                l_stringStreamStack.push({std::ostringstream{}, l_entry.getOriginLine()});
            }
            else if(l_token.size() == 1 and l_token[0] == '}')
            {
                if (l_stringStreamStack.size() > 1)
                {
                    const std::string l_functionExtracted{l_stringStreamStack.top().stream.str()};
                    if(not l_functionExtracted.empty())
                    {
                        u32 l_originLine = l_stringStreamStack.top().lineStarted;
                        l_result.push_back({
                               l_entry.getOriginFile(),
                               l_originLine,
                               l_entry.getOriginLine() - l_originLine + 1,
                               removeWhitespacesOnLineStartAndEnd(l_functionExtracted)
                        });
                    }
                    l_stringStreamStack.pop();
                    l_stringStreamStack.top().stream << l_functionExtracted << "}";
                }
            }
            else
            {
                if(not l_token.empty())
                {
                    l_stringStreamStack.top().stream << l_token;
                }
            }
        }
    }

    return l_result;
}
