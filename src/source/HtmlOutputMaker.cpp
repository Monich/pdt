#include <sstream>
#include <CommonLogger.hpp>
#include <WhitespaceFilter.hpp>
#include "HtmlOutputMaker.hpp"

#include "AssignmentComparison.hpp"
#include "IFileAccessor.hpp"
#include "IPathProvider.hpp"
#include <cmath>
#include <algorithm>
#include <queue>

namespace
{
    struct AssignmentComparisonStub
    {
        const std::string& firstAssignmentName;
        const std::string& secondAssignmentName;
        u64 weight;
        double percent;

        AssignmentComparisonStub(const std::string &p_firstAssignmentName, const std::string &p_secondAssignmentName,
                                 u64 p_weight, double p_percent) : firstAssignmentName(p_firstAssignmentName),
                                                                   secondAssignmentName(p_secondAssignmentName),
                                                                   weight(p_weight), percent(p_percent)
        {}
    };

    struct DuplicateEntry
    {
        DuplicateLinesMarker marker;
        std::list<u8> streamIds;

        DuplicateEntry(const DuplicateLinesMarker &p_marker) : marker(p_marker)
        {}
    };

    std::string getDirectoryEntry(const std::string& p_string, size_t p_pos)
    {
        std::string l_result;

        for (unsigned l_entry = p_pos; l_entry < p_string.size(); ++l_entry)
        {
            if(p_string[l_entry] == '}')
            {
                l_result.append(&p_string[p_pos], l_entry - p_pos + 1);
                break;
            }
        }

        return l_result;
    }

    void saveWithLabeling(const std::map<std::string, std::string>& p_dictionary, std::istream& p_inputStream, std::ostream& p_outputStream)
    {
        std::string l_temp;
        while(std::getline(p_inputStream, l_temp, ' '))
        {
            size_t l_pos = l_temp.find('$');
            if(l_pos != -1)
            {
                std::string l_foundEntry = getDirectoryEntry(l_temp, l_pos);
                const std::string& l_replacementString = p_dictionary.at(l_foundEntry);
                l_temp.replace(l_pos, l_foundEntry.size(), l_replacementString);
            }
            p_outputStream << l_temp << ' ';
        }
    }

    void transformToEntries(const DiffComparison& p_comparison, std::vector<std::unique_ptr<DuplicateEntry>>& p_result, u8 streamId)
    {
        for (const auto &l_duplicate : p_comparison.duplicates)
        {
            auto l_duplicateEntry = std::find_if(p_result.begin(), p_result.end(), [&l_duplicate](const std::unique_ptr<DuplicateEntry>& p_entry)
            {
                return p_entry->marker.firstAssignmentMarker == l_duplicate.firstAssignmentMarker and p_entry->marker.secondAssignmentMarker == l_duplicate.secondAssignmentMarker;
            });

            if(l_duplicateEntry == p_result.end())
            {
                auto l_newDuplicateEntry = std::make_unique<DuplicateEntry>(l_duplicate);
                l_newDuplicateEntry->streamIds.push_back(streamId);
                p_result.push_back(std::move(l_newDuplicateEntry));
            }
            else
            {
                (*l_duplicateEntry)->marker.mark += l_duplicate.mark;
                (*l_duplicateEntry)->streamIds.push_back(streamId);
            }
        }
    }

    void transformToEntries(const AssignmentComparison& p_comparison, std::vector<std::unique_ptr<DuplicateEntry>>& p_result)
    {
        p_result.reserve(p_comparison.getTokenizedLineComparison().duplicates.size());
        transformToEntries(p_comparison.getFirstStepComparison(), p_result, 0);
        transformToEntries(p_comparison.getSecondStepComparison(), p_result, 1);
        transformToEntries(p_comparison.getCommentComparison(), p_result, 2);
        transformToEntries(p_comparison.getTokenizedComparison(), p_result, 3);
        transformToEntries(p_comparison.getTokenizedLineComparison(), p_result, 4);
        transformToEntries(p_comparison.getFunctionExtractionComparison(), p_result, 5);
        transformToEntries(p_comparison.getFunctionTokenizedComparison(), p_result, 6);
        transformToEntries(p_comparison.getTokenizedFileComparison(), p_result, 7);
    }

    std::string getStringStream(u8 id)
    {
        switch(id)
        {
            case 0:
                return "Strumień oryginalny";
            case 1:
                return "Strumień czysty";
            case 2:
                return "Strumień komentarzy";
            case 3:
                return "Strumień stokenizowany";
            case 4:
                return "Strumień linii stokenizowanej";
            case 5:
                return "Strumień ekstrakcji";
            case 6:
                return "Strumień ekstrakcji stokenizowanej";
            case 7:
                return "Strumień pliku";
            default:
                return "ERROR";
        }
    }

    std::string generateStreamsString(const std::list<u8> &p_streamIds)
    {
        if(p_streamIds.size() > 1)
        {
            std::stringstream l_stringstream;
            for (const auto &l_id : p_streamIds)
            {
                l_stringstream << getStringStream(l_id);
                l_stringstream << "</br>";
            }
            return l_stringstream.str();
        }
        else
        {
            return getStringStream(p_streamIds.front());
        }
    }

    std::string getPreLines(const Assignment &p_assignment, u32 p_firstLine)
    {
        u32 l_lineToLookFor = std::max(p_firstLine, u32{5}) - 5;
        std::ostringstream l_output;

        for (const auto &l_entry : p_assignment.getStepOneStream())
        {
            if(l_entry.getOriginLine() == l_lineToLookFor)
            {
                l_output << l_entry.getLineData() << "</br>\n";
                l_lineToLookFor++;
                if(l_lineToLookFor == p_firstLine)
                    break;
            }
        }

        return l_output.str();
    }

    std::string getInLines(const Assignment &p_assignment, u32 p_firstLine, u32 p_lastLine)
    {
        u32 l_lineToLookFor = p_firstLine;
        std::ostringstream l_output;

        for (const auto &l_entry : p_assignment.getStepOneStream())
        {
            if(l_entry.getOriginLine() == l_lineToLookFor)
            {
                l_output << l_entry.getLineData() << "</br>\n";
                l_lineToLookFor++;
                if(l_lineToLookFor > p_lastLine)
                    break;
            }
        }

        return l_output.str();
    }

    std::string getPostLines(const Assignment &p_assignment, u32 p_lastLine)
    {
        u32 l_lineToLookFor = p_lastLine;
        std::ostringstream l_output;

        for (const auto &l_entry : p_assignment.getStepOneStream())
        {
            if(l_entry.getOriginLine() == l_lineToLookFor)
            {
                l_output << l_entry.getLineData() << "</br>\n";
                l_lineToLookFor++;
                if(l_lineToLookFor == p_lastLine + 5)
                    break;
            }
        }

        return l_output.str();
    }
}

HtmlOutputMaker::HtmlOutputMaker(const IFileAccessor &p_fileAccessor, const IPathProvider &p_pathProvider)
        : m_fileAccessor(p_fileAccessor), m_pathProvider(p_pathProvider)
{}

EStatus HtmlOutputMaker::createHtmlOutput(const Assignments &p_assignments,
                                          const AssignmentComparisonMap &p_comparisonMap) const
{
    LOG_INFO << "Saving results";

    return std::max(createHtmlIndexFile(p_comparisonMap, p_assignments.size()),
                    createHtmlComparisonFiles(p_assignments, p_comparisonMap));
}

EStatus HtmlOutputMaker::createHtmlIndexFile(const AssignmentComparisonMap &p_comparisonMap, u32 p_assignmentCount) const
{
    auto l_indexOutputFile = m_fileAccessor.getOutputStream(m_pathProvider.getIndexHeader());
    auto l_indexHeaderFile = m_fileAccessor.getInputStream(m_pathProvider.getIndexSiteHeaderPath());
    auto l_indexEntryFile = m_fileAccessor.getInputStream(m_pathProvider.getIndexSiteEntryPath());
    auto l_indexFooterFile = m_fileAccessor.getInputStream(m_pathProvider.getIndexSiteFooterPath());

    if(not (l_indexOutputFile and l_indexHeaderFile and l_indexEntryFile))
    {
        return Failure;
    }

    std::map<std::string, std::string> l_dictionary;
    l_dictionary["${NO_OF_ASSIGNMENTS}"] = std::to_string(p_assignmentCount);

    LOG_DEBUG << "Saving index header data";
    saveWithLabeling(l_dictionary, *l_indexHeaderFile, *l_indexOutputFile);

    LOG_DEBUG << "Sorting received comparison map";
    std::vector<std::unique_ptr<AssignmentComparisonStub>> l_comparisonStubs;
    l_comparisonStubs.reserve(p_comparisonMap.size());
    for (const auto &l_assignmentComparison : p_comparisonMap)
    {
        l_comparisonStubs.push_back(std::make_unique<AssignmentComparisonStub>(
                                        l_assignmentComparison.first.first.getAssignmentName(),
                                        l_assignmentComparison.first.second.getAssignmentName(),
                                        l_assignmentComparison.second.getWeight(),
                                        std::max(l_assignmentComparison.second.getFirstAssignmentPercent(),
                                                l_assignmentComparison.second.getSecondAssignmentPercent())
        ));
    }
    std::sort(l_comparisonStubs.begin(), l_comparisonStubs.end(),
            [](const std::unique_ptr<AssignmentComparisonStub>& p_first, const std::unique_ptr<AssignmentComparisonStub>& p_second)
            {
                return p_first->weight > p_second->weight or (p_first->weight == p_second->weight and p_first->percent > p_second->percent);
            });

    // entry file
    LOG_DEBUG << "Saving index assignments data";
    std::string l_entryFileStream((std::istreambuf_iterator<char>(*l_indexEntryFile)),
                                  std::istreambuf_iterator<char>());
    for (const auto &l_assignmentComparisonStub : l_comparisonStubs)
    {
        std::istringstream l_tempStream{l_entryFileStream};
        l_dictionary["${ASSIGNMENT_1_NAME}"] = l_assignmentComparisonStub->firstAssignmentName;
        l_dictionary["${ASSIGNMENT_2_NAME}"] = l_assignmentComparisonStub->secondAssignmentName;
        l_dictionary["${WEIGHT}"] = std::to_string(l_assignmentComparisonStub->weight);
        l_dictionary["${PERCENT}"] = std::to_string(l_assignmentComparisonStub->percent);

        std::ostringstream l_stringStream;
        l_stringStream << l_assignmentComparisonStub->firstAssignmentName;
        l_stringStream << '/';
        l_stringStream << l_assignmentComparisonStub->secondAssignmentName;

        l_dictionary["${NAMES_COMBINED}"] = l_stringStream.str();
        saveWithLabeling(l_dictionary, l_tempStream, *l_indexOutputFile);
    }

    saveWithLabeling(l_dictionary, *l_indexFooterFile, *l_indexOutputFile);

    return Success;
}

EStatus HtmlOutputMaker::createHtmlComparisonFiles(const Assignments &p_assignments,
                                                   const AssignmentComparisonMap &p_comparisonMap) const
{
    fs::create_directories(m_pathProvider.getCurrentDirectory().append(".pdt"));

    LOG_INFO << "Creating separate output files";
    auto l_headerFile = m_fileAccessor.getInputStream(m_pathProvider.getComparisonSiteHeaderPath());
    auto l_entryFile = m_fileAccessor.getInputStream(m_pathProvider.getComparisonSiteEntryPath());
    auto l_footerFile = m_fileAccessor.getInputStream(m_pathProvider.getComparisonSiteFooterPath());
    auto l_detailsFile = m_fileAccessor.getInputStream(m_pathProvider.getDetailsSiteFooterPath());

    if(not (l_headerFile and l_entryFile and l_footerFile and l_detailsFile))
    {
        return Failure;
    }

    std::string l_headerFileContents((std::istreambuf_iterator<char>(*l_headerFile)),
                                      std::istreambuf_iterator<char>());
    std::string l_entryFileContents((std::istreambuf_iterator<char>(*l_entryFile)),
                                      std::istreambuf_iterator<char>());
    std::string l_footerFileContents((std::istreambuf_iterator<char>(*l_footerFile)),
                                      std::istreambuf_iterator<char>());
    std::string l_detailsFileContents((std::istreambuf_iterator<char>(*l_detailsFile)),
                                      std::istreambuf_iterator<char>());

    EStatus l_result{Success};
    for (const auto &l_comparison : p_comparisonMap)
    {
        LOG_DEBUG << "Creating file for " << l_comparison.first.first.getAssignmentName() << " and " << l_comparison.first.second.getAssignmentName();
        EStatus l_newResult = createHtmlComparisonFile(l_comparison.first.first, l_comparison.first.second, l_comparison.second, l_headerFileContents, l_entryFileContents, l_footerFileContents, l_detailsFileContents);
        l_result = std::max(l_newResult, l_result);
        if(l_result == Failure)
            break;
    }

    return l_result;
}

EStatus HtmlOutputMaker::createHtmlComparisonFile(const Assignment &p_firstAssignment,
                                                  const Assignment &p_secondAssignment,
                                                  const AssignmentComparison &p_comparison,
                                                  const std::string& p_headerFileContents,
                                                  const std::string& p_entryFileContents,
                                                  const std::string& p_footerFileContents,
                                                  const std::string& p_detailsFileContents) const
{
    fs::create_directories(m_pathProvider.getCurrentDirectory().append(".pdt").append(p_firstAssignment.getAssignmentName()));
    fs::create_directories(m_pathProvider.getCurrentDirectory().append(".pdt").append(p_firstAssignment.getAssignmentName()).append(p_secondAssignment.getAssignmentName()));
    auto l_outputFile = m_fileAccessor.getOutputStream(m_pathProvider.getComparisonOutputPage(p_firstAssignment, p_secondAssignment));

    if(not l_outputFile)
    {
        return Failure;
    }

    std::istringstream l_headerStream{p_headerFileContents};
    std::istringstream l_footerStream{p_footerFileContents};

    std::map<std::string, std::string> l_dictionary;
    l_dictionary["${ASSIGNMENT_1_NAME}"] = p_firstAssignment.getAssignmentName();
    l_dictionary["${ASSIGNMENT_2_NAME}"] = p_secondAssignment.getAssignmentName();
    l_dictionary["${ASSIGNMENT_1_PERCENT}"] = std::to_string(p_comparison.getFirstAssignmentPercent());
    l_dictionary["${ASSIGNMENT_2_PERCENT}"] = std::to_string(p_comparison.getSecondAssignmentPercent());
    l_dictionary["${WEIGHT}"] = std::to_string(p_comparison.getWeight());
    saveWithLabeling(l_dictionary, l_headerStream, *l_outputFile);

    // Entries
    std::vector<std::unique_ptr<DuplicateEntry>> l_entries;
    transformToEntries(p_comparison, l_entries);
    std::sort(l_entries.begin(), l_entries.end(),
              [](const std::unique_ptr<DuplicateEntry>& p_first, const std::unique_ptr<DuplicateEntry>& p_second)
              {
                  return p_first->marker.mark > p_second->marker.mark;
              });

    u32 l_id = 0;
    for (const auto &l_entry : l_entries)
    {
        if(l_entry->marker.mark > 50)
        {
            formatEntry(*l_entry, p_entryFileContents, *l_outputFile, p_firstAssignment.getAssignmentName(), p_secondAssignment.getAssignmentName(), l_id);
            createHtmlDetailsPage(*l_entry, p_firstAssignment, p_secondAssignment, p_detailsFileContents, l_id++);
        }
    }

    saveWithLabeling(l_dictionary, l_footerStream, *l_outputFile);

    return Success;
}


void HtmlOutputMaker::formatEntry(const DuplicateEntry& p_entry,
                                  const std::string& p_entryFileContents,
                                  std::ostream& p_output,
                                  const std::string &p_firstAssignmentName,
                                  const std::string &p_secondAssignmentName,
                                  u32 p_id) const
{
    std::map<std::string, std::string> l_dictionary;
    l_dictionary["${ASSIGNMENT_1_PATH}"] = p_entry.marker.firstAssignmentMarker.fileName.string();
    l_dictionary["${ASSIGNMENT_1_FIRST_LINE}"] = std::to_string(p_entry.marker.firstAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_1_LAST_LINE}"] = std::to_string(p_entry.marker.firstAssignmentMarker.lastLine);
    l_dictionary["${ASSIGNMENT_2_PATH}"] = p_entry.marker.secondAssignmentMarker.fileName.string();
    l_dictionary["${ASSIGNMENT_2_FIRST_LINE}"] = std::to_string(p_entry.marker.secondAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_2_LAST_LINE}"] = std::to_string(p_entry.marker.secondAssignmentMarker.lastLine);
    l_dictionary["${WEIGHT}"] = std::to_string(p_entry.marker.mark);
    l_dictionary["${STREAMS}"] = generateStreamsString(p_entry.streamIds);
    std::ostringstream l_entry;
    l_entry << p_secondAssignmentName << '/' << p_id;
    l_dictionary["${ENTRY_PATH}"] = l_entry.str();

    std::istringstream l_inputStream{p_entryFileContents};

    saveWithLabeling(l_dictionary, l_inputStream, p_output);
}

EStatus HtmlOutputMaker::createHtmlDetailsPage(const DuplicateEntry &p_entry,
                                            const Assignment &p_firstAssignment,
                                            const Assignment &p_secondAssignment,
                                            const std::string &p_detailsFileContents,
                                            u32 p_id) const
{
    std::ostringstream l_filename;
    l_filename << p_id;
    l_filename << ".html";
    auto l_outputFile = m_fileAccessor.getOutputStream(m_pathProvider.getCurrentDirectory()
                                                                     .append(".pdt")
                                                                     .append(p_firstAssignment.getAssignmentName())
                                                                     .append(p_secondAssignment.getAssignmentName())
                                                                     .append(l_filename.str()));

    if(not l_outputFile)
    {
        return Failure;
    }

    std::map<std::string, std::string> l_dictionary;
    l_dictionary["${ASSIGNMENT_1_NAME}"] = p_firstAssignment.getAssignmentName();
    l_dictionary["${ASSIGNMENT_2_NAME}"] = p_secondAssignment.getAssignmentName();
    l_dictionary["${ASSIGNMENT_1_PATH}"] = p_entry.marker.firstAssignmentMarker.fileName.string();
    l_dictionary["${ASSIGNMENT_1_FIRST_LINE}"] = std::to_string(p_entry.marker.firstAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_1_LAST_LINE}"] = std::to_string(p_entry.marker.firstAssignmentMarker.lastLine);
    l_dictionary["${ASSIGNMENT_2_PATH}"] = p_entry.marker.secondAssignmentMarker.fileName.string();
    l_dictionary["${ASSIGNMENT_2_FIRST_LINE}"] = std::to_string(p_entry.marker.secondAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_2_LAST_LINE}"] = std::to_string(p_entry.marker.secondAssignmentMarker.lastLine);
    l_dictionary["${WEIGHT}"] = std::to_string(p_entry.marker.mark);
    l_dictionary["${STREAMS}"] = generateStreamsString(p_entry.streamIds);

    l_dictionary["${ASSIGNMENT_1_PRE_LINES}"] = getPreLines(p_firstAssignment, p_entry.marker.firstAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_1_LINES}"] = getInLines(p_firstAssignment, p_entry.marker.firstAssignmentMarker.firstLine, p_entry.marker.firstAssignmentMarker.lastLine);
    l_dictionary["${ASSIGNMENT_1_POST_LINES}"] = getPostLines(p_firstAssignment, p_entry.marker.firstAssignmentMarker.lastLine);

    l_dictionary["${ASSIGNMENT_2_PRE_LINES}"] = getPreLines(p_secondAssignment, p_entry.marker.secondAssignmentMarker.firstLine);
    l_dictionary["${ASSIGNMENT_2_LINES}"] = getInLines(p_secondAssignment, p_entry.marker.secondAssignmentMarker.firstLine, p_entry.marker.secondAssignmentMarker.lastLine);
    l_dictionary["${ASSIGNMENT_2_POST_LINES}"] = getPostLines(p_secondAssignment, p_entry.marker.secondAssignmentMarker.lastLine);

    std::istringstream l_fileStream{p_detailsFileContents};
    saveWithLabeling(l_dictionary, l_fileStream, *l_outputFile);

    return Success;
}
