#pragma once

#include "Assignment.hpp"

#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
const Path ORIG_FILE = "file";
const Path SECOND_FILE = "file2";
const u32 ORIG_LINE = 231;
}

template<typename Filter>
class FilterTestSuite : public Test
{
public:
    void parse(const std::string& p_input, const std::string& p_output, const Path p_path = ORIG_FILE)
    {
        u32 l_lineNumber = ORIG_LINE + m_inputData.size();

        m_inputData.push_back({
              p_path,
              l_lineNumber,
              p_input
        });

        m_expectedOutputData.push_back({
               p_path,
               l_lineNumber,
               p_output
        });
    }

    void parse(const std::string& p_input, const std::vector<std::string>& p_output)
    {
        u32 l_lineNumber = ORIG_LINE + m_inputData.size();

        m_inputData.push_back({
              ORIG_FILE,
              l_lineNumber,
              p_input
        });

        for (const auto &l_entry : p_output)
        {
            m_expectedOutputData.push_back({
                   ORIG_FILE,
                   l_lineNumber,
                   l_entry
            });
        }
    }

    void addInput(const std::string& p_input, const Path& p_path = ORIG_FILE, u32 p_length = 1)
    {
        u32 l_lineNumber = ORIG_LINE + m_inputData.size();

        m_inputData.push_back({
              p_path,
              static_cast<u32>(ORIG_LINE + m_inputData.size()),
              p_length,
              p_input
        });
    }

    void addOutput(const std::string& p_output, u32 p_lineNumber, u32 p_length = 1, const Path& p_path = ORIG_FILE)
    {
        m_expectedOutputData.push_back({
               p_path,
               p_lineNumber + ORIG_LINE,
               p_length,
               p_output
        });
    }

    void execute()
    {
        Filter l_sut;
        AssignmentStream l_outputData{l_sut.filter(m_inputData)};
        ASSERT_EQ(m_expectedOutputData, l_outputData);
    }

    AssignmentStream m_inputData;
    AssignmentStream m_expectedOutputData;
};