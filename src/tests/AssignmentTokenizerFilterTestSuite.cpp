#include "AssignmentTokenizerFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class AssignmentTokenizerFilterTestSuite : public FilterTestSuite<AssignmentTokenizerFilter>{};

TEST_F(AssignmentTokenizerFilterTestSuite, shouldTokenize)
{
    parse("some text", "AAA AAB");
    parse("other text", "AAC AAB");
    parse("integer++", "AAD++");
    parse("int a = 31;", "int AAE = 31;");
    parse("std::string str;", "std::AAF AAG;");
    parse("std::cin >> str;", "std::AAH >> AAG;");
    execute();
}
