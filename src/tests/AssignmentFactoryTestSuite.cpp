#include "AssignmentFactory.hpp"

#include "mocks/FileLoaderMock.hpp"
#include "mocks/FilesLookupMock.hpp"
#include "mocks/FilterMock.hpp"
#include "TestHelpers.hpp"
#include "mocks/LineSplitterMock.hpp"

namespace
{
    const std::string ASSIGNMENT_NAME_1{"one"};
    const std::string ASSIGNMENT_NAME_2{"two"};
    const std::string ASSIGNMENT_NAME_3{"three"};

    const Path ASSIGNMENT_PATH_1{"/dir/one"};
    const Path ASSIGNMENT_PATH_2{"/dir/two"};
    const Path ASSIGNMENT_PATH_3{"/dir/three"};

    const std::set<Path> ASSIGNMENT_PATHS{ASSIGNMENT_PATH_1, ASSIGNMENT_PATH_2, ASSIGNMENT_PATH_3};

    const std::set<Path> ASSIGNMENT_FILES_1{{"/dir/one/one"}, {"/dir/one/two"}, {"/dir/one/three"}};
    const std::set<Path> ASSIGNMENT_FILES_2{{"/dir/two/one"}, {"/dir/two/two"}, {"/dir/two/three"}};
    const std::set<Path> ASSIGNMENT_FILES_3{{"/dir/three/one"}, {"/dir/three/two"}, {"/dir/three/three"}};

    const AssignmentStream ASSIGNMENT_1_DATA_STAGE1{{"st1", 1, "qwe"}, {"fule", 2, "asd"}};
    const AssignmentStream ASSIGNMENT_2_DATA_STAGE1{{"qwe", 2, "ads"}, {"zcx", 3, "oip"}};
    const AssignmentStream ASSIGNMENT_3_DATA_STAGE1{{"gsp", 9, "jhd"}, {"ksdn", 4, "erw"}};

    const AssignmentStream ASSIGNMENT_1_DATA_STAGE2{{"st2", 5, "bvx"}, {"asd", 6, "fda"}};
    const AssignmentStream ASSIGNMENT_2_DATA_STAGE2{{"vxc", 4, "gfd"}, {"gfc", 2, "cxz"}};
    const AssignmentStream ASSIGNMENT_3_DATA_STAGE2{{"jhh", 2, "mnb"}, {"das", 7, "fhg"}};

    const AssignmentStream ASSIGNMENT_1_DATA_COMMENTS{{"com", 7, "xzc"}};
    const AssignmentStream ASSIGNMENT_2_DATA_COMMENTS{{"opi", 3, "bvo"}};
    const AssignmentStream ASSIGNMENT_3_DATA_COMMENTS{{"mpe", 1, "fds"}};

    const AssignmentStream ASSIGNMENT_1_DATA_TOKENIZED_WITH_SPACES{{"tok", 846, "ckg"}};
    const AssignmentStream ASSIGNMENT_2_DATA_TOKENIZED_WITH_SPACES{{"dps", 302, "asd"}};
    const AssignmentStream ASSIGNMENT_3_DATA_TOKENIZED_WITH_SPACES{{"lkd", 432, "jfn"}};

    const AssignmentStream ASSIGNMENT_1_DATA_TOKENIZED{{"tns", 581, "ort"}};
    const AssignmentStream ASSIGNMENT_2_DATA_TOKENIZED{{"fyg", 671, "vdw"}};
    const AssignmentStream ASSIGNMENT_3_DATA_TOKENIZED{{"vca", 206, "vfi"}};

    const AssignmentStream ASSIGNMENT_1_DATA_LINE_TOKENIZED_WITH_SPACES{{"ltk", 952, "jfo"}};
    const AssignmentStream ASSIGNMENT_2_DATA_LINE_TOKENIZED_WITH_SPACES{{"vfr", 129, "xpa"}};
    const AssignmentStream ASSIGNMENT_3_DATA_LINE_TOKENIZED_WITH_SPACES{{"sao", 583, "aso"}};

    const AssignmentStream ASSIGNMENT_1_DATA_LINE_TOKENIZED{{"lts", 208, "kro"}};
    const AssignmentStream ASSIGNMENT_2_DATA_LINE_TOKENIZED{{"ash", 637, "jcu"}};
    const AssignmentStream ASSIGNMENT_3_DATA_LINE_TOKENIZED{{"kgu", 741, "jvi"}};

    const AssignmentStream ASSIGNMENT_1_DATA_WORDS_LINED{};
    const AssignmentStream ASSIGNMENT_2_DATA_WORDS_LINED{};
    const AssignmentStream ASSIGNMENT_3_DATA_WORDS_LINED{};

    const AssignmentStream ASSIGNMENT_1_DATA_WORDS_UNLINED{};
    const AssignmentStream ASSIGNMENT_2_DATA_WORDS_UNLINED{};
    const AssignmentStream ASSIGNMENT_3_DATA_WORDS_UNLINED{};

    const AssignmentStream ASSIGNMENT_1_DATA_FUNCTION{{"fun", 681, "fit"}};
    const AssignmentStream ASSIGNMENT_2_DATA_FUNCTION{{"mkg", 132, "fiw"}};
    const AssignmentStream ASSIGNMENT_3_DATA_FUNCTION{{"olk", 488, "vka"}};

    const AssignmentStream ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED_WITH_SPACES{{"fnt", 789, "vie"}};
    const AssignmentStream ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED_WITH_SPACES{{"vio", 406, "sap"}};
    const AssignmentStream ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED_WITH_SPACES{{"cpa", 002, "zxl"}};

    const AssignmentStream ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED{{"fts", 789, "tbi"}};
    const AssignmentStream ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED{{"cax", 406, "zos"}};
    const AssignmentStream ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED{{"vmt", 002, "wmy"}};

    const AssignmentStream ASSIGNMENT_1_DATA_FILE_TOKENIZED_WITH_SPACES{{"ftk", 468, "xog"}};
    const AssignmentStream ASSIGNMENT_2_DATA_FILE_TOKENIZED_WITH_SPACES{{"mrf", 213, "slb"}};
    const AssignmentStream ASSIGNMENT_3_DATA_FILE_TOKENIZED_WITH_SPACES{{"osl", 486, "mhv"}};

    const AssignmentStream ASSIGNMENT_1_DATA_FILE_TOKENIZED{{"fts", 430, "ung"}};
    const AssignmentStream ASSIGNMENT_2_DATA_FILE_TOKENIZED{{"ciw", 108, "qlm"}};
    const AssignmentStream ASSIGNMENT_3_DATA_FILE_TOKENIZED{{"lvc", 130, "rem"}};

    const Assignment ASSIGNMENT_1{ASSIGNMENT_NAME_1,
                                  ASSIGNMENT_1_DATA_STAGE1,
                                  ASSIGNMENT_1_DATA_STAGE2,
                                  ASSIGNMENT_1_DATA_COMMENTS,
                                  ASSIGNMENT_1_DATA_TOKENIZED,
                                  ASSIGNMENT_1_DATA_LINE_TOKENIZED,
                                  ASSIGNMENT_1_DATA_WORDS_LINED,
                                  ASSIGNMENT_1_DATA_WORDS_UNLINED,
                                  ASSIGNMENT_1_DATA_FUNCTION,
                                  ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED,
                                  ASSIGNMENT_1_DATA_FILE_TOKENIZED};
    const Assignment ASSIGNMENT_2{ASSIGNMENT_NAME_2,
                                  ASSIGNMENT_2_DATA_STAGE1,
                                  ASSIGNMENT_2_DATA_STAGE2,
                                  ASSIGNMENT_2_DATA_COMMENTS,
                                  ASSIGNMENT_2_DATA_TOKENIZED,
                                  ASSIGNMENT_2_DATA_LINE_TOKENIZED,
                                  ASSIGNMENT_2_DATA_WORDS_LINED,
                                  ASSIGNMENT_2_DATA_WORDS_UNLINED,
                                  ASSIGNMENT_2_DATA_FUNCTION,
                                  ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED,
                                  ASSIGNMENT_2_DATA_FILE_TOKENIZED};
    const Assignment ASSIGNMENT_3{ASSIGNMENT_NAME_3,
                                  ASSIGNMENT_3_DATA_STAGE1,
                                  ASSIGNMENT_3_DATA_STAGE2,
                                  ASSIGNMENT_3_DATA_COMMENTS,
                                  ASSIGNMENT_3_DATA_TOKENIZED,
                                  ASSIGNMENT_3_DATA_LINE_TOKENIZED,
                                  ASSIGNMENT_3_DATA_WORDS_LINED,
                                  ASSIGNMENT_3_DATA_WORDS_UNLINED,
                                  ASSIGNMENT_3_DATA_FUNCTION,
                                  ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED,
                                  ASSIGNMENT_3_DATA_FILE_TOKENIZED};

    const Assignments ASSIGNMENTS{ASSIGNMENT_1, ASSIGNMENT_3, ASSIGNMENT_2};
}

using namespace ::testing;

class AssignmentFactoryTestSuite : public Test
{
public:
    void execute(const Assignments& p_expectedAssignments = ASSIGNMENTS)
    {
        AssignmentFactory l_sut{m_fileLoader,
                                m_filesLookup,
                                m_lineSplitter,
                                m_whitespaceFilter,
                                m_assignmentTokenizerFilter,
                                m_lineTokenizerFilter,
                                m_keywordsRemoverFilter,
                                m_wordsSplitterFilter,
                                m_functionExtractorFilter,
                                m_fileTokenizerFilter};
        Assignments l_result = l_sut.loadAssignments(false);
        ASSERT_EQ(l_result, p_expectedAssignments);
    }

    void provideAssignmentPaths(const std::set<Path>& p_returnedPaths = ASSIGNMENT_PATHS)
    {
        ON_CALL(m_filesLookup, scanDirectoriesForAssignment()).WillByDefault(Return(p_returnedPaths));
    }

    void provideAssignmentFiles(const Path& p_expectedPath, const std::set<Path>& p_returnedPaths)
    {
        ON_CALL(m_filesLookup, scanFilesInPath(p_expectedPath)).WillByDefault(Return(p_returnedPaths));
    }

    void provideStepOneData(const std::set<Path>& p_paths, const AssignmentStream& p_returnedData)
    {
        ON_CALL(m_fileLoader, loadAssignmentData(p_paths)).WillByDefault(Return(p_returnedData));
    }

    void provideStepTwoData(const AssignmentStream& p_inputData, const AssignmentStream& p_codeData, const AssignmentStream& p_commentData)
    {
        ON_CALL(m_lineSplitter, splitCodeAndComments(p_inputData)).WillByDefault(Return(std::make_pair(p_codeData, p_commentData)));
    }

    static void provideFilterData(NiceMock<FilterMock>& p_filter, const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        ON_CALL(p_filter, filter(p_inputData)).WillByDefault(Return(p_outputData));
    }

    void provideAssignmentTokenizerFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_assignmentTokenizerFilter, p_inputData, p_outputData);
    }

    void provideLineTokenizerFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_lineTokenizerFilter, p_inputData, p_outputData);
    }

    void provideKeywordsRemoverFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_keywordsRemoverFilter, p_inputData, p_outputData);
    }

    void provideWordsSplitterFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_wordsSplitterFilter, p_inputData, p_outputData);
    }

    void provideFunctionExtractorFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_functionExtractorFilter, p_inputData, p_outputData);
    }

    void provideWhitespaceFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_whitespaceFilter, p_inputData, p_outputData);
    }

    void provideFileTokenizerFilterData(const AssignmentStream& p_inputData, const AssignmentStream& p_outputData)
    {
        provideFilterData(m_fileTokenizerFilter, p_inputData, p_outputData);
    }

    void provideThreeAssignmentFiles()
    {
        provideAssignmentFiles(ASSIGNMENT_PATH_1, ASSIGNMENT_FILES_1);
        provideAssignmentFiles(ASSIGNMENT_PATH_2, ASSIGNMENT_FILES_2);
        provideAssignmentFiles(ASSIGNMENT_PATH_3, ASSIGNMENT_FILES_3);
    }

    void provideThreeAssignmentStepOneData()
    {
        provideStepOneData(ASSIGNMENT_FILES_1, ASSIGNMENT_1_DATA_STAGE1);
        provideStepOneData(ASSIGNMENT_FILES_2, ASSIGNMENT_2_DATA_STAGE1);
        provideStepOneData(ASSIGNMENT_FILES_3, ASSIGNMENT_3_DATA_STAGE1);
    }

    void provideThreeAssignmentCommentData()
    {
        provideStepTwoData(ASSIGNMENT_1_DATA_STAGE1, ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_COMMENTS);
        provideStepTwoData(ASSIGNMENT_2_DATA_STAGE1, ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_COMMENTS);
        provideStepTwoData(ASSIGNMENT_3_DATA_STAGE1, ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_COMMENTS);
    }

    void provideThreeTokenizedAssignmentData()
    {
        provideAssignmentTokenizerFilterData(ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_TOKENIZED_WITH_SPACES);
        provideAssignmentTokenizerFilterData(ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_TOKENIZED_WITH_SPACES);
        provideAssignmentTokenizerFilterData(ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_TOKENIZED_WITH_SPACES);
    }

    void provideThreeTokenizedNoSpacesData()
    {
        provideWhitespaceFilterData(ASSIGNMENT_1_DATA_TOKENIZED_WITH_SPACES, ASSIGNMENT_1_DATA_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_2_DATA_TOKENIZED_WITH_SPACES, ASSIGNMENT_2_DATA_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_3_DATA_TOKENIZED_WITH_SPACES, ASSIGNMENT_3_DATA_TOKENIZED);
    }

    void provideThreeLineTokenizedData()
    {
        provideLineTokenizerFilterData(ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_LINE_TOKENIZED_WITH_SPACES);
        provideLineTokenizerFilterData(ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_LINE_TOKENIZED_WITH_SPACES);
        provideLineTokenizerFilterData(ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_LINE_TOKENIZED_WITH_SPACES);
    }

    void provideThreeLineTokenizedNoSpacesData()
    {
        provideWhitespaceFilterData(ASSIGNMENT_1_DATA_LINE_TOKENIZED_WITH_SPACES, ASSIGNMENT_1_DATA_LINE_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_2_DATA_LINE_TOKENIZED_WITH_SPACES, ASSIGNMENT_2_DATA_LINE_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_3_DATA_LINE_TOKENIZED_WITH_SPACES, ASSIGNMENT_3_DATA_LINE_TOKENIZED);
    }


    void provideThreeWordsLinedData()
    {
        provideKeywordsRemoverFilterData(ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_WORDS_LINED);
        provideKeywordsRemoverFilterData(ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_WORDS_LINED);
        provideKeywordsRemoverFilterData(ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_WORDS_LINED);
    }

    void provideThreeWordsSeparateData()
    {
        provideWordsSplitterFilterData(ASSIGNMENT_1_DATA_WORDS_LINED, ASSIGNMENT_1_DATA_WORDS_UNLINED);
        provideWordsSplitterFilterData(ASSIGNMENT_2_DATA_WORDS_LINED, ASSIGNMENT_2_DATA_WORDS_UNLINED);
        provideWordsSplitterFilterData(ASSIGNMENT_3_DATA_WORDS_LINED, ASSIGNMENT_3_DATA_WORDS_UNLINED);
    }

    void provideThreeFunctionExtractionData()
    {
        provideFunctionExtractorFilterData(ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_FUNCTION);
        provideFunctionExtractorFilterData(ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_FUNCTION);
        provideFunctionExtractorFilterData(ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_FUNCTION);
    }

    void provideThreeFunctionExtractionTokenizedData()
    {
        provideLineTokenizerFilterData(ASSIGNMENT_1_DATA_FUNCTION, ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED_WITH_SPACES);
        provideLineTokenizerFilterData(ASSIGNMENT_2_DATA_FUNCTION, ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED_WITH_SPACES);
        provideLineTokenizerFilterData(ASSIGNMENT_3_DATA_FUNCTION, ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED_WITH_SPACES);
    }

    void provideThreeFunctionTokenizedNoSpacesData()
    {
        provideWhitespaceFilterData(ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED_WITH_SPACES, ASSIGNMENT_1_DATA_FUNCTION_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED_WITH_SPACES, ASSIGNMENT_2_DATA_FUNCTION_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED_WITH_SPACES, ASSIGNMENT_3_DATA_FUNCTION_TOKENIZED);
    }

    void provideThreeFileTokenizedData()
    {
        provideFileTokenizerFilterData(ASSIGNMENT_1_DATA_STAGE2, ASSIGNMENT_1_DATA_FILE_TOKENIZED_WITH_SPACES);
        provideFileTokenizerFilterData(ASSIGNMENT_2_DATA_STAGE2, ASSIGNMENT_2_DATA_FILE_TOKENIZED_WITH_SPACES);
        provideFileTokenizerFilterData(ASSIGNMENT_3_DATA_STAGE2, ASSIGNMENT_3_DATA_FILE_TOKENIZED_WITH_SPACES);
    }

    void provideThreeFileTokenizedNoSpacesData()
    {
        provideWhitespaceFilterData(ASSIGNMENT_1_DATA_FILE_TOKENIZED_WITH_SPACES, ASSIGNMENT_1_DATA_FILE_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_2_DATA_FILE_TOKENIZED_WITH_SPACES, ASSIGNMENT_2_DATA_FILE_TOKENIZED);
        provideWhitespaceFilterData(ASSIGNMENT_3_DATA_FILE_TOKENIZED_WITH_SPACES, ASSIGNMENT_3_DATA_FILE_TOKENIZED);
    }

    void provideAllFilterData()
    {
        provideThreeAssignmentStepOneData();
        provideThreeAssignmentCommentData();
        provideThreeTokenizedAssignmentData();
        provideThreeTokenizedNoSpacesData();
        provideThreeLineTokenizedData();
        provideThreeLineTokenizedNoSpacesData();
        provideThreeWordsLinedData();
        provideThreeWordsSeparateData();
        provideThreeFunctionExtractionData();
        provideThreeFunctionExtractionTokenizedData();
        provideThreeFunctionTokenizedNoSpacesData();
        provideThreeFileTokenizedData();
        provideThreeFileTokenizedNoSpacesData();
    }

private:
    NiceMock<FileLoaderMock> m_fileLoader;
    NiceMock<FilesLookupMock> m_filesLookup;
    NiceMock<LineSplitterMock> m_lineSplitter;
    NiceMock<FilterMock> m_whitespaceFilter;
    NiceMock<FilterMock> m_assignmentTokenizerFilter;
    NiceMock<FilterMock> m_lineTokenizerFilter;
    NiceMock<FilterMock> m_keywordsRemoverFilter;
    NiceMock<FilterMock> m_wordsSplitterFilter;
    NiceMock<FilterMock> m_functionExtractorFilter;
    NiceMock<FilterMock> m_fileTokenizerFilter;
};

TEST_F(AssignmentFactoryTestSuite, basicFlow)
{
    provideAssignmentPaths();
    provideThreeAssignmentFiles();
    provideAllFilterData();
    execute();
}

TEST_F(AssignmentFactoryTestSuite, shouldReturnEmptySetIfNoDirectoriesWereFound)
{
    provideAssignmentPaths({});
    execute({});
}

TEST_F(AssignmentFactoryTestSuite, shouldIgnoreAssignmentIfItsListIsEmpty)
{
    provideAssignmentPaths();
    provideAllFilterData();

    provideAssignmentFiles(ASSIGNMENT_PATH_1, {});
    provideAssignmentFiles(ASSIGNMENT_PATH_2, ASSIGNMENT_FILES_2);
    provideAssignmentFiles(ASSIGNMENT_PATH_3, {});
    
    execute({ASSIGNMENT_2});
}
