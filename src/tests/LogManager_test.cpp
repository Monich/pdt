#include "LogManager.hpp"

#include "LogWrapper.hpp"

#include "mocks/Logger_mock.hpp"

#include <gtest/gtest.h>

namespace
{
    constexpr bool CONFIG_DEBUG_DISABLED = false;
    constexpr bool CONFIG_DEBUG_ENABLED = true;
    const StructureThrowOnHumanizer MESSAGE_TO_HUMANIZE;
}

TEST(LogManager, ShouldNotStartLoggingWhenNotInitialized)
{
    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());

    ASSERT_NO_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                        ELogSeverity_Info) << MESSAGE_TO_HUMANIZE);
    ASSERT_NO_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                        ELogSeverity_Warning) << MESSAGE_TO_HUMANIZE);
    ASSERT_NO_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                        ELogSeverity_Error) << MESSAGE_TO_HUMANIZE);
    ASSERT_NO_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                        ELogSeverity_Debug) << MESSAGE_TO_HUMANIZE);

    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
}

TEST(LogManager, ShouldStartLoggingWhenInitializedAndStopDebugLoggingWhenDisabled)
{
    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
    ASSERT_TRUE(
        LogManager::getSingletonInstance()->initialize(CONFIG_DEBUG_DISABLED));

    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Info) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Warning) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Error) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_NO_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                        ELogSeverity_Debug) << MESSAGE_TO_HUMANIZE);

    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
}

TEST(LogManager, ShouldStartLoggingWhenInitializedForDebugLogsAsWell)
{
    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
    ASSERT_TRUE(
        LogManager::getSingletonInstance()->initialize(CONFIG_DEBUG_ENABLED));

    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Info) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Warning) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Error) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);
    ASSERT_THROW(LogManager::getSingletonInstance()->getLogWithSeverity(
                     ELogSeverity_Debug) << MESSAGE_TO_HUMANIZE, HumanizerInvokedException);

    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
}
