#include "FileTokenizerFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class FileTokenizerFilterTestSuite : public FilterTestSuite<FileTokenizerFilter>{};

TEST_F(FileTokenizerFilterTestSuite, shouldTokenize)
{
    parse("some text", "AAA AAB");
    parse("other text", "AAC AAB");
    parse("integer++", "AAD++");
    parse("int a = 31;", "int AAA = 31;", SECOND_FILE);
    parse("std::string str;", "std::AAB AAC;", SECOND_FILE);
    parse("std::cin >> str;", "std::AAD >> AAC;", SECOND_FILE);
    execute();
}
