#include "KeywordsRemoverFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class KeywordsRemoverFilterTestSuite : public FilterTestSuite<KeywordsRemoverFilter>{};

TEST_F(KeywordsRemoverFilterTestSuite, shouldExtractWords)
{
    parse("using namespace ::testing;", "testing");
    parse("class WordExtractorFilterTestSuite : public FilterTestSuite<KeywordsRemoverFilter>{};",
            "WordExtractorFilterTestSuite FilterTestSuite KeywordsRemoverFilter");
    execute();
}
