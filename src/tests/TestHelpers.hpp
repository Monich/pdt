#pragma once

#include "Assignment.hpp"
#include "AssignmentComparison.hpp"

inline bool operator==(const DuplicateLinesMarker &p_o1, const DuplicateLinesMarker &p_o2)
{
    return p_o1.firstAssignmentMarker == p_o2.firstAssignmentMarker
           and p_o1.secondAssignmentMarker == p_o2.secondAssignmentMarker;
}

inline bool operator==(const DiffComparison &p_o1, const DiffComparison &p_o2)
{
    return p_o1.lineHits == p_o2.lineHits
           and p_o1.linesAnalyzed == p_o2.linesAnalyzed
           and p_o1.duplicates == p_o2.duplicates;
}

inline bool operator==(const AssignmentComparison& p_o1, const AssignmentComparison& p_o2)
{
    return p_o1.getFirstStepComparison() == p_o2.getFirstStepComparison()
        and p_o1.getSecondStepComparison() == p_o2.getSecondStepComparison()
        and p_o1.getCommentComparison() == p_o2.getCommentComparison()
        and p_o1.getTokenizedComparison() == p_o2.getTokenizedComparison()
        and p_o1.getTokenizedLineComparison() == p_o2.getTokenizedLineComparison()
        and p_o1.getNoKeywordsComparison() == p_o2.getNoKeywordsComparison()
        and p_o1.getIdentifiersComparison() == p_o2.getIdentifiersComparison()
        and p_o1.getFunctionExtractionComparison() == p_o2.getFunctionExtractionComparison()
        and p_o1.getFunctionTokenizedComparison() == p_o2.getFunctionTokenizedComparison()
        and p_o1.getTokenizedFileComparison() == p_o2.getTokenizedFileComparison()
        ;
}
