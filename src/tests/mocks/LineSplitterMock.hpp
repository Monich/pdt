#pragma once

#include "ILineSplitter.hpp"

#include <gmock/gmock.h>

class LineSplitterMock : public ILineSplitter
{
public:
    MOCK_CONST_METHOD1(splitCodeAndComments, std::pair<AssignmentStream, AssignmentStream>(const AssignmentStream&));
};

