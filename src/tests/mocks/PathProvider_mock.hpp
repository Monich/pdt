#pragma once

#include "../TestHelpers.hpp"

#include "IPathProvider.hpp"

#include <gmock/gmock.h>

class PathProviderMock : public IPathProvider
{
public:
    MOCK_CONST_METHOD0(getCurrentDirectory, Path());
    MOCK_CONST_METHOD0(getIndexHeader, Path());
    MOCK_CONST_METHOD0(getIndexSiteHeaderPath, Path());
    MOCK_CONST_METHOD0(getIndexSiteEntryPath, Path());
    MOCK_CONST_METHOD0(getIndexSiteFooterPath, Path());
    MOCK_CONST_METHOD0(getComparisonSiteHeaderPath, Path());
    MOCK_CONST_METHOD0(getComparisonSiteEntryPath, Path());
    MOCK_CONST_METHOD0(getComparisonSiteFooterPath, Path());
    MOCK_CONST_METHOD0(getDetailsSiteFooterPath, Path());

    MOCK_CONST_METHOD2(getComparisonOutputPage, Path(const Assignment&, const Assignment&));
};
