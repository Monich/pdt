#pragma once

#include "IHtmlOutputMaker.hpp"

#include <gmock/gmock.h>

class HtmlOutputMakerMock : public IHtmlOutputMaker
{
public:
    MOCK_CONST_METHOD2(createHtmlOutput, EStatus(const Assignments& p_assignments,
            const AssignmentComparisonMap& p_comparisonMap));
};