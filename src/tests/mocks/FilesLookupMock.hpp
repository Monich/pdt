#pragma once

#include "IFilesLookup.hpp"

#include <gmock/gmock.h>

class FilesLookupMock : public IFilesLookup
{
public:
    MOCK_CONST_METHOD1(scanFilesInPath, std::set<Path>(const Path &));
    MOCK_CONST_METHOD0(scanDirectoriesForAssignment, std::set<Path>());
};
