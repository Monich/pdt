#pragma once

#include "Logger.hpp"

#include <gtest/gtest.h>

class StructureToHumanize
{
public:
    //! Mock variable
    int a;
    //! Mock variable
    int b;

    //! Simple constructor
    StructureToHumanize(int a, int b) : a(a), b(b)
    {}
};

inline void operator<<(Log& p_logger, const StructureToHumanize& p_str)
{
    p_logger << p_str.a;
    p_logger << " ";
    p_logger << p_str.b;
}

class StructureThrowOnHumanizer
{
};

class HumanizerInvokedException : public std::exception
{
};

inline void operator<<(Logger&, const StructureThrowOnHumanizer&)
{
    throw HumanizerInvokedException();
}

using namespace ::testing;

class LoggerTestSuite : public Test
{
protected:
    void SetUp() override
    {
        m_stringStream = std::make_shared<std::ostringstream>();
    }

    template<typename T>
    void log(T p_item)
    {
        Logger l_tempSut(m_stringStream);
        l_tempSut << p_item;
    }

    void checkOutput(const char* p_correctOutput)
    {
        ASSERT_STREQ(m_stringStream->str().c_str(), p_correctOutput);
    }

private:
    std::shared_ptr<std::ostringstream> m_stringStream;
};
