#pragma once

#include "AssignmentComparison.hpp"
#include "IDiffComparisonMaker.hpp"

#include <gmock/gmock.h>

class DiffComparisonMakerMock : public IDiffComparisonMaker
{
public:
    MOCK_CONST_METHOD3(compareData, DiffComparison(const AssignmentStream&, const AssignmentStream&, const IMarkGenerator &p_markGenerator));
};
