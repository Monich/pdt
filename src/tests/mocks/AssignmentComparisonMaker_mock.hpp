#pragma once

#include "IAssignmentComparisonMaker.hpp"

#include <gmock/gmock.h>

class AssignmentComparisonMakerMock : public IAssignmentComparisonMaker
{
public:
    MOCK_CONST_METHOD2(compareAssignments, AssignmentComparison(const Assignment&, const Assignment&));
};
