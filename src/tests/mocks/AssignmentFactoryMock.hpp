#pragma once

#include "IAssignmentFactory.hpp"

#include <gmock/gmock.h>

class AssignmentFactoryMock : public IAssignmentFactory
{
public:
    MOCK_CONST_METHOD1(loadAssignments, Assignments(bool p_simplify));
};
