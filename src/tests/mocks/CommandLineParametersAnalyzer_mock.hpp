#pragma once

#include "ICommandLineParametersAnalyzer.hpp"

#include <gmock/gmock.h>

class CommandLineParametersAnalyzerMock : public ICommandLineParametersAnalyser
{
public:
    CommandLineConfigurationUPtr analyzeCommandLineParameters(const CommandLineParameters& p_parameters) const override
    {
        return std::make_unique<CommandLineConfiguration>(analyzeCommandLineParametersProxy(p_parameters));
    }

    MOCK_CONST_METHOD1(analyzeCommandLineParametersProxy, CommandLineConfiguration(const CommandLineParameters&));
};
