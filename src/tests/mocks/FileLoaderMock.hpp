#pragma once

#include "IFileLoader.hpp"

#include <gmock/gmock.h>

class FileLoaderMock : public IFileLoader
{
public:
    MOCK_CONST_METHOD1(loadAssignmentData, AssignmentStream(const std::set<Path>&));
};
