#pragma once

#include "IFileAccessor.hpp"

#include <gmock/gmock.h>

#include <optional>

class OutputStreamMock : public std::ostringstream
{
public:
    explicit OutputStreamMock(std::string p_expectedResult) :
        m_expectedResult(std::move(p_expectedResult))
    {}

    ~OutputStreamMock() override
    {
        EXPECT_EQ(str(), m_expectedResult);
    }

private:
    std::string m_expectedResult;
};

class FileAccessorMock : public IFileAccessor
{
public:
    std::unique_ptr<std::istream> getInputStream(const Path &p_path) const override
    {
        auto l_optionalResult = getInputStreamProxy(p_path);
        if(l_optionalResult)
        {
            return std::make_unique<std::istringstream>(std::move(*l_optionalResult));
        }
        return nullptr;
    }

    std::unique_ptr<std::ostream> getOutputStream(const Path &p_path) const override
    {
        auto l_optionalResult = getOutputStreamProxy(p_path);
        if(l_optionalResult)
        {
            return std::make_unique<OutputStreamMock>(std::move(*l_optionalResult));
        }
        return nullptr;
    }

    MOCK_CONST_METHOD1(getInputStreamProxy, std::optional<std::string>(const Path&));
    MOCK_CONST_METHOD1(getOutputStreamProxy, std::optional<std::string>(const Path&));
};
