#pragma once

#include "IFilter.hpp"

#include <gmock/gmock.h>

class FilterMock : public IFilter
{
public:
    MOCK_CONST_METHOD1(filter, AssignmentStream(const AssignmentStream&));
};
