#include "Assignment.hpp"
#include "Humanizers.hpp"

#include "mocks/Logger_mock.hpp"

#ifndef _MSC_VER
// Not usable in MSVS
TEST_F(LoggerTestSuite, ShouldUseBooleanHumanizer)
{
    constexpr const char *l_testTextOutput = "true\nfalse\n";

    log(true);
    log(false);
    checkOutput(l_testTextOutput);
}
#endif

TEST_F(LoggerTestSuite, ShouldUseAssignmentFilesHumanizer)
{
    AssignmentFiles l_emptyFiles;
    constexpr const char *l_testTextOutput = "empty assignment files set\n{ a, b, c }\n";
    AssignmentFiles l_files{"a", "b", "c"};

    log(l_emptyFiles);
    log(l_files);
    checkOutput(l_testTextOutput);
}

TEST_F(LoggerTestSuite, ShouldUseStatusHumanizer)
{
    constexpr const char *l_testTextOutput = "Success\nFailure\nPartial Success\n";

    log(Success);
    log(Failure);
    log(PartialSuccess);
    checkOutput(l_testTextOutput);
}
