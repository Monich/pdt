#include "CommonLogger.hpp"

#include <gtest/gtest.h>

TEST(CommonLogger, ShouldLogGivenMessages)
{
    constexpr const char * l_message = "message";
    constexpr const bool l_enableDebugLogs = true;

    LogManager::getSingletonInstance()->terminate();

    ASSERT_TRUE(
        LogManager::getSingletonInstance()->initialize(l_enableDebugLogs));

    LOG_INFO << l_message;
    LOG_WARN << l_message;
    LOG_ERROR << l_message;
    LOG_DEBUG << l_message;

    LogManager::getSingletonInstance()->terminate();
    ASSERT_FALSE(LogManager::getSingletonInstance()->isInitialized());
}
