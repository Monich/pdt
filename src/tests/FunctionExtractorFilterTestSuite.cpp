#include "FunctionExtractorFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class FunctionExtractorFilterTestSuite : public FilterTestSuite<FunctionExtractorFilter>{};

TEST_F(FunctionExtractorFilterTestSuite, shouldExtractFunctions)
{
    addInput("AssignmentStream AssignmentTokenizerFilter::filter(const AssignmentStream &p_data) const");
    addInput("{");
    addInput("AssignmentStream l_result{};");
    addInput("std::map<std::string, std::string> l_dictionary;");
    addInput("for (const auto &l_entry : p_data)");
    addInput("{");
    addInput("l_result.push_back({");
    addInput("l_entry.getOriginFile(),");
    addInput("l_entry.getOriginLine(),");
    addInput("tokenizeWithDictionary(l_entry.getLineData(), l_dictionary)");
    addInput("});");
    addInput("}");
    addInput("return l_result;");
    addInput("}");

    addOutput("l_entry.getOriginFile(), "
              "l_entry.getOriginLine(), "
              "tokenizeWithDictionary(l_entry.getLineData(), l_dictionary)", 6, 5);
    addOutput("l_result.push_back({ "
        "l_entry.getOriginFile(), "
        "l_entry.getOriginLine(), "
        "tokenizeWithDictionary(l_entry.getLineData(), l_dictionary) "
        "});", 5, 7);
    addOutput("AssignmentStream l_result{}; "
        "std::map<std::string, std::string> l_dictionary; "
        "for (const auto &l_entry : p_data) "
        "{ "
        "l_result.push_back({ "
        "l_entry.getOriginFile(), "
        "l_entry.getOriginLine(), "
        "tokenizeWithDictionary(l_entry.getLineData(), l_dictionary) "
        "}); "
        "} "
        "return l_result;", 1, 13);

    execute();
}

TEST_F(FunctionExtractorFilterTestSuite, shouldNotAddWhenInDifferentFiles)
{
    addInput("{ something");
    addInput("some other thing }");
    addInput("and maybe { some thing");
    addInput("but in different file }", SECOND_FILE);
    addInput("yet {some other } thing", SECOND_FILE);

    addOutput("something some other thing", 0, 2);
    addOutput("some other", 4, 1, SECOND_FILE);
    execute();
}
