#include "Executor.hpp"

#include "TestHelpers.hpp"

#include "mocks/AssignmentComparisonMaker_mock.hpp"
#include "mocks/FilesLookupMock.hpp"
#include "mocks/CommandLineParametersAnalyzer_mock.hpp"
#include "mocks/AssignmentFactoryMock.hpp"
#include "mocks/HtmlOutputMakerMock.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
const std::string ASSIGNMENT1_NAME{"1"};
const std::string ASSIGNMENT2_NAME{"2"};
const std::string ASSIGNMENT3_NAME{"3"};

const Assignment ASSIGNMENT1 = {ASSIGNMENT1_NAME, {}};
const Assignment ASSIGNMENT2 = {ASSIGNMENT2_NAME, {}};
const Assignment ASSIGNMENT3 = {ASSIGNMENT3_NAME, {}};

const Assignments FILLED_ASSIGNMENTS{ASSIGNMENT1, ASSIGNMENT2, ASSIGNMENT3};

const DiffComparison EMPTY_DIFF_COMPARISON{};
const DiffComparison DIFF_COMPARISON_1_2{1, 2, {}};
const DiffComparison DIFF_COMPARISON_1_3{1, 2, {}};
const DiffComparison DIFF_COMPARISON_2_3{2, 3, {}};

const AssignmentComparison COMPARISON_1_2{DIFF_COMPARISON_1_2};
const AssignmentComparison COMPARISON_1_3{DIFF_COMPARISON_1_3};
const AssignmentComparison COMPARISON_2_3{DIFF_COMPARISON_2_3};
const AssignmentComparison EMPTY_COMPARISON{EMPTY_DIFF_COMPARISON};

const AssignmentComparisonMap COMPARISON_MAP{
        {{ASSIGNMENT1, ASSIGNMENT2}, COMPARISON_1_2},
        {{ASSIGNMENT1, ASSIGNMENT3}, COMPARISON_1_3},
        {{ASSIGNMENT2, ASSIGNMENT3}, COMPARISON_2_3},
};

}

class ExecutorTestSuite : public Test
{
public:
    ExecutorTestSuite() = default;

    void execute()
    {
        Executor l_sut(m_assignmentComparisonMaker,
                       m_assignmentFactory,
                       m_commandLineParametersAnalyzer,
                       m_htmlOutputFormatter);

        CommandLineParameters l_commandLineParameters{};

        l_sut.execute(l_commandLineParameters);
    }

    void expectAssignmentFactoryCall(const Assignments& p_assignmentsReturned)
    {
        EXPECT_CALL(m_assignmentFactory, loadAssignments(false)).WillOnce(Return(p_assignmentsReturned));
    }

    void expectAssignmentComparisonCall(const Assignment& p_firstAssignmentCalled,
                                        const Assignment& p_secondAssignmentCalled,
                                        const AssignmentComparison& p_result = EMPTY_COMPARISON)
    {
        EXPECT_CALL(m_assignmentComparisonMaker, compareAssignments(p_firstAssignmentCalled, p_secondAssignmentCalled)).WillOnce(Return(p_result));
    }

    void returnCommandLineConfiguration(const CommandLineConfiguration& p_configuration)
    {
        ON_CALL(m_commandLineParametersAnalyzer, analyzeCommandLineParametersProxy(CommandLineParameters{})).WillByDefault(Return(p_configuration));
    }

    void throwExceptionOnAnalyzeCommandLineConfigurationParameters()
    {
        ON_CALL(m_commandLineParametersAnalyzer, analyzeCommandLineParametersProxy(CommandLineParameters{}))
            .WillByDefault(Throw(CommandLineParametersException(__FUNCTION__)));
    }

    void expectHtmlOutputFormatterCall(const Assignments& p_assignments = FILLED_ASSIGNMENTS,
            const AssignmentComparisonMap& p_assignmentMap = COMPARISON_MAP)
    {
        EXPECT_CALL(m_htmlOutputFormatter, createHtmlOutput(p_assignments, p_assignmentMap)).WillOnce(Return(Success));
    }

    void expectThreeAssignmentComparisonCalls()
    {
        expectAssignmentComparisonCall(ASSIGNMENT1, ASSIGNMENT2, COMPARISON_1_2);
        expectAssignmentComparisonCall(ASSIGNMENT1, ASSIGNMENT3, COMPARISON_1_3);
        expectAssignmentComparisonCall(ASSIGNMENT2, ASSIGNMENT3, COMPARISON_2_3);
    }

private:
    StrictMock<AssignmentComparisonMakerMock> m_assignmentComparisonMaker;
    StrictMock<AssignmentFactoryMock> m_assignmentFactory;
    NiceMock<CommandLineParametersAnalyzerMock> m_commandLineParametersAnalyzer;
    StrictMock<HtmlOutputMakerMock> m_htmlOutputFormatter;
};

TEST_F(ExecutorTestSuite, basicFlow)
{
    returnCommandLineConfiguration(CommandLineConfiguration{false});
    expectAssignmentFactoryCall(FILLED_ASSIGNMENTS);
    expectThreeAssignmentComparisonCalls();
    expectHtmlOutputFormatterCall();
    execute();
}

TEST_F(ExecutorTestSuite, shouldEnableDebugLogsIfSpecifiedInConfiguration)
{
    returnCommandLineConfiguration(CommandLineConfiguration{true});
    expectAssignmentFactoryCall(Assignments{});
    execute();
}

TEST_F(ExecutorTestSuite, shouldAbortIfReadingConfigurationWasInterrupted)
{
    returnCommandLineConfiguration(CommandLineConfiguration{false});
    throwExceptionOnAnalyzeCommandLineConfigurationParameters();
    EXPECT_THROW(execute(), CommandLineParametersException);
}
