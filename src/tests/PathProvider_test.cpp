#include "PathProvider.hpp"

#include "TestHelpers.hpp"

#include <gtest/gtest.h>

TEST(PathProviderTestSuite, shouldReturnCurrentDir)
{
    PathProvider l_sut{"/some/temp/dir"};
    ASSERT_EQ(l_sut.getCurrentDirectory(), fs::current_path());
}

TEST(PathProviderTestSuite, shouldReturnIndexHeaderPath)
{
    PathProvider l_sut{"/some/temp/dir"};
    const Path l_expectedPath{"/some/temp/html/index_pieces/header.html"};
    ASSERT_EQ(l_sut.getIndexSiteHeaderPath(), l_expectedPath);
}

TEST(PathProviderTestSuite, shouldReturnIndexEntryPath)
{
    PathProvider l_sut{"/some/temp/dir"};
    const Path l_expectedPath{"/some/temp/html/index_pieces/entry.html"};
    ASSERT_EQ(l_sut.getIndexSiteEntryPath(), l_expectedPath);
}
