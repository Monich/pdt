#include "mocks/Logger_mock.hpp"

TEST_F(LoggerTestSuite, ShouldPassMessageToStream)
{
    constexpr const char *l_testTextInput = "TEXT ";
    constexpr int l_numberInput = 22;
    constexpr const char *l_testTextOutput = "TEXT \n22\n";

    log(l_testTextInput);
    log(l_numberInput);
    checkOutput(l_testTextOutput);
}

TEST_F(LoggerTestSuite, ShouldUseHumanizerIfAvailable)
{
    const StructureToHumanize l_input(0, 1);
    constexpr const char *l_testTextOutput = "0 1\n";

    log(l_input);
    checkOutput(l_testTextOutput);
}
