#include "WhitespaceFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class WhitespaceFilterTestSuite : public FilterTestSuite<WhitespaceFilter>{};

TEST_F(WhitespaceFilterTestSuite, shouldRemoveSpaces)
{
    parse("some text", "sometext");
    parse(" other texts ", "othertexts");
    parse("i n e e d m o r e s p a c e s", "ineedmorespaces");
    execute();
}

TEST_F(WhitespaceFilterTestSuite, shouldRemoveTabs)
{
    parse("some\ttext", "sometext");
    parse("\tother\ttexts\t", "othertexts");
    parse("i\tn\te\te\td\tm\to\tr\te\tt\ta\tb\ts", "ineedmoretabs");
    execute();
}
