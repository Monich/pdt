#include "LineTokenizerFilter.hpp"
#include "FilterTestSuite.hpp"

using namespace ::testing;

class LineTokenizerFilterTestSuite : public FilterTestSuite<LineTokenizerFilter>{};

TEST_F(LineTokenizerFilterTestSuite, shouldTokenize)
{
    parse("some text", "AAA AAB");
    parse("other text", "AAA AAB");
    parse("integer++", "AAA++");
    parse("int a = 31;", "int AAA = 31;");
    parse("std::string str;", "std::AAA AAB;");
    parse("std::cin >> str;", "std::AAA >> AAB;");
    execute();
}

TEST_F(LineTokenizerFilterTestSuite, shouldKeepLengthWhenDealingWithMultilineFilter)
{
    addInput("something");
    addInput("here is some other multiline thing", ORIG_FILE, 12);

    addOutput("AAA", 0);
    addOutput("AAA AAB AAC AAD AAE AAF", 1, 12);

    execute();
}
