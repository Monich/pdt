#include "FileLoader.hpp"

#include "mocks/FileAccessor_mock.hpp"
#include "mocks/PathProvider_mock.hpp"

using namespace ::testing;

namespace
{
    const Path OUTPUT_FILE{"output"};
}

class FileLoaderTestSuite : public Test
{
public:
    Path addFile(const std::string &p_contents)
    {
        const Path& l_filePath = generateNewFilename();

        ON_CALL(m_fileProvider, getInputStreamProxy(l_filePath)).WillByDefault(Return(p_contents));
        m_filesToLoad.insert(l_filePath);

        m_expectedAssignmentData.push_back({l_filePath, 1, p_contents});

        return l_filePath;
    }

    void overrideExpectedAssignmentData(const AssignmentStream &p_expectedData)
    {
        m_expectedAssignmentData = p_expectedData;
    }

    void addEmptyFile()
    {
        m_filesToLoad.insert(generateNewFilename());
    }

    void execute()
    {
        FileLoader l_sut(m_fileProvider);
        ASSERT_EQ(l_sut.loadAssignmentData(m_filesToLoad), m_expectedAssignmentData);
    }

private:
    std::set<Path> m_filesToLoad;
    AssignmentStream m_expectedAssignmentData{};
    NiceMock<FileAccessorMock> m_fileProvider;

    Path generateNewFilename()
    {
        return std::to_string(m_filesToLoad.size());
    }
};

TEST_F(FileLoaderTestSuite, shouldPassEmptyAssignment)
{
    execute();
}

TEST_F(FileLoaderTestSuite, shouldPassEmptyFile)
{
    addEmptyFile();
    execute();
}

TEST_F(FileLoaderTestSuite, shouldCopyMessagesFromProvidedFiles)
{
    constexpr const char* l_message1 = "abc";
    constexpr const char* l_message2 = "def";

    addFile(l_message1);
    addFile(l_message2);

    execute();
}

TEST_F(FileLoaderTestSuite, shouldLoadMultiLineFile)
{
    constexpr const char* l_message = "qwe\nasd\nzxc\n";

    Path l_filePath = addFile(l_message);

    AssignmentStream l_expectedAssignmentData{
            {l_filePath, 1, "qwe"},
            {l_filePath, 2, "asd"},
            {l_filePath, 3, "zxc"},
    };

    overrideExpectedAssignmentData(l_expectedAssignmentData);

    execute();
}
