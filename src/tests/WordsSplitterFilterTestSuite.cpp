#include "WordsSplitterFilter.hpp"
#include "FilterTestSuite.hpp"

class WordsSplitterFilterTestSuite : public FilterTestSuite<WordsSplitterFilter>{};

TEST_F(WordsSplitterFilterTestSuite, shouldSplitWords)
{
    parse("some text is here", {"some", "text", "is", "here"});
    execute();
}
