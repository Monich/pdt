#include "AssignmentComparisonMaker.hpp"

#include "Assignment.hpp"

#include "mocks/DiffComparisonMaker_mock.hpp"
#include "TestHelpers.hpp"

#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
    const Assignment ASSIGNMENT_ONE{
        "one",
        {{"qwe", 2, "asd"}},
        {{"zxc", 0, "fgh"}},
        {{"okb", 4, "uye"}},
        {{"vuw", 8, "sap"}},
        {{"viw", 6, "fca"}},
        {{"voa", 7, "vka"}},
        {{"zps", 1, "sad"}},
        {{"zpa", 3, "gos"}},
        {{"vle", 4, "lbl"}},
        {{"owr", 8, "fvx"}}};
    const Assignment ASSIGNMENT_TWO{
        "two",
        {{"asd", 0, "qwe"}},
        {{"ghj", 0, "zxc"}},
        {{"dlr", 0, "oiu"}},
        {{"slt", 8, "sap"}},
        {{"cio", 6, "fca"}},
        {{"czx", 7, "vka"}},
        {{"cps", 1, "sap"}},
        {{"apw", 6, "xcv"}},
        {{"vcl", 4, "fsd"}},
        {{"wo", 8, "fvx"}}};

    const DiffComparison DIFF_COMP_S1{ 123, 1234 };
    const DiffComparison DIFF_COMP_S2{ 321, 3210 };
    const DiffComparison DIFF_COMP_COMMENTS{890, 345 };
    const DiffComparison DIFF_COMP_TOKENIZED{842, 130 };
    const DiffComparison DIFF_COMP_TOKENIZED_LINE{488, 12 };
    const DiffComparison DIFF_COMP_NO_KEYWORDS{11, 876 };
    const DiffComparison DIFF_COMP_IDENTIFIERS{585, 111 };
    const DiffComparison DIFF_COMP_FUNCTION{545, 138 };
    const DiffComparison DIFF_COMP_FUNCTION_TOKENIZED{123, 789 };
    const DiffComparison DIFF_COMP_TOKENIZED_FILE{200, 1000 };

    const AssignmentComparison SUCCESS_ASSIGNMENT_DIFF_COMP{DIFF_COMP_S1, DIFF_COMP_S2,
                                                            DIFF_COMP_COMMENTS,
                                                            DIFF_COMP_TOKENIZED,
                                                            DIFF_COMP_TOKENIZED_LINE,
                                                            DIFF_COMP_NO_KEYWORDS,
                                                            DIFF_COMP_IDENTIFIERS, DIFF_COMP_FUNCTION,
                                                            DIFF_COMP_FUNCTION_TOKENIZED,
                                                            DIFF_COMP_TOKENIZED_FILE, 10, 10 };

} // namespace

class AssignmentComparisonMakerTestSuite : public Test
{
public:
    void execute(const AssignmentComparison &p_expectedComparison)
    {
        AssignmentComparisonMaker l_sut{m_diffComparisonMaker};
        ASSERT_EQ(l_sut.compareAssignments(ASSIGNMENT_ONE, ASSIGNMENT_TWO), p_expectedComparison);
    }

    void addComparison(const AssignmentStream& p_firstAssignmentData, const AssignmentStream& p_secondAssignmentData, const DiffComparison &p_result)
    {
        ON_CALL(m_diffComparisonMaker, compareData(p_firstAssignmentData, p_secondAssignmentData, _)).WillByDefault(Return(p_result));
    }

    void addAllComparisonData()
    {
        addComparison(ASSIGNMENT_ONE.getStepOneStream(), ASSIGNMENT_TWO.getStepOneStream(), DIFF_COMP_S1);
        addComparison(ASSIGNMENT_ONE.getStepTwoStream(), ASSIGNMENT_TWO.getStepTwoStream(), DIFF_COMP_S2);
        addComparison(ASSIGNMENT_ONE.getCommentStream(), ASSIGNMENT_TWO.getCommentStream(), DIFF_COMP_COMMENTS);
        addComparison(ASSIGNMENT_ONE.getTokenizedStream(), ASSIGNMENT_TWO.getTokenizedStream(), DIFF_COMP_TOKENIZED);
        addComparison(ASSIGNMENT_ONE.getTokenizedLineStream(), ASSIGNMENT_TWO.getTokenizedLineStream(), DIFF_COMP_TOKENIZED_LINE);
        addComparison(ASSIGNMENT_ONE.getNoKeywordsStream(), ASSIGNMENT_TWO.getNoKeywordsStream(), DIFF_COMP_NO_KEYWORDS);
        addComparison(ASSIGNMENT_ONE.getIdentifiersStream(), ASSIGNMENT_TWO.getIdentifiersStream(), DIFF_COMP_IDENTIFIERS);
        addComparison(ASSIGNMENT_ONE.getFunctionExtractionStream(), ASSIGNMENT_TWO.getFunctionExtractionStream(), DIFF_COMP_FUNCTION);
        addComparison(ASSIGNMENT_ONE.getFunctionTokenizedStream(), ASSIGNMENT_TWO.getFunctionTokenizedStream(), DIFF_COMP_FUNCTION_TOKENIZED);
        addComparison(ASSIGNMENT_ONE.getTokenizedFileStream(), ASSIGNMENT_TWO.getTokenizedFileStream(), DIFF_COMP_TOKENIZED_FILE);
    }

private:
    NiceMock<DiffComparisonMakerMock> m_diffComparisonMaker;
};

TEST_F(AssignmentComparisonMakerTestSuite, shouldCompareTwoAssignments)
{
    addAllComparisonData();
    execute(SUCCESS_ASSIGNMENT_DIFF_COMP);
}
