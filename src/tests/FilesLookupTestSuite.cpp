#include "FilesLookup.hpp"

#include "cmake_defines.hpp"

#include "TestHelpers.hpp"
#include "mocks/PathProvider_mock.hpp"

#include <gtest/gtest.h>

using namespace ::testing;

namespace
{
std::set<Path> generateTestFilesStructure()
    {
        return {
            Path(PDT_TEST_FILES_PATH).append("first/code.cpp"),
            Path(PDT_TEST_FILES_PATH).append("mine/code.cpp"),
            Path(PDT_TEST_FILES_PATH).append("mine/utils.cpp"),
            Path(PDT_TEST_FILES_PATH).append("mine/utils.hpp"),
            Path(PDT_TEST_FILES_PATH).append("other/source/lib.cpp"),
            Path(PDT_TEST_FILES_PATH).append("other/source/main.cpp"),
            Path(PDT_TEST_FILES_PATH).append("other/include/header.hpp"),
            Path(PDT_TEST_FILES_PATH).append("other/include/utils.hpp"),
            Path(PDT_TEST_FILES_PATH).append("task/source.c"),
            Path(PDT_TEST_FILES_PATH).append("task/lib.h"),
        };
    }
}

TEST(FilesLookupTestSuite, testfiles)
{
    NiceMock<PathProviderMock> l_currentDirectoryProvider;
    ON_CALL(l_currentDirectoryProvider, getCurrentDirectory()).WillByDefault(Return(PDT_TEST_FILES_PATH));

    FilesLookup l_sut(l_currentDirectoryProvider);

    auto l_foundAssignments = l_sut.scanDirectoriesForAssignment();
    ASSERT_EQ(std::find(l_foundAssignments.begin(),
                             l_foundAssignments.end(),
                             Path(PDT_TEST_FILES_PATH).append(".pdt")),
         l_foundAssignments.end());

    std::set<Path> l_pathsFound;
    for (const auto &l_assignment : l_foundAssignments)
    {
        auto l_files = l_sut.scanFilesInPath(l_assignment);
        l_pathsFound.insert(l_files.begin(), l_files.end());
    }

    std::set<Path> l_expectedOutput{generateTestFilesStructure()};

    ASSERT_EQ(l_pathsFound.size(), l_expectedOutput.size());

    for(const auto& l_assignment : l_pathsFound)
    {
        ASSERT_NE(std::find(l_expectedOutput.begin(), l_expectedOutput.end(), l_assignment), l_expectedOutput.end());
    }
}
