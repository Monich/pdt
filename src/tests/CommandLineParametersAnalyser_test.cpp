#include "CommandLineParametersAnalyser.hpp"

#include <gtest/gtest.h>

using namespace ::testing;

bool operator==(const CommandLineConfiguration p_first, const CommandLineConfiguration p_other)
{
    return p_first.areDebugLogsEnabled == p_other.areDebugLogsEnabled;
}

class CommandLineParametersAnalyserTestSuite : public Test
{
public:

    static void analize(const std::vector<std::string>& p_parameters, CommandLineConfiguration p_expectedParameters)
    {
        CommandLineParametersAnalyser l_sut;

        auto l_result = l_sut.analyzeCommandLineParameters(p_parameters);
        EXPECT_EQ(*l_result, p_expectedParameters);
    }
};

TEST_F(CommandLineParametersAnalyserTestSuite, shouldReturnAllDisabledParametersIfNoInput)
{
  analize({}, CommandLineConfiguration{false});
}

TEST_F(CommandLineParametersAnalyserTestSuite, shouldReturnEnabledDebugLogIfFlagWasFound)
{
  analize({"-d"}, CommandLineConfiguration{true});
}
