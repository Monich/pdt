#include "LogWrapper.hpp"

#include "mocks/Logger_mock.hpp"

#include <gtest/gtest.h>

using namespace ::testing;

class LogWrapperTestSuite : public Test
{
protected:
    void SetUp() override
    {
        m_stringStream = std::make_shared<std::ostringstream>();
    }

    std::unique_ptr<LogWrapper> getWrapper()
    {
        std::shared_ptr<std::ostream> l_transformedStream = m_stringStream;
        return std::make_unique<LogWrapper>(l_transformedStream);
    }

    void checkOutput(const char* p_correctOutput)
    {
        ASSERT_STREQ(m_stringStream->str().c_str(), p_correctOutput);
    }

private:
    std::shared_ptr<std::ostringstream> m_stringStream;
};

TEST_F(LogWrapperTestSuite, ShouldPassParametersToWrapperLogger)
{
    constexpr const char *l_testTextInput = "TEXT";
    constexpr const char *l_testTextInputNext = " + Test";
    constexpr const char *l_testTextOutput = "TEXT + Test\n";

    getWrapper() << l_testTextInput << l_testTextInputNext;

    checkOutput(l_testTextOutput);
}

TEST_F(LogWrapperTestSuite, ShouldUseHumanizationWhenAvailable)
{
    constexpr const char *l_testTextInput = "TEXT + ";
    const StructureToHumanize l_structure(0, 1);
    constexpr const char *l_testTextInputNext = " + Test";
    constexpr const char *l_testTextOutput = "TEXT + 0 1 + Test\n";

    getWrapper() << l_testTextInput << l_structure << l_testTextInputNext;

    checkOutput(l_testTextOutput);
}

TEST(LogWrapper, ShouldNotHumanizeWhenStreamIsClosed)
{
    const StructureThrowOnHumanizer l_structure;

    std::shared_ptr<std::ostream> l_stream = nullptr;
    ASSERT_NO_THROW(std::unique_ptr<LogWrapper>(new LogWrapper(l_stream)) << l_structure);
}
