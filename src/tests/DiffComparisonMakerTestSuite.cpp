#include "DiffComparisonMaker.hpp"

#include "Assignment.hpp"

#include "TestHelpers.hpp"

#include <gtest/gtest.h>

#include <list>
#include <MarkGenerator.hpp>

namespace
{
    const Path ORIGIN_FILE = "orig";
    const Path ORIGIN_FILE2 = "orig2";
}

using namespace ::testing;

class DiffComparisonMakerTestSuite : public Test
{
public:
    void execute(const DiffComparison &p_result)
    {
        MarkGenerator l_generator{1, Assignment{"1", m_data1}, Assignment{"1", m_data2}};
        const auto l_result = DiffComparisonMaker().compareData(m_data1, m_data2, l_generator);
        ASSERT_EQ(l_result.lineHits, p_result.lineHits);
        ASSERT_EQ(l_result.linesAnalyzed, p_result.linesAnalyzed);
        ASSERT_EQ(l_result.duplicates.size(), p_result.duplicates.size());
        ASSERT_EQ(l_result, p_result);
    }

    void addFirstInput(const std::string &p_input, const Path& p_path = ORIGIN_FILE, u32 p_length = 1)
    {
        addInput(m_data1, p_input, p_path, p_length);
    }

    void addFirstInput(const std::vector<std::string> &p_input, const Path& p_path = ORIGIN_FILE)
    {
        for (const auto &l_input : p_input)
        {
            addFirstInput(l_input, p_path);
        }
    }

    void addSecondInput(const std::string &p_input, const Path& p_path = ORIGIN_FILE, u32 p_length = 1)
    {
        addInput(m_data2, p_input, p_path, p_length);
    }

    void addSecondInput(const std::vector<std::string> &p_input, const Path& p_path = ORIGIN_FILE)
    {
        for (const auto &l_input : p_input)
        {
            addSecondInput(l_input, p_path);
        }
    }

private:
    void addInput(AssignmentStream& data, const std::string& p_input, const Path& p_path, u32 p_lenght)
    {
        u32 l_lineNumber = std::count_if(data.begin(), data.end(),
                [p_path](const FileLineData& p_line) { return p_line.getOriginFile() == p_path; }) + 1;

        data.push_back({
            p_path,
            l_lineNumber,
            p_lenght,
            p_input
        });
    }

    AssignmentStream m_data1{};
    AssignmentStream m_data2{};
};

TEST_F(DiffComparisonMakerTestSuite, shouldReturnSuccessWithZeroHitsIfFilesHaveDifferentContents)
{
    addFirstInput("qwe");
    addSecondInput("asd");

    execute({ 0, 1 });
}

TEST_F(DiffComparisonMakerTestSuite, shouldReturnSuccessWithOneHitIfFilesAreEqual)
{
    constexpr const char* l_message = "qwe";

    addFirstInput(l_message);
    addSecondInput(l_message);

    execute({ 1, 1, {
        {
            {ORIGIN_FILE, 1, 1},
            {ORIGIN_FILE, 1, 1},
        }
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldAnalyzeEntireFirstFile)
{
    addFirstInput({"qwe", "asd", "zxc"});
    addSecondInput("zxc");

    execute({ 1, 3 , {
        {
            {ORIGIN_FILE, 3, 3},
            {ORIGIN_FILE, 1, 1},
        }
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldAnalyzeEntireSecondFile)
{
    addFirstInput("zxc");
    addSecondInput({"qwe", "asd", "zxc"});

    execute({ 1, 3 , {
        {
            {ORIGIN_FILE, 1, 1},
            {ORIGIN_FILE, 3, 3},
        }
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldAnalyzeMultiLineFiles)
{
    addFirstInput({"qwe", "asd", "zxc", "lkj", "mnb", "poi"});
    addSecondInput({"mnb", "lkj", "poi", "zxc", "asd", "qwe"});

    execute({ 6, 6, {
        {
            {ORIGIN_FILE, 1, 6},
            {ORIGIN_FILE, 1, 6},
        },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldAnalyzeTwoFiles)
{
    addFirstInput("qwe", ORIGIN_FILE);
    addFirstInput("asd", ORIGIN_FILE2);

    addSecondInput("qwe", ORIGIN_FILE2);
    addSecondInput("asd", ORIGIN_FILE);

    execute({ 2, 2, {
            {
                    {ORIGIN_FILE, 1, 1},
                    {ORIGIN_FILE2, 1, 1},
            },
            {
                    {ORIGIN_FILE2, 1, 1},
                    {ORIGIN_FILE, 1, 1},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldGetDuplicateLineInTheMiddleOfFirstAssignment)
{
    addFirstInput({"qwe", "asd", "zxc", "lkj", "mnb", "poi"});

    addSecondInput("mnb");
    addSecondInput("asd", ORIGIN_FILE2);

    execute({ 2, 6, {
            {
                    {ORIGIN_FILE, 2, 2},
                    {ORIGIN_FILE2, 1, 1},
            },
            {
                    {ORIGIN_FILE, 5, 5},
                    {ORIGIN_FILE, 1, 1},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldSplitDuplicateLinesFromTwoFilesInSecondAssignmentWhenTheyAreInSeperateFiles)
{
    addFirstInput({"qwe", "asd", "zxc", "lkj", "mnb", "poi"});

    addSecondInput("zxc");
    addSecondInput("lkj", ORIGIN_FILE2);

    execute({ 2, 6, {
            {
                    {ORIGIN_FILE, 3, 3},
                    {ORIGIN_FILE, 1, 1},
            },
            {
                    {ORIGIN_FILE, 4, 4},
                    {ORIGIN_FILE2, 1, 1},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldApplyCorrectLengthsForMultilineFilters)
{
    addFirstInput("qwe", ORIGIN_FILE, 10);
    addSecondInput("123", ORIGIN_FILE, 1);
    addSecondInput("123", ORIGIN_FILE, 12);
    addSecondInput("qwe", ORIGIN_FILE, 15);

    execute({ 2, 3, {
            {
                    {ORIGIN_FILE, 1, 10},
                    {ORIGIN_FILE, 3, 17},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldSplitLinesIfFirstFileIsNotCompatible)
{
    addFirstInput("A");
    addFirstInput("B");
    addFirstInput("C");
    addFirstInput("D");
    addSecondInput("A");
    addSecondInput("D");

    execute({ 2, 4, {
            {
                    {ORIGIN_FILE, 1, 1},
                    {ORIGIN_FILE, 1, 1},
            },
            {
                    {ORIGIN_FILE, 4, 4},
                    {ORIGIN_FILE, 2, 2},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldSplitLinesIfSecondFileIsNotCompatible)
{
    addFirstInput("A");
    addFirstInput("D");
    addSecondInput("A");
    addSecondInput("B");
    addSecondInput("C");
    addSecondInput("D");

    execute({ 2, 4, {
            {
                    {ORIGIN_FILE, 1, 1},
                    {ORIGIN_FILE, 1, 1},
            },
            {
                    {ORIGIN_FILE, 2, 2},
                    {ORIGIN_FILE, 4, 4},
            },
    }});
}


TEST_F(DiffComparisonMakerTestSuite, shouldFindDuplicatesIfTheyAreNotInOrder)
{
    addFirstInput("A");
    addFirstInput("B");
    addFirstInput("C");
    addFirstInput("D");

    addSecondInput("D");
    addSecondInput("E");
    addSecondInput("F");
    addSecondInput("G");
    addSecondInput("H");
    addSecondInput("B");

    execute({ 2, 6, {
            {
                    {ORIGIN_FILE, 2, 2},
                    {ORIGIN_FILE, 6, 6},
            },
            {
                    {ORIGIN_FILE, 4, 4},
                    {ORIGIN_FILE, 1, 1},
            },
    }});
}

TEST_F(DiffComparisonMakerTestSuite, shouldCorrectlySortReccuringData)
{
    addFirstInput("A");
    addFirstInput("B");
    addFirstInput("A");
    addFirstInput("C");

    addSecondInput("A");
    addSecondInput("B");
    addSecondInput("A");
    addSecondInput("C");

    execute({ 5, 4, {
            {
                    {ORIGIN_FILE, 1, 4},
                    {ORIGIN_FILE, 1, 4},
            },
    }});
}
