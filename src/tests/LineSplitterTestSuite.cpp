#include "LineSplitter.hpp"

#include "mocks/FileAccessor_mock.hpp"

namespace
{
    const std::string ORIGINAL_FILE_NAME{"orig"};
    const std::string NL{""};
}

struct LRTestData
{
    std::string data;
    u32 line;
    u32 lenght;
};

using namespace ::testing;

class LineSplitterTestSuite : public Test
{
public:
    void executeTest()
    {
        LineSplitter l_sut{};
        ASSERT_EQ(l_sut.splitCodeAndComments(m_inputData), std::make_pair(m_expectedCodeData, m_expectedCommentData));
    }

    void setInput(const std::string &p_content)
    {
        m_inputData.push_back({
            ORIGINAL_FILE_NAME,
            static_cast<u32>(m_inputData.size()) + 1,
            p_content
        });
    }

    void setInput(const std::vector<std::string> &p_content)
    {
        for(const auto& l_string : p_content)
        {
            setInput(l_string);
        }
    }

    void setCodeOutput(const LRTestData &p_content)
    {
        m_expectedCodeData.push_back({
            ORIGINAL_FILE_NAME,
            p_content.line,
            p_content.data
        });
    }

    void setCommentOutput(const LRTestData &p_content)
    {
        m_expectedCommentData.push_back({
            ORIGINAL_FILE_NAME,
            p_content.line,
            p_content.data
        });
    }

    void setCodeOutput(const std::vector<LRTestData> &p_content)
    {
        for(const auto& l_string : p_content)
        {
            setCodeOutput(l_string);
        }
    }

    void setCommentOutput(const std::vector<LRTestData> &p_content)
    {
        for(const auto& l_string : p_content)
        {
            setCommentOutput(l_string);
        }
    }

private:
    AssignmentStream m_inputData;
    AssignmentStream m_expectedCodeData{};
    AssignmentStream m_expectedCommentData{};
};

TEST_F(LineSplitterTestSuite, shouldCopy)
{
    setInput("qwe");
    setCodeOutput({"qwe", 1});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldRemoveWhitespacesAtBeginningAndEndOfTheLine)
{
    setInput({"  qwe", "\tzxc", "asd   "});
    setCodeOutput({{"qwe", 1}, {"zxc", 2}, {"asd", 3}});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldRemoveEmptyLines)
{
    setInput({"qwe", NL, NL, NL, "asd", NL, NL, NL, "zxc", NL, NL});
    setCodeOutput({{"qwe", 1}, {"asd", 5}, {"zxc", 9}});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldSplitOneLineComment)
{
    setInput({"qwe", NL, "//asd"});
    setCodeOutput({"qwe", 1});
    setCommentOutput({"asd", 3});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldSplitEndOfLineComment)
{
    setInput({"qwe//asd gx", "zxc", "qwe"});
    setCodeOutput({{"qwe", 1}, {"zxc", 2}, {"qwe", 3}});
    setCommentOutput({"asd gx", 1});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldSplitInlineComment)
{
    setInput("qwe/*asd gx*/zxc");
    setCodeOutput({"qwe zxc", 1});
    setCommentOutput({"asd gx", 1});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldNotSplitCommentInsideString)
{
    setInput(R"(qwe"/*asd*/"zxc"//as"d)");
    setCodeOutput({R"(qwe"/*asd*/"zxc"//as"d)", 1});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldSplitMultiLineComment)
{
    setInput({"qwe", "/* asd", "zxc", "*/qwe"});
    setCodeOutput({{"qwe", 1}, {"qwe", 4}});
    setCommentOutput({{"asd", 2}, {"zxc", 3}});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldRemoveIncludes)
{
    setInput({"qwe", "#include <asd>", "#include \"zxc.h\"", "qwe"});
    setCodeOutput({{"qwe", 1}, {"qwe", 4}});
    executeTest();
}

TEST_F(LineSplitterTestSuite, shouldConvertToLowercase)
{
    setInput("qweASDzxc");
    setCodeOutput({"qweasdzxc", 1});
    executeTest();
}
