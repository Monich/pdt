#pragma once

#include "IFilter.hpp"

class FilterBase : public IFilter
{
public:
    [[nodiscard]] AssignmentStream filter(const AssignmentStream &p_data) const override;

protected:
    [[nodiscard]] virtual std::string filterEntry(const std::string& p_entry) const = 0;
};
