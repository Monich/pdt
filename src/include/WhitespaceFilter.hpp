#pragma once

#include "FilterBase.hpp"

class WhitespaceFilter : public FilterBase
{
protected:
    [[nodiscard]] std::string filterEntry(const std::string &p_entry) const override;
};
