#pragma once

#include "IFilter.hpp"
#include "StringTokenizer.hpp"

class FunctionExtractorFilter : public IFilter, public StringTokenizer
{
public:
    AssignmentStream filter(const AssignmentStream &p_data) const override;
};
