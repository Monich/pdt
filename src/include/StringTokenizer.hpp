#pragma once

#include <map>
#include <string>
#include <vector>

class StringTokenizer
{
public:
    std::string tokenizeWithDictionary(const std::string& p_str, std::map<std::string, std::string>& p_dictionary) const;
    std::vector<std::string> tokenize(const std::string& p_str) const;

    bool isIdentifier(const std::string& p_input) const;
    void removeWhitespacesOnLineStartAndEnd(std::string &p_line) const;
    std::string removeWhitespacesOnLineStartAndEnd(const std::string &p_line) const;
private:
    std::string toDictionaryString(const std::string &p_input, std::map<std::string, std::string> &p_dictixonary) const;
};
