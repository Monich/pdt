#pragma once

#include "Defines.hpp"

class Logger;

using AssignmentFiles = std::set<Path>;

class FileLineData
{
public:
    FileLineData(Path p_originFile, u32 p_originLine, std::string p_lineData);
    FileLineData(Path p_originFile, u32 p_originLine, u32 p_length, std::string p_lineData);

    bool operator==(const FileLineData &p_rhs) const;
    bool operator!=(const FileLineData &p_rhs) const;

    [[nodiscard]] const std::string &getLineData() const;
    [[nodiscard]] u32 getOriginLine() const;
    [[nodiscard]] u32 getLength() const;
    [[nodiscard]] const Path &getOriginFile() const;
    [[nodiscard]] u32 getLastOriginLine() const;

private:
    Path m_originFile;
    u32 m_originLine;
    u32 m_length; // used in multiline filters
    std::string m_lineData;
};

using AssignmentStream = std::vector<FileLineData>;

class Assignment
{
public:
    Assignment(std::string p_assignmentName,
               AssignmentStream p_stepOneData);

    Assignment(std::string p_assignmentName,
               AssignmentStream p_stepOneData,
               AssignmentStream p_stepTwoData,
               AssignmentStream p_commentData,
               AssignmentStream p_tokenizedData,
               AssignmentStream p_tokenizedLineData,
               AssignmentStream p_noKeywordsData,
               AssignmentStream p_identifiersData,
               AssignmentStream p_functionExtractionData,
               AssignmentStream p_functionTokenizedData,
               AssignmentStream p_tokenizedFileData);

    [[nodiscard]] const std::string &getAssignmentName() const;
    [[nodiscard]] const AssignmentStream &getStepOneStream() const;
    [[nodiscard]] const AssignmentStream &getStepTwoStream() const;
    [[nodiscard]] const AssignmentStream &getCommentStream() const;
    [[nodiscard]] const AssignmentStream &getTokenizedStream() const;
    [[nodiscard]] const AssignmentStream &getTokenizedLineStream() const;
    [[nodiscard]] const AssignmentStream &getNoKeywordsStream() const;
    [[nodiscard]] const AssignmentStream &getIdentifiersStream() const;
    [[nodiscard]] const AssignmentStream &getFunctionExtractionStream() const;
    [[nodiscard]] const AssignmentStream &getFunctionTokenizedStream() const;
    [[nodiscard]] const AssignmentStream &getTokenizedFileStream() const;

    // For mapping
    bool operator<(const Assignment& p_other) const;
    bool operator==(const Assignment &p_other) const;

private:
    std::string m_assignmentName;
    AssignmentStream m_stepOneStream;
    AssignmentStream m_stepTwoStream;
    AssignmentStream m_commentStream;
    AssignmentStream m_tokenizedStream;
    AssignmentStream m_tokenizedLineStream;
    AssignmentStream m_noKeywordsStream;
    AssignmentStream m_identifiersStream;
    AssignmentStream m_functionExtractionStream;
    AssignmentStream m_functionTokenizedStream;
    AssignmentStream m_tokenizedFileStream;
};

using Assignments = std::vector<Assignment>;

void operator<<(Logger& p_logger, const AssignmentFiles& p_input);
