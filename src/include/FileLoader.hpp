#pragma once

#include "IFileLoader.hpp"
#include "Assignment.hpp"

class IFileAccessor;
class IPathProvider;

class FileLoader : public IFileLoader
{
public:
    explicit FileLoader(const IFileAccessor &p_fileAccessor);

    [[nodiscard]] AssignmentStream loadAssignmentData(const std::set<Path> &p_filesToLoad) const override;

private:
    const IFileAccessor &m_fileAccessor;
};
