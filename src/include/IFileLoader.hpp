#pragma once

#include "Assignment.hpp"

class IFileLoader
{
public:
    [[nodiscard]] virtual AssignmentStream loadAssignmentData(const std::set<Path> &p_filesToLoad) const = 0;

    IFileLoader() = default;
    virtual ~IFileLoader() = default;
    IFileLoader(const IFileLoader&) = delete;
    void operator=(const IFileLoader&) = delete;
    IFileLoader(IFileLoader&&) = delete;
    void operator=(IFileLoader&&) = delete;
};
