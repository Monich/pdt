#pragma once

#include "ICommandLineParametersAnalyzer.hpp"

class CommandLineParametersAnalyser : public ICommandLineParametersAnalyser
{
public:
    CommandLineConfigurationUPtr analyzeCommandLineParameters(
        const CommandLineParameters& p_parameters
        ) const override;
};
