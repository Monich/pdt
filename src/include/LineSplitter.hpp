#pragma once

#include "ILineSplitter.hpp"
#include "StringTokenizer.hpp"

class LineSplitter : public ILineSplitter, public StringTokenizer
{
public:
    std::pair<AssignmentStream, AssignmentStream> splitCodeAndComments(const AssignmentStream &p_stepOneData) const override;
};