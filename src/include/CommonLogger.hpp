#pragma once

#include "CurrentTimestamp.hpp"
#include "Defines.hpp"
#include "LogManager.hpp"
#include "LogWrapper.hpp"

#include <cstring>

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

// NOLINT
#define _LOG_INTERNAL(severity, sevString) LogManager::getSingletonInstance()\
                      ->getLogWithSeverity(severity)<< CurrentTimestamp() << " " <<\
                      (sevString) << __FILENAME__ << "#" << __LINE__ << ": "// NOLINT

#define LOG_INFO _LOG_INTERNAL(ELogSeverity_Info, "INF/") 
#define LOG_WARN _LOG_INTERNAL(ELogSeverity_Warning, "WRN/") 
#define LOG_ERROR _LOG_INTERNAL(ELogSeverity_Error, "ERR/") 
#define LOG_DEBUG _LOG_INTERNAL(ELogSeverity_Debug, "DBG/")
