#pragma once

#include "Defines.hpp"
#include "IMarkGenerator.hpp"

class Assignment;

class MarkGenerator : public IMarkGenerator
{
public:
    MarkGenerator(u32 mRate, const Assignment &mFirstAssignment, const Assignment &mSecondAssignment);

    void generateMark(DuplicateLinesMarker& p_linesMarker) const;
private:
    u32 m_rate;
    const Assignment& m_firstAssignment;
    const Assignment& m_secondAssignment;
};
