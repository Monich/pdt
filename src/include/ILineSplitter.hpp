#pragma once

#include "Assignment.hpp"

// This class generates II stage files
class ILineSplitter
{
public:
    virtual std::pair<AssignmentStream, AssignmentStream> splitCodeAndComments(const AssignmentStream& p_stepOneData) const = 0;

    ILineSplitter() = default;
    virtual ~ILineSplitter() = default;
    ILineSplitter(const ILineSplitter&) = delete;
    void operator=(const ILineSplitter&) = delete;
    ILineSplitter(ILineSplitter&&) = delete;
    void operator=(ILineSplitter&&) = delete;
};