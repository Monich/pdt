#pragma once

struct DuplicateLinesMarker;

class IMarkGenerator
{
public:
    virtual ~IMarkGenerator() = default;
    virtual void generateMark(DuplicateLinesMarker& p_linesMarker) const = 0;
};
