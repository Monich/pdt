#pragma once

#include "Defines.hpp"

#include <memory>
#include <optional>

struct CommandLineConfiguration
{
    bool areDebugLogsEnabled;
    bool isSimplifiedModeOn;
    std::optional<std::string> singleAssignmentName;
};

class CommandLineParametersException : public std::runtime_error
{
public:
    explicit CommandLineParametersException(const std::string& p_message);
};

using CommandLineConfigurationUPtr = std::unique_ptr<CommandLineConfiguration>;

class ICommandLineParametersAnalyser
{
public:
    virtual CommandLineConfigurationUPtr analyzeCommandLineParameters(const CommandLineParameters& p_parameters) const = 0;
};
