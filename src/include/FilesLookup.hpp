#pragma once

#include "IFilesLookup.hpp"

class IPathProvider;

class FilesLookup : public IFilesLookup
{
public:
    explicit FilesLookup(
        const IPathProvider& p_currentDirectoryProvider
        );

    [[nodiscard]] std::set<Path> scanFilesInPath(const Path & p_pathToBeScanned) const override;
    [[nodiscard]] std::set<Path> scanDirectoriesForAssignment() const override;
private:
    const IPathProvider& m_currentDirectoryProvider;
};
