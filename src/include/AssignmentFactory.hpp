#pragma once

#include "IAssignmentFactory.hpp"

class IFileLoader;
class IFilesLookup;
class IFilter;
class ILineSplitter;

class AssignmentFactory : public IAssignmentFactory
{
public:
    AssignmentFactory(const IFileLoader & p_fileLoader,
                      const IFilesLookup &p_filesLookup,
                      const ILineSplitter& p_lineSplitter,
                      const IFilter& p_whitespaceFilter,
                      const IFilter& p_assignmentTokenizerFilter,
                      const IFilter& p_lineTokenizerFilter,
                      const IFilter& p_keywordsRemoverFilter,
                      const IFilter& p_wordsSplitterFilter,
                      const IFilter& p_functionExtractionFilter,
                      const IFilter& p_fileTokenizerFilter);

    [[nodiscard]] Assignments loadAssignments(bool p_simplify) const override;

private:
    const IFileLoader & m_fileLoader;
    const IFilesLookup & m_filesLookup;
    const ILineSplitter& m_lineSplitter;
    const IFilter& m_whitespaceFilter;
    const IFilter& m_assignmentTokenizerFilter;
    const IFilter& m_lineTokenizerFilter;
    const IFilter& m_keywordsRemoverFilter;
    const IFilter& m_wordsSplitterFilter;
    const IFilter& m_functionExtractionFilter;
    const IFilter& m_fileTokenizerFilter;
};
