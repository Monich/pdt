#pragma once

#include "Assignment.hpp"

#include <map>

class AssignmentComparison;

using AssignmentComparisonMap = std::map<std::pair<const Assignment&, const Assignment&>, AssignmentComparison>;

class IHtmlOutputMaker
{
public:
    virtual EStatus createHtmlOutput(const Assignments& p_assignments,
                                     const AssignmentComparisonMap &p_comparisonMap) const = 0;
};