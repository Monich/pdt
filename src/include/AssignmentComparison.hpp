#pragma once

#include "Defines.hpp"

#include <list>

class Assignment;

struct LinesMarker
{
    Path fileName;
    u32 firstLine;
    u32 lastLine;

    bool operator==(const LinesMarker&) const;
};

struct DuplicateLinesMarker
{
    LinesMarker firstAssignmentMarker;
    LinesMarker secondAssignmentMarker;
    u32 mark{};
};

using DuplicateLinesMarkers = std::list<DuplicateLinesMarker>;

struct DiffComparison
{
    u64 lineHits;
    u64 linesAnalyzed;
    DuplicateLinesMarkers duplicates;

    u64 getWeight();
};

class AssignmentComparison
{
public:
    AssignmentComparison() = default;
    AssignmentComparison(DiffComparison p_firstStepComparison);
    AssignmentComparison(DiffComparison p_firstStepComparison,
                         DiffComparison p_secondStepComparison,
                         DiffComparison p_commentComparison,
                         DiffComparison p_tokenizedComparison,
                         DiffComparison p_tokenizedLineComparison,
                         DiffComparison p_noKeywordsComparison,
                         DiffComparison p_identifiersComparison,
                         DiffComparison p_functionExtractionComparison,
                         DiffComparison p_functionTokenizedComparison,
                         DiffComparison p_tokenizedFileComparison,
                         u32 firstAssignmentLineCount,
                         u32 secondAssignmentLineCount);

    [[nodiscard]] const DiffComparison &getFirstStepComparison() const;
    [[nodiscard]] const DiffComparison &getSecondStepComparison() const;
    [[nodiscard]] const DiffComparison &getCommentComparison() const;
    [[nodiscard]] const DiffComparison &getTokenizedComparison() const;
    [[nodiscard]] const DiffComparison &getTokenizedLineComparison() const;
    [[nodiscard]] const DiffComparison &getNoKeywordsComparison() const;
    [[nodiscard]] const DiffComparison &getIdentifiersComparison() const;
    [[nodiscard]] const DiffComparison &getFunctionExtractionComparison() const;
    [[nodiscard]] const DiffComparison &getFunctionTokenizedComparison() const;
    [[nodiscard]] const DiffComparison &getTokenizedFileComparison() const;

    u64 getWeight() const;
    double getFirstAssignmentPercent() const;
    double getSecondAssignmentPercent() const;

private:
    DiffComparison m_firstStepComparison;
    DiffComparison m_secondStepComparison;
    DiffComparison m_commentComparison;
    DiffComparison m_tokenizedComparison;
    DiffComparison m_tokenizedLineComparison;
    DiffComparison m_noKeywordsComparison;
    DiffComparison m_identifiersComparison;
    DiffComparison m_functionExtractionComparison;
    DiffComparison m_functionTokenizedComparison;
    DiffComparison m_tokenizedFileComparison;

    u64 m_weight;
    double m_firstAssignmentPercent;
    double m_secondAssignmentPercent;

    void calculateRates(u32 firstAssignmentLineCount, u32 secondAssignmentLineCount);
    void calculateWeight();
};
