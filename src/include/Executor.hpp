#pragma once

#include "Defines.hpp"

#include <memory>

class IAssignmentComparisonMaker;
class IAssignmentFactory;
class ICommandLineParametersAnalyser;
class IHtmlOutputMaker;

class Executor
{
public:
    Executor(const IAssignmentComparisonMaker &p_assignmentComparisonMaker,
             const IAssignmentFactory &p_assignmentFactory,
             const ICommandLineParametersAnalyser &p_commandLineParametersAnalyzer,
             const IHtmlOutputMaker &p_htmlOutputFormatter);

    void execute(const CommandLineParameters &p_commandLineParameters);

private:
    const IAssignmentComparisonMaker &m_assignmentComparisonMaker;
    const IAssignmentFactory &m_assignmentFactory;
    const ICommandLineParametersAnalyser &m_commandLineParametersAnalyzer;
    const IHtmlOutputMaker &m_htmlOutputFormatter;
};


