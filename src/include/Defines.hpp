#pragma once

#include <cstdint>
#include <set>

#ifdef _MSC_VER
#include <filesystem>
namespace fs = std::filesystem;
#else // _MSC_VER
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif // _MSC_VER

using i8 = int8_t;
using u8 = uint8_t;

using i16 = int16_t;
using u16 = uint16_t;

using i32 = int32_t;
using u32 = uint32_t;

using i64 = int64_t;
using u64 = uint64_t;

using Path = fs::path;
using CommandLineParameters = std::vector<std::string>;

enum EStatus : u8
{
    Success,
    PartialSuccess,
    Failure,
};

#ifdef WIN32
#define or ||
#define and &&
#define not !
#endif
