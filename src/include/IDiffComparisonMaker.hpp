#pragma once

#include "Assignment.hpp"
#include "Defines.hpp"
#include "IMarkGenerator.hpp"

#include <list>

struct DiffComparison;

class IDiffComparisonMaker
{
public:
    virtual DiffComparison compareData(const AssignmentStream &p_firstData, const AssignmentStream &p_secondData,
                                       const IMarkGenerator &p_markGenerator) const = 0;

    IDiffComparisonMaker() = default;
    virtual ~IDiffComparisonMaker() = default;
    IDiffComparisonMaker(const IDiffComparisonMaker&) = delete;
    void operator=(const IDiffComparisonMaker&) = delete;
    IDiffComparisonMaker(IDiffComparisonMaker&&) = delete;
    void operator=(IDiffComparisonMaker&&) = delete;
};
