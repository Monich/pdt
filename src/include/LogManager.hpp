#pragma once

#include <fstream>
#include <memory>

class LogWrapper;

enum ELogSeverity
{
    ELogSeverity_Info,
    ELogSeverity_Warning,
    ELogSeverity_Error,
    ELogSeverity_Debug,
};

class LogManager
{
public:
    ~LogManager();

    bool initialize(bool p_shouldLogDebugLogs);
    void terminate();

    bool isInitialized() const;

    std::unique_ptr<LogWrapper> getLogWithSeverity(ELogSeverity p_severity) const;

    static LogManager *getSingletonInstance();

    LogManager(const LogManager&) = delete;
    void operator=(const LogManager&) = delete;
    LogManager(LogManager&&) = delete;
    void operator=(LogManager&&) = delete;
private:
    //! Stream to log file
    std::shared_ptr<std::ofstream> m_fileLog;
    //! Flag for debug logging
    bool m_shouldLogDebugLogs{false};

    LogManager() = default;
};
