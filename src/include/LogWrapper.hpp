#pragma once

#include "Logger.hpp"

class LogWrapper
{
public:
    explicit LogWrapper(const std::shared_ptr<std::ostream> &p_stream);
private:
    //! Logger to pass humanized objects to.
    std::unique_ptr<Logger> m_logger;

    template<typename T>
    friend std::unique_ptr<LogWrapper> operator<<(std::unique_ptr<LogWrapper> p_wrapper, T p_input);
};

template<typename T>
std::unique_ptr<LogWrapper> operator<<(std::unique_ptr<LogWrapper> p_wrapper, T p_input)
{
    if(p_wrapper->m_logger)
    {
        *p_wrapper->m_logger << p_input;
    }

    return std::move(p_wrapper);
}
