#pragma once

#include "Defines.hpp"

class DuplicateRatingCalculator
{
public:
    DuplicateRatingCalculator(unsigned int p_staticFactor, unsigned int p_dynamicFactor = 1);
    unsigned operator()(unsigned p_length) const;

private:
    u32 m_staticFactor;
    u32 m_dynamicFactor;
};