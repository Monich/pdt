#pragma once

#include "Logger.hpp"

inline void operator<<(Logger& p_logger, const EStatus p_status)
{
    const char * l_printee;
    switch (p_status)
    {
        case Success:
            l_printee = "Success";
            break;
        case Failure:
            l_printee = "Failure";
            break;
        case PartialSuccess:
            l_printee = "Partial Success";
            break;
        default:
            l_printee = "ERROR";
    }

    p_logger << l_printee;
}

#ifndef _MSC_VER
// Not usable in MSVC
inline void operator<<(Logger& p_logger, const bool p_boolean)
{
    p_logger << (p_boolean ? "true" : "false");
}

#endif
