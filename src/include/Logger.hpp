#pragma once

#include <memory>
#include <sstream>

class Logger : public std::stringstream
{
public:
    explicit Logger(std::weak_ptr<std::ostream> p_flushStream);
    ~Logger() override;
    
    Logger(const Logger&) = delete;
    void operator=(const Logger&) = delete;
    Logger(Logger&&) = delete;
    void operator=(Logger&&) = delete;
private:
    //! Stream to flush at the end of life cycle. Received as parameter during initialization.
    std::weak_ptr<std::ostream> m_flushStream;
};

using Log = std::stringstream;
