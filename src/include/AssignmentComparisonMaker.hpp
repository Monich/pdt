#pragma once

#include "IAssignmentComparisonMaker.hpp"

class IDiffComparisonMaker;

class AssignmentComparisonMaker : public IAssignmentComparisonMaker
{
public:
    AssignmentComparisonMaker(IDiffComparisonMaker &p_diffComparisonMaker);

    virtual AssignmentComparison compareAssignments(const Assignment &p_firstAssignment,
                                                    const Assignment &p_secondAssignment) const override;

private:
    IDiffComparisonMaker &m_diffComparisonMaker;
};