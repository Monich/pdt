#pragma once

#include "IFilter.hpp"
#include "StringTokenizer.hpp"

class WordsSplitterFilter : public IFilter, public StringTokenizer
{
public:
    AssignmentStream filter(const AssignmentStream &p_data) const override;
};
