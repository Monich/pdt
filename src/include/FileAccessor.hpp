#pragma once

#include "IFileAccessor.hpp"

class FileAccessor : public IFileAccessor
{
    std::unique_ptr<std::istream> getInputStream(const Path &p_path) const override;
    std::unique_ptr<std::ostream> getOutputStream(const Path &p_path) const override;
};
