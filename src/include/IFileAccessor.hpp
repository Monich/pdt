#pragma once

#include "Defines.hpp"

#include <istream>
#include <memory>

class IFileAccessor
{
public:
    virtual std::unique_ptr<std::istream> getInputStream(const Path &p_path) const = 0;
    virtual std::unique_ptr<std::ostream> getOutputStream(const Path &p_path) const = 0;

    IFileAccessor() = default;
    virtual ~IFileAccessor() = default;
    IFileAccessor(const IFileAccessor&) = delete;
    void operator=(const IFileAccessor&) = delete;
    IFileAccessor(IFileAccessor&&) = delete;
    void operator=(IFileAccessor&&) = delete;
};
