#pragma once

#include "Defines.hpp"

#include <utility>

class Assignment;
using Assignments = std::vector<Assignment>;

class IFilesLookup
{
public:
    virtual std::set<Path> scanFilesInPath(const Path & p_pathToBeScanned) const = 0;
    virtual std::set<Path> scanDirectoriesForAssignment() const = 0;

    IFilesLookup() = default;
    virtual ~IFilesLookup() = default;
    IFilesLookup(const IFilesLookup&) = delete;
    void operator=(const IFilesLookup&) = delete;
    IFilesLookup(IFilesLookup&&) = delete;
    void operator=(IFilesLookup&&) = delete;
};
