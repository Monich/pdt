#pragma once

#include "Assignment.hpp"

class IFilter
{
public:
    virtual ~IFilter() = default;
    [[nodiscard]] virtual AssignmentStream filter(const AssignmentStream& p_data) const = 0;
};
