#pragma once

#include "IHtmlOutputMaker.hpp"

class IFileAccessor;
class IPathProvider;

namespace
{
    struct DuplicateEntry;
}

class HtmlOutputMaker : public IHtmlOutputMaker
{
public:
    HtmlOutputMaker(const IFileAccessor &p_fileAccessor, const IPathProvider &p_pathProvider);

    EStatus createHtmlOutput(const Assignments &p_assignments,
                             const AssignmentComparisonMap &p_comparisonMap) const override;

private:
    const IFileAccessor& m_fileAccessor;
    const IPathProvider& m_pathProvider;

    EStatus createHtmlIndexFile(const AssignmentComparisonMap &p_comparisonMap, u32 p_assignmentCount) const;
    EStatus createHtmlComparisonFiles(const Assignments &p_assignments,
                                      const AssignmentComparisonMap &p_comparisonMap) const;
    EStatus createHtmlComparisonFile(const Assignment &p_firstAssignment,
                                     const Assignment &p_secondAssignment,
                                     const AssignmentComparison &p_comparison,
                                     const std::string& p_headerFileContents,
                                     const std::string& p_entryFileContents,
                                     const std::string& p_footerFileContents,
                                     const std::string& p_detailsFileContents) const;
    void formatEntry(const DuplicateEntry& p_entry,
                     const std::string& p_entryFileContents,
                     std::ostream& p_output,
                     const std::string &p_firstAssignmentName,
                     const std::string &p_secondAssignmentName,
                     u32 p_id) const;

    EStatus createHtmlDetailsPage(const DuplicateEntry &p_entry,
                                  const Assignment &p_firstAssignment,
                                  const Assignment &p_secondAssignment,
                                  const std::string &p_detailsFileContents,
                                  u32 p_id) const;
};
