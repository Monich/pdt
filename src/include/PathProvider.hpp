#pragma once

#include "IPathProvider.hpp"

class PathProvider : public IPathProvider
{
public:
    explicit PathProvider(const Path &p_applicationPath);

    [[nodiscard]] Path getCurrentDirectory() const override;
    [[nodiscard]] Path getIndexHeader() const override;

    [[nodiscard]] Path getIndexSiteHeaderPath() const override;
    [[nodiscard]] Path getIndexSiteEntryPath() const override;
    [[nodiscard]] Path getIndexSiteFooterPath() const override;

    [[nodiscard]] Path getComparisonSiteHeaderPath() const override;
    [[nodiscard]] Path getComparisonSiteEntryPath() const override;
    [[nodiscard]] Path getComparisonSiteFooterPath() const override;
    [[nodiscard]] Path getDetailsSiteFooterPath() const override;

    [[nodiscard]] Path getComparisonOutputPage(const Assignment&, const Assignment&) const override;

private:
    const Path m_applicationUtilitiesDirectory;
};
