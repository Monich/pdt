#pragma once

#include "IDiffComparisonMaker.hpp"

class Assignment;
class AssignmentComparison;

class IAssignmentComparisonMaker
{
public:
    virtual AssignmentComparison compareAssignments(const Assignment &p_firstAssignment, const Assignment &p_secondAssignment) const = 0;

    IAssignmentComparisonMaker() = default;
    virtual ~IAssignmentComparisonMaker() = default;
    IAssignmentComparisonMaker(const IAssignmentComparisonMaker&) = delete;
    void operator=(const IAssignmentComparisonMaker&) = delete;
    IAssignmentComparisonMaker(IAssignmentComparisonMaker&&) = delete;
    void operator=(IAssignmentComparisonMaker&&) = delete;
};