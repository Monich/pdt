#pragma once

#include "FilterBase.hpp"
#include "StringTokenizer.hpp"

class KeywordsRemoverFilter : public FilterBase, public StringTokenizer
{
protected:
    std::string filterEntry(const std::string &p_entry) const override;
};
