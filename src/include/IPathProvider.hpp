#pragma once

#include "Defines.hpp"

class Assignment;

class IPathProvider
{
public:
    virtual Path getCurrentDirectory() const = 0;
    virtual Path getIndexHeader() const = 0;

    virtual Path getIndexSiteHeaderPath() const = 0;
    virtual Path getIndexSiteEntryPath() const = 0;
    virtual Path getIndexSiteFooterPath() const = 0;

    virtual Path getComparisonSiteHeaderPath() const = 0;
    virtual Path getComparisonSiteEntryPath() const = 0;
    virtual Path getComparisonSiteFooterPath() const = 0;
    virtual Path getDetailsSiteFooterPath() const = 0;

    virtual Path getComparisonOutputPage(const Assignment&, const Assignment&) const = 0;

    IPathProvider() = default;
    virtual ~IPathProvider() = default;
    IPathProvider(const IPathProvider&) = delete;
    void operator=(const IPathProvider&) = delete;
    IPathProvider(IPathProvider&&) = delete;
    void operator=(IPathProvider&&) = delete;
};
