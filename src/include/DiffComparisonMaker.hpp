#pragma once

#include "IDiffComparisonMaker.hpp"
#include "IMarkGenerator.hpp"

class IFileAccessor;

class DiffComparisonMaker : public IDiffComparisonMaker
{
public:
    DiffComparison compareData(const AssignmentStream &p_firstPath, const AssignmentStream &p_secondPath,
                               const IMarkGenerator &p_markGenerator) const override;
};
