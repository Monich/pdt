#pragma once

#include "AssignmentTokenizerFilter.hpp"

class FileTokenizerFilter : public AssignmentTokenizerFilter
{
public:
    AssignmentStream filter(const AssignmentStream &p_data) const override;
};
