#pragma once

#include "Assignment.hpp"

class IAssignmentFactory
{
public:
    virtual Assignments loadAssignments(bool p_simplify) const = 0;

    IAssignmentFactory() = default;
    virtual ~IAssignmentFactory() = default;
    IAssignmentFactory(const IAssignmentFactory&) = delete;
    void operator=(const IAssignmentFactory&) = delete;
    IAssignmentFactory(IAssignmentFactory&&) = delete;
    void operator=(IAssignmentFactory&&) = delete;
};
