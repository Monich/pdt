#include "ApplicationBase.hpp"

int main(int argc, const char** argv)
{
    ApplicationBase::launch(argc, argv);
}