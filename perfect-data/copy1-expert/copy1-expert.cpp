// Kolejność parametrów
// Nowe metody
// Zmiany w logice
#include <iostream>
using namespace std;

enum Color{
    Invalid,
    Red,
    Black
};

struct CNode
{
    int value{};

    CNode* father = nullptr;
    CNode* leftChild = nullptr;
    CNode* rightChild = nullptr;

    Color color{Invalid};
};

class RedBlackTree
{
	CNode* rootNode;

    public:
        RedBlackTree() : rootNode(nullptr)
		{}

        CNode* getRoot(){
			return rootNode;
		}

         void insertNode(int value)
         {
           if(rootNode != nullptr)
		   {
               auto linkerNode = getRoot();
               auto newNode = new CNode();
               newNode->value = value;

               while(linkerNode != nullptr)
               {
                   if(linkerNode->value <= value)
                   {
                       if(linkerNode->rightChild != nullptr)
                       {
                           linkerNode = linkerNode->rightChild;
                       }
                       else
                       {
                           cout << "Element inserted.\n";
                           linkerNode->rightChild = newNode;
                           newNode->father = linkerNode;
                           newNode->color = Red;
                           break;
                       }
                   }
                   else
                   {
                       if(linkerNode->leftChild == nullptr)
                       {
                           cout << "Element inserted.\n";
                           newNode->color = Red;
                           newNode->father = linkerNode;
                           linkerNode->leftChild = newNode;
                           break;
                       }
                       else {
                           linkerNode = linkerNode->leftChild;
                       }
                   }
               }
               fixAfterInsert(newNode);
           }
           else
		   {
               cout << "Element inserted.\n";
               rootNode = new CNode();
               rootNode->father = nullptr;
               rootNode->color = Black;
               rootNode->value = value;
           }
        }

        void fixAfterInsert(CNode* z)
		{
            while(z->father->color == Red)
			{
                auto auntie = getRoot();
                auto grandfather = z->father->father;
                if(z->father != grandfather->leftChild)
				{
                    if(grandfather->leftChild)
                    {
                        auntie = grandfather->leftChild;
                    }
                    if(auntie->color == Red)
                    {
                        z->father->color = Black;
                        grandfather->color = Red;
                        auntie->color = Black;
                        if(grandfather->value != rootNode->value)
                        {
                            z = grandfather;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else if(z == grandfather->rightChild->leftChild)
                    {
                        rightRotate(z->father);
                    }
                    else
                    {
                        grandfather->color = Red;
                        z->father->color = Black;
                        leftRotate(grandfather);
                        if(grandfather->value != rootNode->value)
                        {
                            z = grandfather;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if(grandfather->rightChild)
                    {
                        auntie = grandfather->rightChild;
                    }
                    if(auntie->color == Red)
                    {
                        z->father->color = Black;
                        grandfather->color = Red;
                        auntie->color = Black;
                        if(grandfather->value == rootNode->value){
                            break;
                        }
                        else
                        {
                            z = grandfather;
                        }
                    }
                    else if(z == grandfather->leftChild->rightChild)
                    {
                        leftRotate(z->father);
                    }
                    else
                    {
                        grandfather->color = Red;
                        z->father->color = Black;
                        rightRotate(grandfather);
                        if(grandfather->value == rootNode->value)
                        {
                            break;
                        }
                        else
                        {
                            z = grandfather;
                        }
                    }
                }
            }
            rootNode->color = Black;
        }


        void removeNode(int value, CNode* current, CNode* father)
		{
            if(current == nullptr)
			{
				return;
			}

            if(current->value == value)
			{
                if(current->leftChild == nullptr && current->rightChild == nullptr)
				{
                    if(father->value == current->value)
					{
                        rootNode = nullptr;
					}
                    else if(father->rightChild != current)
					{
                        fixOnDelete(current);
                        father->leftChild = nullptr;
                    }
                    else
					{
                        fixOnDelete(current);
                        father->rightChild = nullptr;
                    }
                }
                else if(current->leftChild != nullptr && current->rightChild == nullptr)
				{
                    int swapValue = current->value;
                    current->value = current->leftChild->value;
                    current->leftChild->value = swapValue;
                    removeNode(value, current->rightChild, current);
                }
                else if(current->leftChild == nullptr && current->rightChild != nullptr)
				{
                    int swap = current->value;
                    current->value = current->rightChild->value;
                    current->rightChild->value = swap;
                    removeNode(value, current->rightChild, current);
                }
                else
				{
                    bool isLeftChildPresent = false;
                    CNode* tempNode = current->rightChild;
                    while(tempNode->leftChild)
					{
                        isLeftChildPresent = true;
						father = tempNode;
                        tempNode = tempNode->leftChild;
					}
                    if(!isLeftChildPresent)
					{
                        father = current;
					}

                    int swap = current->value;
                    current->value = tempNode->value;
                    tempNode->value = swap;
                    removeNode(swap, tempNode, father);
                }
            }
        }

        void remove(int stuff)
		{
            bool flag = false;
            auto parent = rootNode;
            auto temp = parent;
            if(!temp)
			{
                removeNode(stuff, nullptr, nullptr);
			}

            while(temp)
			{
                if(stuff == temp->value) { flag = true;
                    removeNode(stuff, temp, parent); break; }
                else if(stuff >= temp->value) { parent = temp ; temp = temp->rightChild; }
                else { parent = temp ; temp = temp->leftChild; }
            }

            if(!flag)
			{
				cout << "\nElement doesn't exist in the table";
			}
        }

        void fixOnDelete(CNode* z)
		{
            CNode* temp = z;
            while(temp->value != rootNode->value && temp->color == Black)
			{
                auto brother = getRoot();
                if(temp->father->leftChild == temp)
				{
                    if(temp->father->rightChild)
					{
                        brother = temp->father->rightChild;
					}
                    if(brother)
					{
                        if(brother->color == Red)
						{
                            temp->father->color = Red;
                            brother->color = Black;
                            leftRotate(temp->father);
                            brother = temp->father->rightChild;
                        }

                        if(brother->leftChild == nullptr && brother->rightChild == nullptr)
						{
                            temp = temp->father;
                            brother->color = Red;
                        }
                        else if(brother->leftChild->color == Black && brother->rightChild->color == Black)
						{
                            temp = temp->father;
                            brother->color = Red;
                        }
                        else if(brother->rightChild->color == Black)
						{
                            brother->leftChild->color = Black;
                            brother->color = Red;
                            rightRotate(brother);
                            brother = temp->father->rightChild;
                        }
						else
						{
                            brother->color = temp->father->color;
                            temp->father->color = Black;
                            if(brother->rightChild)
							{
                                brother->rightChild->color = Black;
							}
                            leftRotate(temp->father);
                            temp = rootNode;
                        }
                    }
                }
				else
				{
                    if(temp->father->rightChild == temp)
					{
                        if(temp->father->leftChild)
						{
                            brother = temp->father->leftChild;
						}
                        if(brother)
						{
                            if(brother->color == Red)
							{
                                temp->father->color = Red;
                                brother->color = Black;
                                rightRotate(temp->father);
                                brother = temp->father->leftChild;
                            }
                             if(brother->leftChild == nullptr && brother->rightChild == nullptr)
							{
                                temp = temp->father;
                                brother->color = Red;
                            }
                            else if(brother->leftChild->color == Black && brother->rightChild->color == Black)
							{
                                temp = temp->father;
                                brother->color = Red;
                            }
                            else if(brother->leftChild->color == Black)
							{
                                brother->color = Red;
                                brother->rightChild->color = Black;
                                rightRotate(brother);
                            }
							else
							{
                                brother->color = temp->father->color;
                                temp->father->color = Black;
                                if(brother->leftChild)
								{
                                    brother->leftChild->color = Black;
								}
                                leftRotate(temp->father);
                                temp = rootNode;
                            }
                        }
                    }

                }
            }
            temp->color = Black;
        }

         static void leftRotate(CNode* node)
		 {
            auto newNode = new CNode();
            if(node->rightChild->leftChild)
			{
                newNode->rightChild = node->rightChild->leftChild;
			}
             newNode->value = node->value;
             newNode->color = node->color;
             node->value = node->rightChild->value;
             newNode->leftChild = node->leftChild;
             node->leftChild = newNode;

             if(newNode->leftChild)
             {
                 newNode->leftChild->father = newNode;
             }
             if(newNode->rightChild)
             {
                 newNode->rightChild->father = newNode;
             }
             newNode->father = node;

            if(!node->rightChild->rightChild)
			{
                node->rightChild = nullptr;
			}
            else
			{
                node->rightChild = node->rightChild->rightChild;
			}

            if(node->rightChild)
			{
                node->rightChild->father = node;
			}
        }

        void rightRotate(CNode* x)
		{
            CNode* newNode = new CNode();
            if(x->leftChild->rightChild)
			{
                newNode->leftChild = x->leftChild->rightChild;
			}
            newNode->color = x->color;
            newNode->value = x->value;
            newNode->rightChild = x->rightChild;

            x->color = x->leftChild->color;
            x->value = x->leftChild->value;

            x->rightChild = newNode;

            if(newNode->leftChild)
            {
                newNode->leftChild->father = newNode;
            }
            if(newNode->rightChild)
            {
                newNode->rightChild->father = newNode;
            }

            newNode->father = x;

            if(x->leftChild->leftChild)
			{
				x->leftChild = x->leftChild->leftChild;
			}
            else
			{
				x->leftChild = nullptr;
			}

            if(x->leftChild)
			{
				x->leftChild->father = x;
			}
        }
 };
