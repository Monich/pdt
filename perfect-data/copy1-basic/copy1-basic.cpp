// Dodano formatowanie tekstu
// Dodano zmiany nazw
#include <iostream>
#include <string>
using namespace std;

struct CNode
{
    int value{};

    CNode* leftChild = nullptr;
    CNode* rightChild = nullptr;
    CNode* father = nullptr;

    string colour;
};

class RedBlackTree
{

	CNode* rootNode;

    public:
        RedBlackTree() : rootNode(nullptr)
		{}

        CNode* getRoot(){
			return rootNode;
		}

         void insertNode(int value) {
           if(rootNode == nullptr)
		   {
               rootNode = new CNode();
               rootNode->value = value;
               rootNode->father = nullptr;
               rootNode->colour = "BLACK";
               cout << "Element inserted.\n";
           }
           else
		   {
               auto linkerNode = getRoot();
               CNode* newNode = new CNode();
               newNode->value = value;

               while(linkerNode != nullptr)
			   {
                   if(linkerNode->value > value)
				   {
                       if(linkerNode->leftChild == nullptr)
					   {
                           linkerNode->leftChild = newNode;
                           newNode->colour = "RED";
                           newNode->father = linkerNode;
                           cout << "Element inserted.\n";
						   break;
						   }
                       else {
                           linkerNode = linkerNode->leftChild;
						}
                    }
				   else
					{
                       if(linkerNode->rightChild == nullptr)
					    {
                            linkerNode->rightChild = newNode;
                            newNode->colour = "RED";
                            newNode->father = linkerNode;
                           cout << "Element inserted.\n";
						   break;
						}
                        else
					    {
                            linkerNode = linkerNode->rightChild;
						}
                   }
               }
               fixAfterInsert(newNode);
           }
        }

        void fixAfterInsert(CNode* z)
		{
            while(z->father->colour == "RED")
			{
                auto grandfather = z->father->father;
                auto auntie = getRoot();
                if(z->father == grandfather->leftChild)
				{
                    if(grandfather->rightChild)
					{
                        auntie = grandfather->rightChild;
					}
                    if(auntie->colour == "RED")
					{
                        z->father->colour = "BLACK";
                        auntie->colour = "BLACK";
                        grandfather->colour = "RED";
                        if(grandfather->value != rootNode->value){
							z = grandfather;
						}
                        else
						{
							break;
						}
                    }
                    else if(z == grandfather->leftChild->rightChild)
					{
                        leftRotate(z->father);
                    }
                    else
					{
                        z->father->colour = "BLACK";
                        grandfather->colour = "RED";
                        rightRotate(grandfather);
                        if(grandfather->value != rootNode->value)
						{
							z = grandfather;
						}
                        else
						{
							break;
						}
                    }
                }
                else {
                    if(grandfather->leftChild)
					{
                        auntie = grandfather->leftChild;
					}
                    if(auntie->colour == "RED")
					{
                        z->father->colour = "BLACK";
                        auntie->colour = "BLACK";
                        grandfather->colour = "RED";
                        if(grandfather->value != rootNode->value)
						{
							z = grandfather;
						}
                        else
						{
							break;
						}
                    }
                    else if(z == grandfather->rightChild->leftChild)
					{
                        rightRotate(z->father);
                    }
                    else
					{
                        z->father->colour = "BLACK";
                        grandfather->colour = "RED";
                        leftRotate(grandfather);
                        if(grandfather->value != rootNode->value)
						{
							z = grandfather;
						}
                        else
						{
							break;
						}
                    }
                }
            }
            rootNode->colour = "BLACK";
        }


        void removeNode(CNode* father, CNode* current, int value)
		{
            if(current == nullptr)
			{
				return;
			}
            if(current->value == value)
			{
                if(current->leftChild == nullptr && current->rightChild == nullptr)
				{
                    if(father->value == current->value)
					{
                        rootNode = nullptr;
					}
                    else if(father->rightChild == current)
					{
                        fixOnDelete(current);
                        father->rightChild = nullptr;
                    }
                    else
					{
                        fixOnDelete(current);
                        father->leftChild = nullptr;
                    }
                }
                else if(current->leftChild != nullptr && current->rightChild == nullptr)
				{
                    int swapValue = current->value;
                    current->value = current->leftChild->value;
                    current->leftChild->value = swapValue;
                    removeNode(current, current->rightChild, value);
                }
                else if(current->leftChild == nullptr && current->rightChild != nullptr)
				{
                    int swap = current->value;
                    current->value = current->rightChild->value;
                    current->rightChild->value = swap;
                    removeNode(current, current->rightChild, value);
                }
                else
				{
                    bool isLeftChildPresent = false;
                    CNode* tempNode = current->rightChild;
                    while(tempNode->leftChild)
					{
                        isLeftChildPresent = true;
						father = tempNode;
                        tempNode = tempNode->leftChild;
					}
                    if(!isLeftChildPresent)
					{
                        father = current;
					}
                    int swap = current->value;
                    current->value = tempNode->value;
                    tempNode->value = swap;
                    removeNode(father, tempNode, swap);
                }
            }
        }

        void remove(int stuff)
		{
            auto temp = rootNode;
            auto parent = temp;
            bool flag = false;
            if(!temp)
			{
                removeNode(nullptr, nullptr, stuff);
			}

            while(temp)
			{
                if(stuff == temp->value) { flag = true;
                    removeNode(parent, temp, stuff); break; }
                else if(stuff < temp->value) { parent = temp ; temp = temp->leftChild; }
                else { parent = temp ; temp = temp->rightChild; }
            }

            if(!flag)
			{
				cout << "\nElement doesn't exist in the table";
			}
        }

        void fixOnDelete(CNode* z)
		{
            while(z->value != rootNode->value && z->colour == "BLACK")
			{
                auto brother = getRoot();
                if(z->father->leftChild == z)
				{
                    if(z->father->rightChild)
					{
                        brother = z->father->rightChild;
					}
                    if(brother)
					{
                        if(brother->colour == "RED")
						{
                            brother->colour = "BLACK";
                            z->father->colour = "RED";
                            leftRotate(z->father);
                            brother = z->father->rightChild;
                        }
                        if(brother->leftChild == nullptr && brother->rightChild == nullptr)
						{
                            brother->colour = "RED";
                            z = z->father;
                        }
                        else if(brother->leftChild->colour == "BLACK" && brother->rightChild->colour == "BLACK")
						{
                            brother->colour = "RED";
                            z = z->father;
                        }
                        else if(brother->rightChild->colour == "BLACK")
						{
                            brother->leftChild->colour = "BLACK";
                            brother->colour = "RED";
                            rightRotate(brother);
                            brother = z->father->rightChild;
                        }
						else
						{
                            brother->colour = z->father->colour;
                            z->father->colour = "BLACK";
                            if(brother->rightChild)
							{
                                brother->rightChild->colour = "BLACK";
							}
                            leftRotate(z->father);
                            z = rootNode;
                        }
                    }
                }
				else
				{
                    if(z->father->rightChild == z)
					{
                        if(z->father->leftChild)
						{
                            brother = z->father->leftChild;
						}
                        if(brother)
						{
                            if(brother->colour == "RED")
							{
                                brother->colour = "BLACK";
                                z->father->colour = "RED";
                                rightRotate(z->father);
                                brother = z->father->leftChild;
                            }
                             if(brother->leftChild == nullptr && brother->rightChild == nullptr)
							{
                                brother->colour = "RED";
                                z = z->father;
                            }
                            else if(brother->leftChild->colour == "BLACK" && brother->rightChild->colour == "BLACK")
							{
                                brother->colour = "RED";
                                z = z->father;
                            }
                            else if(brother->leftChild->colour == "BLACK")
							{
                                brother->rightChild->colour = "BLACK";
                                brother->colour = "RED";
                                rightRotate(brother);
                                brother = z->father->leftChild;
                            }
							else
							{
                                brother->colour = z->father->colour;
                                z->father->colour = "BLACK";
                                if(brother->leftChild)
								{
                                    brother->leftChild->colour = "BLACK";
								}
                                leftRotate(z->father);
                                z = rootNode;
                            }
                        }
                    }

                }
            }
            z->colour = "BLACK";
        }

        CNode* search(int value)
		{
            auto tempNode = getRoot();
            if(tempNode == nullptr)
			{
				return nullptr;
			}

            while(tempNode)
			{
                if(value == tempNode->value)
				{
					return tempNode;
				}
                else if(value < tempNode->value)
				{
                    tempNode = tempNode->leftChild;
				}
                else
				{
                    tempNode = tempNode->rightChild;
				}
            }
            return nullptr;
        }

         void leftRotate(CNode* node)
		 {
            CNode* newNode = new CNode();
            if(node->rightChild->leftChild)
			{
                newNode->rightChild = node->rightChild->leftChild;
			}
             newNode->leftChild = node->leftChild;
             newNode->value = node->value;
             newNode->colour = node->colour;
             node->value = node->rightChild->value;

             node->leftChild = newNode;
            if(newNode->leftChild)
			{
                newNode->leftChild->father = newNode;
			}
            if(newNode->rightChild)
			{
                newNode->rightChild->father = newNode;
			}
             newNode->father = node;

            if(node->rightChild->rightChild)
			{
                node->rightChild = node->rightChild->rightChild;
			}
            else
			{
                node->rightChild = nullptr;
			}

            if(node->rightChild)
			{
                node->rightChild->father = node;
			}
        }

        void rightRotate(CNode* x)
		{
            CNode* newNode = new CNode();
            if(x->leftChild->rightChild)
			{
                newNode->leftChild = x->leftChild->rightChild;
			}
            newNode->rightChild = x->rightChild;
            newNode->value = x->value;
            newNode->colour = x->colour;

            x->value = x->leftChild->value;
            x->colour = x->leftChild->colour;

            x->rightChild = newNode;
            if(newNode->leftChild)
			{
                newNode->leftChild->father = newNode;
			}
            if(newNode->rightChild)
			{
                newNode->rightChild->father = newNode;
			}
            newNode->father = x;

            if(x->leftChild->leftChild)
			{
				x->leftChild = x->leftChild->leftChild;
			}
            else
			{
				x->leftChild = nullptr;
			}

            if(x->leftChild)
			{
				x->leftChild->father = x;
			}
        }

        void preorderTraversal(CNode* tempNode)
		{
            if(!tempNode)
			{
				return;
			}
            cout << "--> " << tempNode->value << "<" << tempNode->colour << ">";
            preorderTraversal(tempNode->leftChild);
            preorderTraversal(tempNode->rightChild);
        }

        void postorderTraversal(CNode *tempNode)
		{
            if(!tempNode)
			{
				return;
			}
            postorderTraversal(tempNode->leftChild);
            postorderTraversal(tempNode->rightChild);
            cout << "--> " << tempNode->value << "<" << tempNode->colour << ">";
        }
 };
