// Kolejność wykonywania instrukcji
// Nowe zmienne
// Zanegowane wyrażenia warunkowe
// Zmienione pętle
#include <iostream>
#include <string>
using namespace std;

struct CNode
{
    int value{};

    CNode* father = nullptr;
    CNode* leftChild = nullptr;
    CNode* rightChild = nullptr;

    string colour;
};

class RedBlackTree
{
	CNode* rootNode;

    public:
        RedBlackTree() : rootNode(nullptr)
		{}

        CNode* getRoot(){
			return rootNode;
		}

         void insertNode(int value)
         {
           if(rootNode != nullptr)
		   {
               auto linkerNode = getRoot();
               CNode* newNode = new CNode();
               newNode->value = value;

               while(linkerNode != nullptr)
               {
                   if(linkerNode->value <= value)
                   {
                       if(linkerNode->rightChild != nullptr)
                       {
                           linkerNode = linkerNode->rightChild;
                       }
                       else
                       {
                           cout << "Element inserted.\n";
                           linkerNode->rightChild = newNode;
                           newNode->father = linkerNode;
                           newNode->colour = "RED";
                           break;
                       }
                   }
                   else
                   {
                       if(linkerNode->leftChild == nullptr)
                       {
                           cout << "Element inserted.\n";
                           newNode->colour = "RED";
                           newNode->father = linkerNode;
                           linkerNode->leftChild = newNode;
                           break;
                       }
                       else {
                           linkerNode = linkerNode->leftChild;
                       }
                   }
               }
               fixAfterInsert(newNode);
           }
           else
		   {
               cout << "Element inserted.\n";
               rootNode = new CNode();
               rootNode->father = nullptr;
               rootNode->colour = "BLACK";
               rootNode->value = value;
           }
        }

        void fixAfterInsert(CNode* z)
		{
            while(z->father->colour == "RED")
			{
                auto auntie = getRoot();
                auto grandfather = z->father->father;
                if(z->father != grandfather->leftChild)
				{
                    if(grandfather->leftChild)
                    {
                        auntie = grandfather->leftChild;
                    }
                    if(auntie->colour == "RED")
                    {
                        z->father->colour = "BLACK";
                        grandfather->colour = "RED";
                        auntie->colour = "BLACK";
                        if(grandfather->value != rootNode->value)
                        {
                            z = grandfather;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else if(z == grandfather->rightChild->leftChild)
                    {
                        rightRotate(z->father);
                    }
                    else
                    {
                        grandfather->colour = "RED";
                        z->father->colour = "BLACK";
                        leftRotate(grandfather);
                        if(grandfather->value != rootNode->value)
                        {
                            z = grandfather;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if(grandfather->rightChild)
                    {
                        auntie = grandfather->rightChild;
                    }
                    if(auntie->colour == "RED")
                    {
                        z->father->colour = "BLACK";
                        grandfather->colour = "RED";
                        auntie->colour = "BLACK";
                        if(grandfather->value == rootNode->value){
                            break;
                        }
                        else
                        {
                            z = grandfather;
                        }
                    }
                    else if(z == grandfather->leftChild->rightChild)
                    {
                        leftRotate(z->father);
                    }
                    else
                    {
                        grandfather->colour = "RED";
                        z->father->colour = "BLACK";
                        rightRotate(grandfather);
                        if(grandfather->value == rootNode->value)
                        {
                            break;
                        }
                        else
                        {
                            z = grandfather;
                        }
                    }
                }
            }
            rootNode->colour = "BLACK";
        }


        void removeNode(CNode* father, CNode* current, int value)
		{
            if(current == nullptr)
			{
				return;
			}

            if(current->value == value)
			{
                if(current->leftChild == nullptr && current->rightChild == nullptr)
				{
                    if(father->value == current->value)
					{
                        rootNode = nullptr;
					}
                    else if(father->rightChild != current)
					{
                        fixOnDelete(current);
                        father->leftChild = nullptr;
                    }
                    else
					{
                        fixOnDelete(current);
                        father->rightChild = nullptr;
                    }
                }
                else if(current->leftChild != nullptr && current->rightChild == nullptr)
				{
                    int swapValue = current->value;
                    current->value = current->leftChild->value;
                    current->leftChild->value = swapValue;
                    removeNode(current, current->rightChild, value);
                }
                else if(current->leftChild == nullptr && current->rightChild != nullptr)
				{
                    int swap = current->value;
                    current->value = current->rightChild->value;
                    current->rightChild->value = swap;
                    removeNode(current, current->rightChild, value);
                }
                else
				{
                    bool isLeftChildPresent = false;
                    CNode* tempNode = current->rightChild;
                    while(tempNode->leftChild)
					{
                        isLeftChildPresent = true;
						father = tempNode;
                        tempNode = tempNode->leftChild;
					}
                    if(!isLeftChildPresent)
					{
                        father = current;
					}

                    int swap = current->value;
                    current->value = tempNode->value;
                    tempNode->value = swap;
                    removeNode(father, tempNode, swap);
                }
            }
        }

        void remove(int stuff)
		{
            bool flag = false;
            auto parent = rootNode;
            auto temp = parent;
            if(!temp)
			{
                removeNode(nullptr, nullptr, stuff);
			}

            while(temp)
			{
                if(stuff == temp->value) { flag = true;
                    removeNode(parent, temp, stuff); break; }
                else if(stuff >= temp->value) { parent = temp ; temp = temp->rightChild; }
                else { parent = temp ; temp = temp->leftChild; }
            }

            if(!flag)
			{
				cout << "\nElement doesn't exist in the table";
			}
        }

        void fixOnDelete(CNode* z)
		{
            while(z->value != rootNode->value && z->colour == "BLACK")
			{
                auto brother = getRoot();
                if(z->father->leftChild == z)
				{
                    if(z->father->rightChild)
					{
                        brother = z->father->rightChild;
					}
                    if(brother)
					{
                        if(brother->colour == "RED")
						{
                            z->father->colour = "RED";
                            brother->colour = "BLACK";
                            leftRotate(z->father);
                            brother = z->father->rightChild;
                        }

                        if(brother->leftChild == nullptr && brother->rightChild == nullptr)
						{
                            z = z->father;
                            brother->colour = "RED";
                        }
                        else if(brother->leftChild->colour == "BLACK" && brother->rightChild->colour == "BLACK")
						{
                            z = z->father;
                            brother->colour = "RED";
                        }
                        else if(brother->rightChild->colour == "BLACK")
						{
                            brother->leftChild->colour = "BLACK";
                            brother->colour = "RED";
                            rightRotate(brother);
                            brother = z->father->rightChild;
                        }
						else
						{
                            brother->colour = z->father->colour;
                            z->father->colour = "BLACK";
                            if(brother->rightChild)
							{
                                brother->rightChild->colour = "BLACK";
							}
                            leftRotate(z->father);
                            z = rootNode;
                        }
                    }
                }
				else
				{
                    if(z->father->rightChild == z)
					{
                        if(z->father->leftChild)
						{
                            brother = z->father->leftChild;
						}
                        if(brother)
						{
                            if(brother->colour == "RED")
							{
                                z->father->colour = "RED";
                                brother->colour = "BLACK";
                                rightRotate(z->father);
                                brother = z->father->leftChild;
                            }
                             if(brother->leftChild == nullptr && brother->rightChild == nullptr)
							{
                                z = z->father;
                                brother->colour = "RED";
                            }
                            else if(brother->leftChild->colour == "BLACK" && brother->rightChild->colour == "BLACK")
							{
                                z = z->father;
                                brother->colour = "RED";
                            }
                            else if(brother->leftChild->colour == "BLACK")
							{
                                brother->colour = "RED";
                                brother->rightChild->colour = "BLACK";
                                rightRotate(brother);
                                brother = z->father->leftChild;
                            }
							else
							{
                                brother->colour = z->father->colour;
                                z->father->colour = "BLACK";
                                if(brother->leftChild)
								{
                                    brother->leftChild->colour = "BLACK";
								}
                                leftRotate(z->father);
                                z = rootNode;
                            }
                        }
                    }

                }
            }
            z->colour = "BLACK";
        }

        CNode* search(int value)
		{
            auto tempNode = getRoot();
            if(tempNode == nullptr)
			{
				return nullptr;
			}

            while(tempNode)
			{
                if(value == tempNode->value)
				{
					return tempNode;
				}
                else if(value >= tempNode->value)
				{
                    tempNode = tempNode->rightChild;
				}
                else
				{
                    tempNode = tempNode->leftChild;
				}
            }
            return nullptr;
        }

         void leftRotate(CNode* node)
		 {
            CNode* newNode = new CNode();
            if(node->rightChild->leftChild)
			{
                newNode->rightChild = node->rightChild->leftChild;
			}
             newNode->value = node->value;
             newNode->colour = node->colour;
             node->value = node->rightChild->value;
             newNode->leftChild = node->leftChild;
             node->leftChild = newNode;

             if(newNode->leftChild)
             {
                 newNode->leftChild->father = newNode;
             }
             if(newNode->rightChild)
             {
                 newNode->rightChild->father = newNode;
             }
             newNode->father = node;

            if(!node->rightChild->rightChild)
			{
                node->rightChild = nullptr;
			}
            else
			{
                node->rightChild = node->rightChild->rightChild;
			}

            if(node->rightChild)
			{
                node->rightChild->father = node;
			}
        }

        void rightRotate(CNode* x)
		{
            CNode* newNode = new CNode();
            if(x->leftChild->rightChild)
			{
                newNode->leftChild = x->leftChild->rightChild;
			}
            newNode->colour = x->colour;
            newNode->value = x->value;
            newNode->rightChild = x->rightChild;

            x->colour = x->leftChild->colour;
            x->value = x->leftChild->value;

            x->rightChild = newNode;

            if(newNode->leftChild)
            {
                newNode->leftChild->father = newNode;
            }
            if(newNode->rightChild)
            {
                newNode->rightChild->father = newNode;
            }

            newNode->father = x;

            if(x->leftChild->leftChild)
			{
				x->leftChild = x->leftChild->leftChild;
			}
            else
			{
				x->leftChild = nullptr;
			}

            if(x->leftChild)
			{
				x->leftChild->father = x;
			}
        }

        void preorderTraversal(CNode* tempNode)
		{
            if(!tempNode)
			{
				return;
			}
            cout << "--> " << tempNode->value << "<" << tempNode->colour << ">";
            preorderTraversal(tempNode->leftChild);
            preorderTraversal(tempNode->rightChild);
        }

        void postorderTraversal(CNode *tempNode)
		{
            if(!tempNode)
			{
				return;
			}
            postorderTraversal(tempNode->leftChild);
            postorderTraversal(tempNode->rightChild);
            cout << "--> " << tempNode->value << "<" << tempNode->colour << ">";
        }
 };
