#!/bin/bash
mkdir -p "data"
cd src/source
FILES=*
for f in $FILES
do
	newDirName="../../data/$f/"
	newFileName="$newDirName/file.cpp"
	mkdir -p $newDirName
	cp $f $newFileName
done

cd ../include
FILES=*
for f in $FILES
do
        newDirName="../../data/$f/"
        newFileName="$newDirName/file.cpp"
        mkdir -p $newDirName
        cp $f $newFileName
done
